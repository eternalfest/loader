# SWF Socket

The **SWF Socket** system allows bi-directional communication between a Flash movie
and its host page.

1. The host page creates a server. It chooses a random method name and attaches it to `window`.
   This method is the server's entry point: clients can call this method to establish a
   connection with the server.
2. The host page creates the HTML element for the Flash movie. It generates an `id` for this
   HTML element. It also defines the following `flashvars`:
   - `object_id`: the `id` of the HTML Element for this movie (so the movie knows its element)
   - `server`: The name of the server method.
3. Once loaded, the flash movie generate its own method name. This is the client method, to
   receive message from the server. The movie registers it on its external interface.
   It then calls the server method with its element id and client method name (this pair is
   called the client id). `clientId = elementId + ":" + clientMethodName; serverMethod(clientId)`.
4. The server decodes the client Id to retrieve the elementId and method name. It then creates
   a JS object for this client. This client generates its own method name dedicated for this
   communication channel. This method name is the result of `serverMethod` and sent back to Flash.

At this point, there is a server method on `window` to accept new connections, a client method
on `window` dedicated to a single client and callable from Flash and a client method on the
HTML element of the movie dedicated for this same client but callable from JS.
