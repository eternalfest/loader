#!/usr/bin/env oil
var SCRIPT_DIR = $(cd "$_this_dir" { pwd })

cd $SCRIPT_DIR {
  dot -Tsvg -o loader.svg loader.dot
}
