# References

Here are some useful links:

- [Getting started with Haxe][archive-haxe-flash8]
- [Using an external library for Flash][haxe-external-library]

[archive-haxe-flash8]: https://web.archive.org/web/20140220034838/http://haxe.org/doc/start/flash
[haxe-external-library]: https://haxe.org/manual/target-flash-external-libraries.html
