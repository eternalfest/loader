![Loader](./graphs/loader.svg)

```

var tr = new TaskRunner();

var messages = tr.task([], function(ctx){

});

var game = tr.task([], function(ctx){

});

var gameXml: <Xml|null> = tr.task([game], function(ctx){

});

var langXml: <Xml|null> = tr.task([game], function(ctx){

});

var patchedMessages = tr.task([messages, gameXml, langXml], function(ctx){

});

var gameSwf = tr.task([game, patchedMessages], function(ctx){

});

var patchedGameSwf = tr.task([gameSwf, gameXml], function(ctx){
  tr.trap(ctx, game);
});

tr.start([game, patchedGameSwf, patchedMessages, gameXml], function(ctx) {

});

```
