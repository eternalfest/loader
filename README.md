# Loader

Loader for Eternalfest.

## Installation

Download the latest build artifacts from Gitlab and use them directly.

## Build and develop

See [CONTRIBUTING.md](./CONTRIBUTING.md)
