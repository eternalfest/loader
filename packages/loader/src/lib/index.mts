import sysPath from "path";
import { pathToFileURL, fileURLToPath } from "url";

export const FLASH8_OBFU = toUri("loader.flash8.swf");
export const FLASH11 = toUri("loader.flash11.swf");
export const VERSION = "5.1.2";

export enum Version {
  Flash8,
  Flash11,
}

export function getLoaderUri(version: Version): URL {
  switch (version) {
    case Version.Flash8:
      return FLASH8_OBFU;
    case Version.Flash11:
      return FLASH11;
    default:
      throw new TypeError(`Expected \`Version\` variant, got ${version}`);
  }
}

function toUri(filename: string): URL {
  const selfPath = fileURLToPath(import.meta.url);
  const resultPath = sysPath.join(selfPath, "..", filename);
  return pathToFileURL(resultPath);
}

// TODO: v5: This is a compat layer, remove in the next major version
export default {
  getLoaderUri,
  Version,
};
