import { getObfuMapUri } from "../lib/index.mjs";
import chai from "chai";
import { existsSync } from "fs";

console.log("foo");

describe("loader-types", function () {
  it("supports Flash 8", function() {
    const map = getObfuMapUri();
    chai.assert.instanceOf(map, URL);
    chai.assert(existsSync(map), `file exists: ${map}`);
  });
});
