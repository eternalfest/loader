package ef.patcher;

import devtools.server.runtime.ConsoleAPI;
import FlashStd;

class SwfSocketConsoleModule implements IConsoleModule {
  private var consoleApi: ConsoleAPI;
  private var frameHandler: Void -> Void;
  private var oldPrint: String;
  private var nextPrint: String;
  private var hasPrinted: Bool;

  public function new(consoleApi: ConsoleAPI) {
    this.consoleApi = consoleApi;
    this.nextPrint = "";
    this.oldPrint = "";
    this.frameHandler = function() {
      this.onFrame();
    }
    // TODO: Dispose object and remove frame listener
    FlashStd.addEnterFrameListener(this.frameHandler);
  }

  public function log(message: Dynamic): Void {
    this.consoleApi.log(message);
  }

  public function warn(message: Dynamic): Void {
    this.consoleApi.warn(message);
  }

  public function error(message: Dynamic): Void {
    this.consoleApi.error(message);
  }

  public function print(message: Dynamic): Void {
    if (this.nextPrint != "") {
      this.nextPrint = this.nextPrint + "\n";
    }
    this.nextPrint = this.nextPrint + message;
  }

  public function clear(): Void {
    this.nextPrint = "";
  }

  public function onFrame(): Void {
    if (this.nextPrint == "") {
      if (this.oldPrint != "") {
        this.consoleApi.clear();
      }
    } else {
      if (this.nextPrint != this.oldPrint) {
        this.consoleApi.setPrint(this.nextPrint);
      }
      this.oldPrint = this.nextPrint;
      this.nextPrint = "";
    }
  }
}
