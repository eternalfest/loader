package ef.patcher;

import haxe.Json;
import tink.core.Error;

class JsonModule {
  public function new() {
  }

  public function stringify(document: Dynamic): String {
    return Json.stringify(document);
  }

  public function parse(string: String): Dynamic {
    try {
      return Json.parse(string);
    } catch (err: String) {
      throw new Error(err);
    }
  }
}
