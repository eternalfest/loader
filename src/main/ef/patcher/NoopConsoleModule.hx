package ef.patcher;

class NoopConsoleModule implements IConsoleModule {
  public function new() {
  }

  public function log(message: Dynamic): Void {
  }

  public function warn(message: Dynamic): Void {
  }

  public function error(message: Dynamic): Void {
  }

  public function print(message: Dynamic): Void {
  }

  public function clear(): Void {
  }
}
