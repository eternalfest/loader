package ef.patcher;

import ef.api.run.IRun;
import etwin.Obfu;

class RunModule {
  private var run: IRun;

  public function new(run: IRun) {
    this.run = run;
  }

  public function getRun(): IRun {
    var run: IRun = this.run;

    Reflect.setField(run, Obfu.raw("id"), run.id);
    Reflect.setField(run, Obfu.raw("createdAt"), run.createdAt);
    Reflect.setField(run, Obfu.raw("startedAt"), run.startedAt);
    Reflect.setField(run, Obfu.raw("game"), run.game);
    Reflect.setField(run, Obfu.raw("user"), run.user);
    Reflect.setField(run, Obfu.raw("gameMode"), run.gameMode);
    Reflect.setField(run, Obfu.raw("gameOptions"), run.gameOptions);
    Reflect.setField(run, Obfu.raw("settings"), run.settings);

    Reflect.setField(run.game, Obfu.raw("id"), run.game.id);
    Reflect.setField(run.game, Obfu.raw("displayName"), run.game.displayName);

    Reflect.setField(run.user, Obfu.raw("id"), run.user.id);
    Reflect.setField(run.user, Obfu.raw("displayName"), run.user.displayName);

    Reflect.setField(run.settings, Obfu.raw("sound"), run.settings.sound);
    Reflect.setField(run.settings, Obfu.raw("music"), run.settings.music);
    Reflect.setField(run.settings, Obfu.raw("shake"), run.settings.shake);
    Reflect.setField(run.settings, Obfu.raw("detail"), run.settings.detail);
    Reflect.setField(run.settings, Obfu.raw("volume"), run.settings.volume);

    return run;
  }
}
