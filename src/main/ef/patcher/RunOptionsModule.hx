package ef.patcher;

import ef.api.run.IRun;

class RunOptionsModule {
  private var mode: String;
  private var options: Array<String>;

  public function new(run: IRun) {
    this.mode = run.gameMode;
    this.options = run.gameOptions.copy();
  }

  public function getMode() {
    return this.mode;
  }

  public function getOptions(): Array<String> {
    return this.options;
  }
}
