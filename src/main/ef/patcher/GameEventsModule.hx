package ef.patcher;

import ef.api.run.ISetRunResultOptions;
import ef.api.run.SetRunResultOptions;
import ef.loader.GameEventsHandler;
import etwin.Obfu;
import tink.core.Error;

class GameEventsModule {
  private var handler(default, null): GameEventsHandler;

  public function new(handler: GameEventsHandler) {
    this.handler = handler;
  }

  public function emitError(error: Error): Void {
    trace(Obfu.raw("ERROR"));
    trace(error);
  }

  public function endGame(options: ISetRunResultOptions): Void {
    this.handler.onGameEnd(options);
  }

  public function __endGame(__options: Dynamic): Void {
    var isVictory: Bool = Reflect.field(__options, Obfu.raw("isVictory"));
    var maxLevel: Int = Reflect.field(__options, Obfu.raw("maxLevel"));
    var scores: Array<Int> = Reflect.field(__options, Obfu.raw("scores"));
    var rawItems: Dynamic = Reflect.field(__options, Obfu.raw("items"));
    var items: Map<String, Int> = new Map();
    for (itemId in Reflect.fields(rawItems)) {
      items.set(itemId, Reflect.field(rawItems, itemId));
    }
    var stats: Dynamic = Reflect.field(__options, Obfu.raw("stats"));
    var options: ISetRunResultOptions = new SetRunResultOptions(isVictory, maxLevel, scores, items, stats);
    this.endGame(options);
  }
}
