package ef.patcher;

import ef.api.run.IRunStart;
import etwin.Obfu;

class RunStartModule {
  private var runStart: Void -> Null<IRunStart>;

  public function new(runStart: Void -> Null<IRunStart>) {
    this.runStart = runStart;
  }

  public function get(): Null<IRunStart> {
    var rs: IRunStart = this.runStart();

    Reflect.setField(rs, Obfu.raw("run"), rs.run);
    Reflect.setField(rs, Obfu.raw("startedAt"), rs.startedAt);
    Reflect.setField(rs, Obfu.raw("families"), rs.families);
    Reflect.setField(rs, Obfu.raw("items"), rs.items);
    Reflect.setField(rs, Obfu.raw("key"), rs.key);

    Reflect.setField(rs.run, Obfu.raw("id"), rs.run.id);

    return rs;
  }
}
