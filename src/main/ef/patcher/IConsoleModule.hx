package ef.patcher;

interface IConsoleModule {
  function log(message: Dynamic): Void;

  function warn(message: Dynamic): Void;

  function error(message: Dynamic): Void;

  function print(message: Dynamic): Void;

  function clear(): Void;
}
