package ef.patcher;

import devtools.server.Devtools;
import ef.api.run.IRun;
import ef.api.run.IRunStart;
import ef.loader.GameContent;
import ef.loader.GameEventsHandler;
import ef.patcher.EnvModule;
import ef.patcher.ErrorModule;
import ef.patcher.GameEventsModule;
import ef.patcher.JsonModule;
import ef.patcher.RunModule;
import ef.patcher.RunOptionsModule;
import ef.patcher.RunStartModule;
import etwin.flash.MovieClip;
import etwin.Obfu;
import net.demurgos.uri.Uri;
import tink.core.Error;

class ModuleLoader {
  private var cache: Map<String, Dynamic>;
  private var devtools: Null<Devtools>;
  private var gameEngine: MovieClip;
  private var gameContent: Null<GameContent>;
  private var gameEventsHandler: GameEventsHandler;
  private var run: IRun;
  private var runStart: Void -> Null<IRunStart>;
  private var doneCallback: Null<Error> -> Void;

  public function new(
    devtools: Null<Devtools>,
    gameEngine: MovieClip,
    gameContent: Null<GameContent>,
    gameEventsHandler: GameEventsHandler,
    run: IRun,
    runStart: Void -> Null<IRunStart>,
    doneCallback: Null<Error> -> Void
  ) {
    this.cache = new Map<String, Dynamic>();
    this.devtools = devtools;
    this.gameEngine = gameEngine;
    this.gameContent = gameContent;
    this.gameEventsHandler = gameEventsHandler;
    this.runStart = runStart;
    this.doneCallback = doneCallback;
    this.run = run;
  }

  public function require(id: String) {
    if (this.cache.exists(id)) {
      return this.cache.get(id);
    }

    var module: Dynamic;
    if (id == Obfu.raw("console")) {
      module = this.createConsole();
    } else if (id == Obfu.raw("data")) {
      module = this.createData();
    } else if (id == Obfu.raw("env")) {
      module = this.createEnv();
    } else if (id == Obfu.raw("error")) {
      module = this.createError();
    } else if (id == Obfu.raw("game-engine")) {
      module = this.createGameEngine();
    } else if (id == Obfu.raw("game-events")) {
      module = this.createGameEvents();
    } else if (id == Obfu.raw("json")) {
      module = this.createJson();
    } else if (id == Obfu.raw("run")) {
      module = this.createRun();
    } else if (id == Obfu.raw("run-start")) {
      module = this.createRunStart();
    } else if (id == Obfu.raw("run-options")) {
      module = this.createRunOptions();
    } else {
      this.emitError(new Error("Unknown module: " + id));
      return null;
    }
    this.cache.set(id, module);
    return module;
  }

  private function emitError(err: Error): Void {
    trace(Obfu.raw("ERROR"));
    trace(err);
  }

  private function createConsole(): IConsoleModule {
    var isDebug: Bool = false;
    var loaderUriStr: Null<String> = Reflect.field(flash.Lib.current, "_url");
    if (loaderUriStr != null) {
      var loaderUri: Uri = new Uri(loaderUriStr);
      isDebug = loaderUri.getHostname() == Obfu.raw("localhost");
    }
    var mod: IConsoleModule;
    if (!isDebug) {
      mod = new NoopConsoleModule();
    } else if (this.devtools != null) {
      mod = new SwfSocketConsoleModule(this.devtools.runtime.consoleAPI);
    } else {
      mod = new LocalConsoleModule();
    }
    Reflect.setField(mod, Obfu.raw("log"), mod.log);
    Reflect.setField(mod, Obfu.raw("warn"), mod.warn);
    Reflect.setField(mod, Obfu.raw("error"), mod.error);
    Reflect.setField(mod, Obfu.raw("print"), mod.print);
    Reflect.setField(mod, Obfu.raw("clear"), mod.clear);
    return mod;
  }

  private function createData(): Dynamic {
    return this.gameContent != null ? this.gameContent.getDataCopy() : {};
  }

  private function createEnv(): EnvModule {
    var isDebug: Bool = false;
    var loaderUriStr: Null<String> = Reflect.field(flash.Lib.current, "_url");
    if (loaderUriStr != null) {
      var loaderUri: Uri = new Uri(loaderUriStr);
      isDebug = loaderUri.getHostname() == Obfu.raw("localhost");
    }

    var mod: EnvModule = new EnvModule(isDebug);
    Reflect.setField(mod, Obfu.raw("getLoaderVersion"), mod.getLoaderVersion);
    Reflect.setField(mod, Obfu.raw("isDebug"), mod.isDebug);
    return mod;
  }

  private function createError(): ErrorModule {
    var mod: ErrorModule = new ErrorModule();
    Reflect.setField(mod, Obfu.raw("createError"), mod.createError);
    return mod;
  }

  private function createGameEngine(): MovieClip {
    return this.gameEngine;
  }

  private function createGameEvents(): GameEventsModule {
    var mod: GameEventsModule = new GameEventsModule(this.gameEventsHandler);
    Reflect.setField(mod, Obfu.raw("emitError"), mod.emitError);
    Reflect.setField(mod, Obfu.raw("endGame"), mod.__endGame);
    return mod;
  }

  private function createJson(): JsonModule {
    var mod: JsonModule = new JsonModule();
    Reflect.setField(mod, Obfu.raw("stringify"), mod.stringify);
    Reflect.setField(mod, Obfu.raw("parse"), mod.parse);
    return mod;
  }

  private function createRun(): RunModule {
    var mod: RunModule = new RunModule(this.run);
    Reflect.setField(mod, Obfu.raw("getRun"), mod.getRun);
    return mod;
  }

  private function createRunStart(): RunStartModule {
    var mod: RunStartModule = new RunStartModule(this.runStart);
    Reflect.setField(mod, Obfu.raw("get"), mod.get);
    return mod;
  }

  private function createRunOptions(): RunOptionsModule {
    var mod: RunOptionsModule = new RunOptionsModule(this.run);
    Reflect.setField(mod, Obfu.raw("getMode"), mod.getMode);
    Reflect.setField(mod, Obfu.raw("getOptions"), mod.getOptions);
    return mod;
  }
}
