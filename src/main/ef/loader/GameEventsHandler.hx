package ef.loader;

import ef.api.run.ISetRunResultOptions;
import etwin.Error;

typedef OnGameEndHandler = ISetRunResultOptions -> Void;

typedef OnGameCrashHandler = Error -> Void;

typedef GameEventsHandler = {
  onGameEnd: OnGameEndHandler,
  onGameCrash: OnGameCrashHandler
}
