package ef.loader;

import ef.patcher.IConsoleModule;
import haxe.crypto.Md5;
import haxe.ds.StringMap;
import haxe.Json;
import etwin.Obfu;
import tink.core.Error;

class LevelSet {
  public var name(default, null): String;
  public var signature(default, null): String;
  public var data(default, null): String;

  public function new(name: String, signature: String, data: String) {
    this.name = name;
    this.signature = signature;
    this.data = data;
  }
}

class GameContent {
  public var sourceString: String;
  public var dimensions: StringMap<LevelSet>;
  public var specialItemsXml: Null<Xml>;
  public var scoreItemsXml: Null<Xml>;
  public var linksXml: Null<Xml>;
  public var questsXml: Null<Xml>;
  public var messagesXml: Null<Xml>;
  private var data: Dynamic;

  private function new(gameContentNode: Xml, ?console: IConsoleModule) {
    this.sourceString = gameContentNode.toString();
    this.dimensions = new StringMap<LevelSet>();
    this.specialItemsXml = null;
    this.scoreItemsXml = null;
    this.linksXml = null;
    this.questsXml = null;
    this.data = {};

    if (gameContentNode.nodeType != Xml.Element || gameContentNode.nodeName != Obfu.raw("game")) {
      throw new Error("AssertionFailed: Expected `gameContentNode` to be a <game> XML Element");
    }

    for (child in gameContentNode.elements()) {
      if (child.nodeType != Xml.Element) {
        continue;
      }
      if (child.nodeName == Obfu.raw("levels")) {
        this.readDimensions(child, console);
      } else if (child.nodeName == Obfu.raw("links")) {
        this.linksXml = child;
      } else if (child.nodeName == Obfu.raw("quests")) {
        this.questsXml = child;
      } else if (child.nodeName == Obfu.raw("lang")) {
        if (console != null) {
          // console.warn("Deprecated: <lang> node, use <messages>");
        }
        child.nodeName = Obfu.raw("messages");
        this.messagesXml = child;
      } else if (child.nodeName == Obfu.raw("items")) {
        var itemsType: Null<String> = child.get(Obfu.raw("type"));
        if (itemsType == Obfu.raw("score")) {
          this.scoreItemsXml = child;
        } else if (itemsType == Obfu.raw("special")) {
          this.specialItemsXml = child;
        } else {
          continue;
        }
      } else if (child.nodeName == Obfu.raw("datas")) {
        for (dataNode in child.elements()) {
          if (dataNode.nodeType != Xml.Element || dataNode.nodeName != "data") {
            continue;
          }
          this.addData(dataNode, console);
        }
      } else {
        continue;
      }
    }
  }

  private function addData(dataNode: Xml, ?console: IConsoleModule): Void {
    if (dataNode.nodeType != Xml.Element || dataNode.nodeName != Obfu.raw("data")) {
      throw new Error("AssertionFailed: Expected `dataNode` to be a <data> XML Element");
    }
    var key: Null<String> = dataNode.get(Obfu.raw("name"));
    if (key == null) {
      throw new Error("AssertionFailed: Missing `name` attribute on <data>");
    }

    var valueString: Null<String> = null;
    for (child in dataNode.iterator()) {
      if (child.nodeType == Xml.PCData) {
        valueString = child.nodeValue;
      }
    }
    if (valueString == null) {
      if (console != null) {
        console.warn("Empty data node: " + key);
      }
      return;
    }

    var value: Dynamic;
    try {
      value = Json.parse(valueString);
    } catch (err: String) {
      throw new Error(err);
    }
    Reflect.setField(this.data, key, value);
  }

  public function getDataCopy(): Dynamic {
    return Json.parse(Json.stringify(this.data));
  }

  public static function fromXmlDocument(xmlDocument: Xml, ?console: IConsoleModule): GameContent {
    if (xmlDocument.nodeType != Xml.Document) {
      throw new Error("Assertion: Expected `xmlDocument.nodeType` to be `Xml.Document`");
    }
    var gameContentNode: Null<Xml> = xmlDocument.firstElement();
    if (gameContentNode == null || gameContentNode.nodeType != Xml.Element || gameContentNode.nodeName != Obfu.raw("game")) {
      throw new Error("Assertion: Expected <game> XML Element as first child of `xmlDocument`");
    }
    removeWhitespaceAndCommentNodes(gameContentNode);
    return new GameContent(gameContentNode, console);
  }

  private function readDimensions(levelsNode: Xml, ?console: IConsoleModule): Void {
    for (dimensionNode in levelsNode.elements()) {
      if (dimensionNode.nodeType != Xml.Element || dimensionNode.nodeName != Obfu.raw("dim")) {
        continue;
      }
      var data: String = "";
      for (child in dimensionNode.iterator()) {
        if (child.nodeType == Xml.PCData) {
          data = child.nodeValue;
        }
      }
      var name: Null<String> = dimensionNode.get(Obfu.raw("name"));
      var signature: Null<String> = dimensionNode.get(Obfu.raw("signature"));
      if (name == null || signature == null) {
        throw new Error("Missing level set signature");
      }
      var levelSet: LevelSet = new LevelSet(name, signature, data);
      this.dimensions.set(name, levelSet);
    }
  }

  private static function getLevelSetSignature(name: String, data: String): String {
    var value: Int = 0;
    var parts: Array<String> = data.split("0");
    var c: Int = name.charCodeAt(3);
    for (part in parts) {
      if (part.length > 30) {
        value += part.charCodeAt(c % 5);
        value += part.charCodeAt(c % 9) * part.charCodeAt(c % 15);
        value += part.charCodeAt(c % 19);
        value += part.charCodeAt(c % 22);
      }
    }

    return Md5.encode(name) + Md5.encode(Std.string(value));
  }

  private static function removeWhitespaceAndCommentNodes(node: Xml): Void {
    var toRemove: Array<Xml> = [];

    for (child in node.iterator()) {
      switch(child.nodeType) {
        case Xml.PCData:
          if (StringTools.trim(child.nodeValue) == "") {
            toRemove.push(child);
          }
        case Xml.Comment:
          toRemove.push(child);
        case Xml.Element:
          removeWhitespaceAndCommentNodes(child);
      }
    }

    for (elem in toRemove) {
      node.removeChild(elem);
    }
  }
}
