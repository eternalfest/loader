package ef.loader;

typedef Progress = {
  current: Int,
  total: Int,
}
