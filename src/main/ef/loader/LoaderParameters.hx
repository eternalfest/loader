package ef.loader;

import ef.api.run.IRun;
import net.demurgos.uri.Uri;

typedef LoaderParameters = {
  apiBase:Uri,
  gameUri:Uri,
  run:IRun,
  locale:String,
  swfSocketServer:Null<String>,
  objectId:Null<String>,
}
