package ef.loader;

import ef.api.run.ISetRunResultOptions;
import ef.loader.GameEventsHandler;
import devtools.server.Devtools;
import ef.api.run.SetRunResultOptions;
import ef.loader.GameEventsHandler.OnGameCrashHandler;
import ef.loader.GameEventsHandler.OnGameEndHandler;
import ef.api.blob.Blob;
import ef.api.Api;
import ef.api.game.Game;
import ef.api.game.GameBuild;
import ef.api.game.GameBuildI18n;
import ef.api.game.GameEngine;
import ef.api.game.GameModeSpec;
import ef.api.game.GameOptionSpec;
import ef.api.game.GamePatcher;
import ef.api.game.GameResource;
import ef.api.games.GameRef;
import ef.api.games.IGameResource;
import ef.api.games.IGameMode;
import ef.api.games.GameOptionState;
import ef.api.games.GameModeState;
import ef.api.run.RunStart;
import ef.api.run.IRunStart;
import ef.api.run.IRunSettings;
import ef.api.run.IRun;
import ef.api.run.Run;
import ef.api.users.UserRef;
import ef.loader.i18n.Dictionary;
import ef.loader.i18n.Locale;
import ef.loader.i18n.Locale.LocaleTools;
import ef.loader.LoadOptions;
import ef.loader.LoaderCompat;
import ef.loader.LoaderTask;
import ef.loader.GameContent;
import ef.patcher.IConsoleModule;
import ef.patcher.ModuleLoader;
import etwin.ds.Nil;
import haxe.crypto.Md5;
import haxe.Json;
import haxe.Timer;
import mt.hfest.IManager;
import mt.hfest.ManagerOptions;
import net.demurgos.uri.Uri;
import orchestrator.IDependencies;
import orchestrator.Orchestrator;
import orchestrator.Token;
import tink.core.Error;
import tink.core.Future;
import tink.core.Outcome;
import tink.CoreApi.Surprise;
import Reflect;
import etwin.Obfu;

#if flash8
import etwin.flash.MovieClip;
import etwin.flash.MovieClipLoader;
import etwin.flash.Sound;
#else
import flash.display.MovieClip;
import openfl.media.Sound;
#end

class LoaderService {
  public static var GAME_ENGINE_DEPTH(default, null) = 10;
  public static var MUSICS_DEPTH(default, null) = 20;
  public static var EXTERNAL_DEPTH(default, null) = 30;
  public static var ENGINE_PATCHER_DEPTH(default, null) = 40;
  // Failure to load the music if it takes more than 10 minutes
  public static var LOAD_MUSIC_TIMEOUT(default, null) = 10 * 60 * 1000;

  private var api:Api;
  private var console:IConsoleModule;
  private var compatibilityMode:Bool;
  // Store loaders here so they aren't garbage collected
  private var loaders:Array<Dynamic>;
  private var isVictory:Bool;
  private var stats:Dynamic;
  private var isDebug:Bool;
  // TODO: This should NOT be a service variable!
  private var runStart:Null<IRunStart>;

  public function new(api:Api, console:IConsoleModule, isDebug:Bool) {
    this.api = api;
    this.console = console;
    this.isDebug = isDebug;
    this.compatibilityMode = true;
    this.loaders = [];
    this.isVictory = false;
    this.stats = {};
    this.runStart = null;
  }

  public function load(options:LoadOptions): LoaderTask {
    var onceGameEventsHandler: GameEventsHandler = LoaderService.ensureGameEventsOnce(options.gameEventsHandler);
    var orchestrator: Orchestrator = new Orchestrator();
    var compat: Bool = true;
    var progress: LoaderProgress = new LoaderProgress();

    var virtualRootToken:Token<MovieClip> = orchestrator.syncTask(
      [],
      function(deps:IDependencies):MovieClip {
        return options.virtualRoot;
      }
    );

    var messagesToken:Token<Dictionary> = orchestrator.syncTask(
      [],
      function(deps:IDependencies):Dictionary {
        return Dictionary.getDictionary(options.locale);
      }
    );

    var gameEventsHandlerToken:Token<GameEventsHandler> = orchestrator.syncTask(
      [],
      function(deps:IDependencies):GameEventsHandler {
        return onceGameEventsHandler;
      }
    );

    var gameMetadataToken:Token<Game> = orchestrator.asyncTask(
      [],
      function(deps:IDependencies):Surprise<Game, Error> {
        return this.api.getGame(options.gameUri);
      }
    );

    var resolvedRunOptionsToken:Token<IRun> = orchestrator.syncTask(
      [gameMetadataToken],
      function(deps:IDependencies):IRun {
        return this.resolveRunOptions(deps.get(gameMetadataToken), options.run);
      }
    );

    var patchedVirtualRootToken:Token<MovieClip> = orchestrator.syncTask(
      [virtualRootToken, gameEventsHandlerToken, resolvedRunOptionsToken],
      function(deps:IDependencies):MovieClip {
        var virtualRoot:MovieClip = deps.get(virtualRootToken);
        var gameEventsHandler:GameEventsHandler = deps.get(gameEventsHandlerToken);
        var run:IRun = deps.get(resolvedRunOptionsToken);
        this.patchVirtualRoot(virtualRoot, gameEventsHandler, run, options.locale);
        return virtualRoot;
      }
    );

    var gameEngineToken:Token<MovieClip> = orchestrator.asyncTask(
      [gameMetadataToken, patchedVirtualRootToken],
      function(deps:IDependencies):Surprise<MovieClip, Error> {
        var gameMetadata:Game = deps.get(gameMetadataToken);
        var patchedVirtualRoot:MovieClip = deps.get(patchedVirtualRootToken);
        return this.loadGameEngine(patchedVirtualRoot, gameMetadata, progress);
      }
    );

    var gameContentToken:Token<Null<GameContent>> = orchestrator.asyncTask(
      [gameMetadataToken],
      function(deps:IDependencies):Surprise<Null<GameContent>, Error> {
        var gameMetadata:Game = deps.get(gameMetadataToken);
        return this.getGameContent(gameMetadata, progress);
      }
    );

    var messagesPatchToken:Token<Null<Dictionary>> = orchestrator.asyncTask(
      [gameMetadataToken],
      function(deps:IDependencies):Surprise<Null<Dictionary>, Error> {
        var gameMetadata:Game = deps.get(gameMetadataToken);
        return this.getMessagesPatch(gameMetadata, options.locale, progress);
      }
    );

    var patchedMessagesToken:Token<Dictionary> = orchestrator.syncTask(
      [messagesToken, gameContentToken, messagesPatchToken],
      function(deps:IDependencies):Dictionary {
        var messages:Dictionary = deps.get(messagesToken);
        var gameContent:Null<GameContent> = deps.get(gameContentToken);
        var messagesPatch:Null<Dictionary> = deps.get(messagesPatchToken);
        return this.getPatchedMessages(messages, gameContent, messagesPatch);
      }
    );

    var contentPatchedGameEngineToken:Token<MovieClip> = orchestrator.syncTask(
      [gameEngineToken, gameContentToken],
      function(deps:IDependencies):MovieClip {
        var gameEngine:MovieClip = deps.get(gameEngineToken);
        var gameContent:Null<GameContent> = deps.get(gameContentToken);
        if (gameContent != null) {
          this.contentPatchGameEngine(gameEngine, gameContent, compat);
        }
        return gameEngine;
      }
    );

    var enginePatcherToken:Token<Null<MovieClip>> = orchestrator.asyncTask(
      [patchedVirtualRootToken, gameMetadataToken],
      function(deps:IDependencies):Surprise<Null<MovieClip>, Error> {
        var patchedVirtualRoot:MovieClip = deps.get(patchedVirtualRootToken);
        var gameMetadata:Game = deps.get(gameMetadataToken);
        return this.loadEnginePatcher(patchedVirtualRoot, gameMetadata, progress);
      }
    );

    var fullyPatchedGameEngineToken:Token<MovieClip> = orchestrator.asyncTask(
      [contentPatchedGameEngineToken, enginePatcherToken, gameContentToken, gameEventsHandlerToken],
      function(deps:IDependencies):Surprise<Null<MovieClip>, Error> {
        var contentPatchedGameEngine:MovieClip = deps.get(contentPatchedGameEngineToken);
        var enginePatcher:MovieClip = deps.get(enginePatcherToken);
        var gameContent:Null<GameContent> = deps.get(gameContentToken);
        var gameEventsHandler:GameEventsHandler = deps.get(gameEventsHandlerToken);
        var run:IRun = deps.get(resolvedRunOptionsToken);
        return this.applyEnginePatcher(contentPatchedGameEngine, enginePatcher, options.devtools, gameContent, gameEventsHandler, run);
      }
    );

    var musicsToken:Token<Null<Array<Sound>>> = orchestrator.asyncTask(
      [gameMetadataToken, patchedVirtualRootToken, resolvedRunOptionsToken],
      function(deps:IDependencies):Surprise<Null<Array<Sound>>, Error> {
        var gameMetadata:Game = deps.get(gameMetadataToken);
        var patchedVirtualRoot:MovieClip = deps.get(patchedVirtualRootToken);
        var run:IRun = deps.get(resolvedRunOptionsToken);
        return this.loadMusics(patchedVirtualRoot, gameMetadata, run.settings, progress);
      }
    );

    var readyToStartToken:Token<Void> = orchestrator.syncTask(
      [contentPatchedGameEngineToken, patchedMessagesToken, musicsToken],
      function(deps:IDependencies):Void {
        return;
      }
    );

    var startGameSignalToken:Token<Dynamic> = orchestrator.asyncTask(
      [readyToStartToken],
      function(deps:IDependencies):Surprise<Dynamic, Error> {
        var onClick:Future<Dynamic> = options.awaitStartGameSignal();
        var result:Surprise<Dynamic, Error> = onClick.map(function(any:Dynamic):Outcome<Dynamic, Error> {
          return Outcome.Success(null);
        });
        return result;
      }
    );

    var runStartToken:Token<IRunStart> = orchestrator.asyncTask(
      [startGameSignalToken, resolvedRunOptionsToken],
      function(deps:IDependencies):Surprise<IRunStart, Error> {
        untyped { // TODO: Once Haxe adds support for generic variance, use it (right now we cast from `RunStart` to `IRunStart`
          return this.api.startRun(deps.get(resolvedRunOptionsToken).id);
        }
      }
    );

    var gameManagerToken:Token<Null<IManager>> = orchestrator.syncTask(
      [fullyPatchedGameEngineToken, patchedMessagesToken, musicsToken, runStartToken, gameEventsHandlerToken, resolvedRunOptionsToken],
      function(deps:IDependencies):Null<IManager> {
        var gameEngine:MovieClip = deps.get(fullyPatchedGameEngineToken);
        var messages:Dictionary = deps.get(patchedMessagesToken);
        var musics:Null<Array<Sound>> = deps.get(musicsToken);
        var runStart:IRunStart = deps.get(runStartToken);
        var gameEventsHandler:GameEventsHandler = deps.get(gameEventsHandlerToken);
        var run:IRun = deps.get(resolvedRunOptionsToken);
        return this.createGameManager(gameEngine, messages, musics, run.gameOptions, runStart, gameEventsHandler);
      }
    );

    var runner = orchestrator.startRunner(gameManagerToken);
    return new LoaderTask(runner, progress);
  }

  public function resolveRunOptions(game: Game, run: IRun): IRun {
    // Validate game mode
    var mode: GameModeSpec = null;
    for (modeKey in game.channels.active.build.modes.keys()) {
      var cur: GameModeSpec = game.channels.active.build.modes[modeKey];
      if (modeKey == run.gameMode && cur.isVisible) {
        mode = cur;
        break;
      }
    }
    if(mode == null) {
      throw new Error("InvalidGameMode: " + run.gameMode);
    }

    // Validate options
    var invalidOptions:Array<String> = run.gameOptions.copy();
    for (optionKey in mode.options.keys()) {
      var cur: GameOptionSpec = mode.options[optionKey];
      var selected = invalidOptions.remove(optionKey);

      if (!cur.isEnabled) {
        if (selected != cur.defaultValue) {
          invalidOptions.push(selected ? optionKey : '!$optionKey');
        }
      }
    }
    if(invalidOptions.length > 0) {
      throw new Error("InvalidGameOptions: " + invalidOptions.join(", "));
    }

    return new Run(
      run.id,
      run.createdAt,
      run.startedAt,
      new GameRef(run.game.id, run.game.displayName),
      new UserRef(run.user.id, run.user.displayName),
      run.gameMode,
      run.gameOptions,
      run.settings
    );
  }

  public function patchVirtualRoot(virtualRoot:MovieClip, gameEventsHandler:GameEventsHandler, run:IRun, locale:Locale):Void {
    this.patchOnGameEnd(virtualRoot, run.gameMode, gameEventsHandler);
    this.patchOnGameCrash(virtualRoot, gameEventsHandler.onGameCrash);

    switch (locale) {
      case Locale.EnUs: Reflect.setField(virtualRoot, "$lang", Obfu.raw("en"));
      case Locale.EsSp: Reflect.setField(virtualRoot, "$lang", Obfu.raw("es"));
      case Locale.FrFr: Reflect.setField(virtualRoot, "$lang", Obfu.raw("fr"));
      default: throw new Error("UnknownLocale: " + locale);
    }
    Reflect.setField(virtualRoot, "$options", run.gameOptions.join(","));
    Reflect.setField(virtualRoot, "$mode", run.gameMode);
    Reflect.setField(virtualRoot, "$volume", Std.string(Math.floor(100 * run.settings.volume)));
    Reflect.setField(virtualRoot, "$sound", run.settings.sound ? "1" : "0");
    Reflect.setField(virtualRoot, "$music", run.settings.music ? "1" : "0");
    Reflect.setField(virtualRoot, "$detail", run.settings.detail ? "1" : "0");
    Reflect.setField(virtualRoot, "$shake", run.settings.shake ? "1" : "0");
    Reflect.setField(virtualRoot, "$version", Reflect.field(flash.system.Capabilities, Obfu.raw("version")));

    Reflect.setField(virtualRoot, Obfu.raw("setInterval"), Reflect.field(flash.Lib._global, Obfu.raw("setInterval")));
    Reflect.setField(virtualRoot, Obfu.raw("clearInterval"), Reflect.field(flash.Lib._global, Obfu.raw("clearInterval")));
    Reflect.setField(virtualRoot, Obfu.raw("parseFloat"), Reflect.field(flash.Lib._global, Obfu.raw("parseFloat")));

    return;
  }

  public function loadGameEngine(container:MovieClip, gameMetadata:Game, progress: LoaderProgress):Surprise<MovieClip, Error> {
    return Future.async(function(handler:Outcome<MovieClip, Error> -> Void):Void {
      var gameEngineMeta: GameEngine = gameMetadata.channels.active.build.engine;
      var gameEngineUri:Uri;
      switch (gameEngineMeta) {
        case GameEngine.V96: {
          gameEngineUri = this.api.resolveGameFileUri();
        }
        case GameEngine.Custom(blob): {
          gameEngineUri = this.api.resolveRawFileUri(blob.id);
        }
      }
      var gameEngine:MovieClip = FlashStd.addEmptyMovie(container, LoaderService.GAME_ENGINE_DEPTH);

      var containerUri = new Uri(container._url);
      if (gameEngineUri.getOrigin() != containerUri.getOrigin()) {
        handler(Outcome.Failure(new Error("InvalidGameEngineOrigin")));
        return;
      }

      var resolved:Bool = false;
      var mcl:MovieClipLoader = new MovieClipLoader();
      var gameEngineUriString:String = gameEngineUri.toString();

      mcl.onLoadError = function(target:MovieClip, error:String, httpStatus: Int):Void {
        if (!resolved) {
          resolved = true;
          handler(Outcome.Failure(new Error("Engine load error: " + error + "\nURI=" + gameEngineUriString)));
        }
      }

      mcl.onLoadInit = function(target:MovieClip):Void {
        if (!resolved) {
          resolved = true;
          progress.engine = Nil.some({current: target.getBytesLoaded(), total: target.getBytesTotal()});
          StdInc.init(target, this.compatibilityMode, container, container);
          handler(Outcome.Success(target));
        }
      }

      mcl.onLoadProgress = function(target:MovieClip, loadedBytes: Int, totalBytes: Int):Void {
        progress.engine = Nil.some({current: loadedBytes, total: totalBytes});
      }

      if (!mcl.loadClip(gameEngineUriString, gameEngine)) {
        resolved = true;
        handler(Outcome.Failure(new Error("Unable to start loading GameEngine\nURI=" + gameEngineUriString)));
      }
    });
  }

  public function getGameContent(gameMetadata:Game, progress: LoaderProgress):Surprise<Null<GameContent>, Error> {
    var gameContent: Null<Blob> = gameMetadata.channels.active.build.content.toNullable();
    if (gameContent == null) {
      return Future.sync(Outcome.Success(null));
    } else {
      return this.api.getXml(
        this.api.resolveRawFileUri(gameContent.id),
        function(loaded: Int, total: Int) {
          progress.content = Nil.some({current: loaded, total: total});
        }
      )
      .map(function(outcome:Outcome<Xml, Error>):Outcome<Null<GameContent>, Error> {
        return switch(outcome) {
          case Outcome.Failure(failure): Outcome.Failure(failure);
          case Outcome.Success(data): {
            try {
              var gameContent:GameContent = GameContent.fromXmlDocument(data, this.console);
              return Outcome.Success(gameContent);
            } catch (err:Error) {
              return Outcome.Failure(err);
            }
          }
        }
      });
    }
  }

  public function getMessagesPatch(gameMetadata: Game, locale: Locale, progress: LoaderProgress):Surprise<Null<Dictionary>, Error> {
    var build: GameBuild = gameMetadata.channels.active.build;
    var gameContentI18n: Null<Blob> = build.contentI18n.toNullable();
    if (gameContentI18n == null) {
      return Future.sync(Outcome.Success(null));
    } else {
      var buildI18n: Null<GameBuildI18n> = build.i18n.get(LocaleTools.stringify(locale));
      var newGameContentI18n: Null<Blob> = buildI18n != null ? buildI18n.contentI18n.toNullable() : null;
      if (newGameContentI18n != null) {
        gameContentI18n = newGameContentI18n;
      }

      return this.api.getXml(
        this.api.resolveRawFileUri(gameContentI18n.id),
        function(loaded: Int, total: Int) {
          progress.contentI18n = Nil.some({current: loaded, total: total});
        }
      )
      .map(function(outcome:Outcome<Xml, Error>):Outcome<Null<Dictionary>, Error> {
        try {
          return switch(outcome) {
            case Outcome.Failure(failure): Outcome.Failure(failure);
            case Outcome.Success(data): Outcome.Success(Dictionary.fromXmlDocument(locale, data, this.compatibilityMode));
          }
        } catch (err:Error) {
          return Outcome.Failure(err);
        }
      });
    }
  }

  public function getPatchedMessages(messages:Dictionary, gameContent:Null<GameContent>, messagesPatch:Null<Dictionary>):Dictionary {
    if (messagesPatch != null) {
      return messagesPatch;
    } else if (gameContent != null && gameContent.messagesXml != null) {
      return new Dictionary(messages.locale, gameContent.messagesXml);
    }
    return messages;
  }

  public function contentPatchGameEngine(gameEngine:MovieClip, gameContent:GameContent, compat:Bool):Void {
    if (gameContent.scoreItemsXml != null) {
      Reflect.setField(gameEngine, Obfu.raw("8pNA=("), gameContent.scoreItemsXml.toString());
      Reflect.setField(gameEngine, Obfu.raw("xml_scoreItems"), gameContent.scoreItemsXml.toString());
      if (compat) {
        Reflect.setField(gameEngine, Obfu.raw("scoreItemsXml"), gameContent.scoreItemsXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("xml_Objets1"), gameContent.scoreItemsXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("XML_SCORE_ITEMS"), gameContent.scoreItemsXml.toString());
      }
    }
    if (gameContent.specialItemsXml != null) {
      Reflect.setField(gameEngine, Obfu.raw("54q_)("), gameContent.specialItemsXml.toString());
      Reflect.setField(gameEngine, Obfu.raw("xml_specialItems"), gameContent.specialItemsXml.toString());
      if (compat) {
        Reflect.setField(gameEngine, Obfu.raw("specialItemsXml"), gameContent.specialItemsXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("xml_Objets2"), gameContent.specialItemsXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("XML_SPEC_ITEMS"), gameContent.specialItemsXml.toString());
      }
    }
    if (gameContent.linksXml != null) {
      Reflect.setField(gameEngine, Obfu.raw("=tt;U"), gameContent.linksXml.toString());
      Reflect.setField(gameEngine, Obfu.raw("xml_portalLinks"), gameContent.linksXml.toString());
      if (compat) {
        Reflect.setField(gameEngine, Obfu.raw("linksXml"), gameContent.linksXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("xml_Dimensions"), gameContent.linksXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("XML_LINKS"), gameContent.linksXml.toString());
      }
    }
    if (gameContent.questsXml != null) {
      Reflect.setField(gameEngine, Obfu.raw(";T45)"), gameContent.questsXml.toString());
      Reflect.setField(gameEngine, Obfu.raw("xml_quests"), gameContent.questsXml.toString());
      if (compat) {
        Reflect.setField(gameEngine, Obfu.raw("questsXml"), gameContent.questsXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("xml_Quetes"), gameContent.questsXml.toString());
        Reflect.setField(gameEngine, Obfu.raw("XML_QUESTS"), gameContent.questsXml.toString());
      }
    }

    var gameManager:Dynamic = getGameManager(gameEngine, compat);
    var levelHashes:Null<Dynamic> = null;
    if (Reflect.hasField(gameManager, Obfu.raw("] ("))) {
      levelHashes = Reflect.field(gameManager, Obfu.raw("] ("));
    } else if (Reflect.hasField(gameManager, Obfu.raw("HASHKEYS"))) {
      levelHashes = Reflect.field(gameManager, Obfu.raw("HASHKEYS"));
    } else {
      throw new Error("Failed to patch levels");
    }

    levelHashes.remove("$8d6fff6186db2e4f436852f16dcfbba8");
    levelHashes.remove("$3bb529b9fb9f62833d42c0c1a7b36a43");
    levelHashes.remove("$9fa2a5eb602c6e8df97aeff54eecce7b");
    levelHashes.remove("$041ccec10a38ecef7d4f5d7acb7b7c46");
    levelHashes.remove("$51cc93b000b284de6097c9319221e891");
    levelHashes.remove("$38fe1fbe22565c2f6691f1f666a600d9");
    levelHashes.remove("$0255841255b29dd91b88a57b4a27f422");
    levelHashes.remove("$1def3777b79eb80048ebf70a0ae83b77");
    levelHashes.remove("$a1a9405afb4576a3bceb75995ad17d09");
    levelHashes.remove("$8c49e07bc65be554538effb12eced2c2");
    levelHashes.remove("$bfe3df5761159d38a6419760d8613c26");

    for (levelSet in gameContent.dimensions) {
      Reflect.setField(gameEngine, levelSet.name, levelSet.data);
      var key: String = "$" + levelSet.signature.substr(0, 32);
      var value: String = "$" + levelSet.signature.substr(32, 64);
      levelHashes.set(key, value);
    }

    untyped _global.ASSetPropFlags(levelHashes, null, 7, 0);
  }

  public function loadMusics(container:MovieClip, gameMetadata:Game, runSettings:IRunSettings, progress: LoaderProgress):Surprise<Null<Array<Sound>>, Error> {
    if (runSettings.music == false || runSettings.volume == 0) {
      return Future.sync(Outcome.Success(null));
    }
    var musicsContainer:MovieClip = FlashStd.addEmptyMovie(container, LoaderService.MUSICS_DEPTH);
    var musicResources: Array<GameResource> = gameMetadata.channels.active.build.musics;
    var futures: Array<Surprise<Sound, Error>> = [];
    var musicProgress: Array<Progress> = [];
    for (musicResource in musicResources) {
      var curProgress = {current: 0, total: musicResource.blob.byteSize};
      var future = this.loadMusic(musicsContainer, this.api.resolveRawFileUri(musicResource.blob.id), curProgress);
      futures.push(future);
      musicProgress.push(curProgress);
    }
    progress.musics = musicProgress;

    return Future
    .ofMany(futures)
    .map(function(outcomes:Array<Outcome<Sound, Error>>):Outcome<Null<Array<Sound>>, Error> {
      var musics:Array<Sound> = [];
      for (outcome in outcomes) {
        switch (outcome) {
          case Outcome.Failure(failure): return Outcome.Failure(failure);
          case Outcome.Success(data): {
            musics.push(data);
          }
        }
      }
      while (musics.length < 3) {
        musics.push(new Sound(container));
        musics.push(new Sound(container));
        musics.push(new Sound(container));
      }
      return Outcome.Success(musics);
    });
  }

  public function loadMusic(container:MovieClip, musicUri:Uri, progress: Progress):Surprise<Sound, Error> {
    return Future.async(function(handler:Outcome<Sound, Error> -> Void):Void {
      var resolved:Bool = false;
      var sound:Sound = new Sound(container);
      var musicUriString = musicUri.toString();

      var progressNotifier = function():Void {
        // Check `getBytesLoaded` is also needed to get the `onLoad` event reliably
        var loaded: Null<Float> = sound.getBytesLoaded();
        var total: Null<Float> = sound.getBytesTotal();
        if (loaded != null && total != null) {
          progress.current = Std.int(loaded);
          progress.total = Std.int(total);
        }
      }
      FlashStd.addEnterFrameListener(progressNotifier);

      sound.onLoad = function(success:Bool):Void {
        if (resolved) {
          return;
        }
        resolved = true;
        progressNotifier();
        FlashStd.removeEnterFrameListener(progressNotifier);

        if (success) {
          handler(Outcome.Success(sound));
        } else {
          handler(Outcome.Failure(new Error("Unable to load music\nURI=" + musicUriString)));
        }
      }

      Timer.delay(function() {
        if (resolved) {
          return;
        }
        resolved = true;
        FlashStd.removeEnterFrameListener(progressNotifier);

        handler(Outcome.Failure(new Error("Unable to load music\nURI=" + musicUriString)));
      }, LoaderService.LOAD_MUSIC_TIMEOUT);

      sound.loadSound(musicUriString, false);
    });
  }

  public function loadEnginePatcher(container:MovieClip, gameMetadata:Game, progress: LoaderProgress):Surprise<Null<MovieClip>, Error> {
    return Future.async(function(handler:Outcome<Null<MovieClip>, Error> -> Void):Void {
      var patcherMeta: Null<GamePatcher> = gameMetadata.channels.active.build.patcher.toNullable();
      if (patcherMeta == null) {
        handler(Outcome.Success(null));
        return;
      }

      if (Reflect.hasField(flash.Lib._global, Obfu.raw("external"))) {
        handler(Outcome.Failure(new Error("Global `external` variable is already used")));
      }

      var enginePatcherUri:Uri = this.api.resolveRawFileUri(patcherMeta.blob.id);
      var enginePatcher:MovieClip = FlashStd.addEmptyMovie(container, LoaderService.ENGINE_PATCHER_DEPTH);

      var containerUri = new Uri(container._url);
      if (enginePatcherUri.getOrigin() != containerUri.getOrigin()) {
        handler(Outcome.Failure(new Error("InvalidEnginePatcherOrigin")));
        return;
      }

      var resolved:Bool = false;
      var mcl:MovieClipLoader = new MovieClipLoader();
      var enginePatcherUriString:String = enginePatcherUri.toString();

      mcl.onLoadError = function(target:MovieClip, error:String, httpStatus: Int):Void {
        if (!resolved) {
          resolved = true;
          handler(Outcome.Failure(new Error("Patcher load error: " + error + "\nURI=" + enginePatcherUriString)));
        }
      }

      mcl.onLoadProgress = function(target:MovieClip, loadedBytes: Int, totalBytes: Int):Void {
        progress.patcher = Nil.some({current: loadedBytes, total: totalBytes});
      }

      mcl.onLoadInit = function(target:MovieClip):Void {
        if (!resolved) {
          resolved = true;
          progress.engine = Nil.some({current: target.getBytesLoaded(), total: target.getBytesTotal()});
          if (Reflect.hasField(flash.Lib._global, Obfu.raw("external"))) {
            var globalExternal:Dynamic = Reflect.field(flash.Lib._global, Obfu.raw("external"));
            Reflect.deleteField(flash.Lib._global, Obfu.raw("external"));
            if (!Reflect.hasField(target, Obfu.raw("external"))) {
              Reflect.setField(target, Obfu.raw("external"), globalExternal);
            }
          }

          if (!Reflect.hasField(target, Obfu.raw("ENGINE_PATCHER_VERSION"))) {
            this.console.log("Engine patcher version not found");
            handler(Outcome.Success(null));
            return;
          }
          if (!Reflect.hasField(target, Obfu.raw("main")) || !Reflect.isFunction(Reflect.field(target, Obfu.raw("main")))) {
            this.console.log("Engine patcher entry point not found");
            handler(Outcome.Success(null));
            return;
          }
          handler(Outcome.Success(target));
        }
      }

      if (!mcl.loadClip(enginePatcherUriString, enginePatcher)) {
        resolved = true;
        handler(Outcome.Failure(new Error("Unable to start loading EnginePatcher\nURI=" + enginePatcherUriString)));
      }
    });
  }

  public function applyEnginePatcher(
    contentPatchedGameEngine:MovieClip,
    enginePatcher:Null<MovieClip>,
    devtools:Null<Devtools>,
    gameContent:Null<GameContent>,
    gameEventsHandler: GameEventsHandler,
    run:IRun
  ):Surprise<MovieClip, Error> {
    return Future.async(function(handler:Outcome<Null<MovieClip>, Error> -> Void):Void {
      if (enginePatcher == null) {
        handler(Outcome.Success(contentPatchedGameEngine));
        return;
      }
      var doneCb:Null<Error> -> Void = function(err) {
        if (err == null) {
          handler(Outcome.Success(contentPatchedGameEngine));
        } else {
          handler(Outcome.Failure(err));
        }
      }
      var ml = new ModuleLoader(
        devtools,
        contentPatchedGameEngine,
        gameContent,
        gameEventsHandler,
        run,
        function (): Null<IRunStart> { return this.runStart; },
        doneCb
      );
      Reflect.callMethod(enginePatcher, Reflect.field(enginePatcher, Obfu.raw("main")), [ml, doneCb]);
    });
  }

  private function patchInternalOnBeyondLastLevel(gameEngine:MovieClip):Void {
    var onGameEndPath = this.getInternalOnGameEnd(gameEngine);
    var onGameEndName = onGameEndPath[onGameEndPath.length - 1];
    var getPlayersPath = this.getInternalGetPlayers(gameEngine);
    var getPlayersName = getPlayersPath[getPlayersPath.length - 1];
    var setPlayerScorePath = this.getInternalSetPlayerScore(gameEngine);
    var setPlayerScoreName = setPlayerScorePath[setPlayerScorePath.length - 1];
    var path = this.getInternalOnBeyondLastLevel(gameEngine);
    LoaderService.setDeepField(gameEngine, path, function() {
      this.isVictory = true;
      var players:Array<Dynamic> = Reflect.callMethod(untyped __this__, Reflect.field(untyped __this__, getPlayersName), []);
      for (i in 0...(players.length)) {
        var player:Dynamic = players[i];
        var pid:Int = this.getInternalPlayerPid(player);
        var score:Int = this.getInternalPlayerScore(player);
        Reflect.callMethod(untyped __this__, Reflect.field(untyped __this__, setPlayerScoreName), [pid, score]);
      }
      Reflect.callMethod(untyped __this__, Reflect.field(untyped __this__, onGameEndName), []);
      this.isVictory = false;
    });
  }

  private function patchInternalOnGameEnd(gameEngine:MovieClip):Void {
    var path = this.getInternalOnGameEnd(gameEngine);
    var oldFn = LoaderService.getDeepField(gameEngine, path);
    LoaderService.setDeepField(gameEngine, path, function() {
      this.setTrappedArrayRemove();
      Reflect.callMethod(untyped __this__, oldFn, []);
      this.setOriginalArrayRemove();
    });
  }

  private function getInternalOnGameEnd(gameEngine:MovieClip):Array<String> {
    var paths:Array<Array<String>> = [
      [Obfu.raw("mode"), Obfu.raw("4(IJ0("), Obfu.raw("prototype"), Obfu.raw("}(Rc5")]
    ];
    if (this.compatibilityMode) {
      paths.push([Obfu.raw("mode"), Obfu.raw("Adventure"), Obfu.raw("prototype"), Obfu.raw("saveScore")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("AdventureSolo"), Obfu.raw("prototype"), Obfu.raw("getDataAndSave")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw("}(Rc5")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("class_ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw("}(Rc5")]);
    }
    for (path in paths) {
      var cur:Dynamic = gameEngine;
      for (component in path) {
        cur = Reflect.field(cur, component);
        if (cur == null) {
          break;
        }
      }
      if (cur != null) {
        return path;
      }
    }
    throw new Error("InternalOnGameEndNotFound");
  }

  private function getInternalGetPlayers(gameEngine:MovieClip):Array<String> {
    var paths:Array<Array<String>> = [
      [Obfu.raw("mode"), Obfu.raw("AdventureSolo"), Obfu.raw("prototype"), Obfu.raw("getPlayers")],
    ];
    if (this.compatibilityMode) {
      paths.push([Obfu.raw("mode"), Obfu.raw("ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw("getPlayers")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("class_ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw("func_getPlayers")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("4(IJ0("), Obfu.raw("prototype"), Obfu.raw(";uMMG(")]);
    }
    for (path in paths) {
      var cur:Dynamic = gameEngine;
      for (component in path) {
        cur = Reflect.field(cur, component);
        if (cur == null) {
          break;
        }
      }
      if (cur != null) {
        return path;
      }
    }
    throw new Error("InternalGetPlayersNotFound");
  }

  private function getInternalSetPlayerScore(gameEngine:MovieClip):Array<String> {
    var paths:Array<Array<String>> = [
      [Obfu.raw("mode"), Obfu.raw("AdventureSolo"), Obfu.raw("prototype"), Obfu.raw("setPlayerScore")],
    ];
    if (this.compatibilityMode) {
      paths.push([Obfu.raw("mode"), Obfu.raw("ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw(",yk2 (")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("class_ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw(",yk2 (")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("4(IJ0("), Obfu.raw("prototype"), Obfu.raw(",yk2 (")]);
    }
    for (path in paths) {
      var cur:Dynamic = gameEngine;
      for (component in path) {
        cur = Reflect.field(cur, component);
        if (cur == null) {
          break;
        }
      }
      if (cur != null) {
        return path;
      }
    }
    throw new Error("InternalSetPlayerScoreNotFound");
  }

  private function getInternalPlayerPid(player:Dynamic):Int {
    var fields:Array<String> = [Obfu.raw("pid")];
    if (this.compatibilityMode) {
      fields.push(Obfu.raw("5,F"));
    }
    for (field in fields) {
      var value:Null<Int> = Reflect.field(player, field);
      if (value != null) {
        return value;
      }
    }
    throw new Error("InternalPlayerPidNotFound");
  }

  private function getInternalPlayerScore(player:Dynamic):Int {
    var fields:Array<String> = [Obfu.raw("score")];
    if (this.compatibilityMode) {
      fields.push(Obfu.raw("(Erk5"));
    }
    for (field in fields) {
      var value:Null<Int> = Reflect.field(player, field);
      if (value != null) {
        return value;
      }
    }
    throw new Error("InternalPlayerScoreNotFound");
  }

  private function getInternalOnBeyondLastLevel(gameEngine:MovieClip):Array<String> {
    var paths:Array<Array<String>> = [
      [Obfu.raw("mode"), Obfu.raw("AdventureSolo"), Obfu.raw("prototype"), Obfu.raw("onBeyondLastLevel")],
    ];
    if (this.compatibilityMode) {
      paths.push([Obfu.raw("mode"), Obfu.raw("ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw("*HvkI(")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("class_ModeAventureSolo"), Obfu.raw("prototype"), Obfu.raw("*HvkI(")]);
      paths.push([Obfu.raw("mode"), Obfu.raw("4(IJ0("), Obfu.raw("prototype"), Obfu.raw("*HvkI(")]);
    }
    for (path in paths) {
      var cur:Dynamic = gameEngine;
      for (component in path) {
        cur = Reflect.field(cur, component);
        if (cur == null) {
          break;
        }
      }
      if (cur != null) {
        return path;
      }
    }
    throw new Error("InternalOnBeyondLastLevelNotFound");
  }

  private function setTrappedArrayRemove():Void untyped {
    #if flash8
    _global.ASSetPropFlags(Array.prototype, Obfu.raw("remove"), 0, 7);
    Reflect.setField(Array.prototype, Obfu.raw("remove"), LoaderService._trappedArrayRemove);
    _global.ASSetPropFlags(Array.prototype, Obfu.raw("remove"), 7, 0);
    if (isCompat) {
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("0ENPA"), 0, 7);
      Reflect.setField(Array.prototype, Obfu.raw("0ENPA"), LoaderService._trappedArrayRemove);
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("0ENPA"), 7, 0);
    }
    #end
  }

  private function setOriginalArrayRemove():Void untyped {
    #if flash8
    _global.ASSetPropFlags(Array.prototype, Obfu.raw("remove"), 0, 7);
    Reflect.setField(Array.prototype, Obfu.raw("remove"), LoaderService._originalArrayRemove);
    _global.ASSetPropFlags(Array.prototype, Obfu.raw("remove"), 7, 0);
    if (this.compatibilityMode) {
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("0ENPA"), 0, 7);
      Reflect.setField(Array.prototype, Obfu.raw("0ENPA"), LoaderService._originalArrayRemove);
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("0ENPA"), 7, 0);
    }
    #end
  }

  private static function _trappedArrayRemove(obj:Dynamic):Bool untyped {
    __this__.length = 0;
    return true;
  }

  // Trapped remove function used to patch the internal onGameLost function
  private static function _originalArrayRemove(obj:Dynamic):Bool untyped {
    var i = 0;
    var l = __this__.length;
    while (i < l) {
      if (__this__[i] == obj) {
        __this__.splice(i, 1);
        return true;
      }
      i++;
    }
    return false;
  }

  public function createGameManager(
    gameEngine:MovieClip,
    messages:Dictionary,
    musics:Null<Array<Sound>>,
    gameOptions:Array<String>,
    runStart:IRunStart,
    gameEventsHandler:GameEventsHandler
  ):Null<IManager> {
    this.runStart = runStart;
    this.patchInternalOnGameEnd(gameEngine);
    this.patchInternalOnBeyondLastLevel(gameEngine);

    var isProduction = false;
    var managerOptions:ManagerOptions = ManagerOptions.fromStruct({
      messagesXmlString: messages.getXmlString(),
      isDevServer: false,
      familiesString: runStart.families,
      gameOptionsString:gameOptions.join(","),
      musics: musics == null ? [] : musics
    });
    if (this.compatibilityMode) {
      ManagerOptions.compat(managerOptions);
    }

    try {
      var ctr:Dynamic = getGameManager(gameEngine, this.compatibilityMode);
      var manager:IManager = untyped __new__(ctr, gameEngine, managerOptions);
      if (manager == null) {
        throw new Error("An unknown error occured during the creation of the game manager");
      }
      return manager;
    } catch (err:Dynamic) {
      throw err;
    }
  }

  private function patchOnGameEnd(global:Dynamic, gameMode:String, handler:GameEventsHandler):Void {
    var wrappedHandler = function(scoreIgor:Int, scoreSandy:Int, onGameEndData:Dynamic):Void {
      this.setOriginalArrayRemove();
      if (gameMode == Obfu.raw("solo")) {
        var items:Map<String, Int> = new Map();
        var rawItems:Dynamic = Reflect.field(onGameEndData, "$item2");
        for (itemId in Reflect.fields(rawItems)) {
          items.set(itemId, Reflect.field(rawItems, itemId));
        }
        handler.onGameEnd(new SetRunResultOptions(
          this.isVictory,
          Reflect.field(onGameEndData, "$reachedLevel"),
          [scoreIgor],
          items,
          this.stats
        ));
      } else if (gameMode == Obfu.raw("multicoop")) {
        var items:Map<String, Int> = new Map();
        var rawItems:Dynamic = Reflect.field(onGameEndData, "$item2");
        for (itemId in Reflect.fields(rawItems)) {
          items.set(itemId, Reflect.field(rawItems, itemId));
        }
        handler.onGameEnd(new SetRunResultOptions(
          this.isVictory,
          Reflect.field(onGameEndData, "$reachedLevel"),
          [Reflect.field(Reflect.field(onGameEndData, "$data"), "$s0"), Reflect.field(Reflect.field(onGameEndData, "$data"), "$s1")],
          items,
          this.stats
        ));
      } else {
        handler.onGameCrash(new etwin.Error("Unexpected game mode: " + gameMode));
      }
    }
    if (Reflect.hasField(global, "gameOver")) {
      throw new Error("gameOver is already registered");
    }
    Reflect.setField(global, "gameOver", wrappedHandler);
    if (this.compatibilityMode) {
      Reflect.setField(global, Obfu.raw("onGameEnd"), wrappedHandler);
      Reflect.setField(global, Obfu.raw("saveGameResultData"), wrappedHandler);
    }
  }

  private function patchOnGameCrash(global: Dynamic, handler: OnGameCrashHandler):Void {
    if (Reflect.hasField(global, "exitGame")) {
      throw new Error("exitGame is already registered");
    }

    // The game engine calls this function without any arguments.
    // This function wraps it to provide more context to the handler.
    var inner = function() {
        handler(new etwin.Error("`exitGame` signal received"));
    }

    Reflect.setField(global, "exitGame", inner);
    if (this.compatibilityMode) {
      Reflect.setField(global, Obfu.raw("onGameCrash"), inner);
      Reflect.setField(global, Obfu.raw("postEndGameItems"), inner);
    }
  }

  private static function getGameManager(gameMc: MovieClip, compat: Bool):Dynamic {
    var mgrCtr: Null<Dynamic> = null;
    if (Reflect.hasField(gameMc, "GameManager")) {
      mgrCtr = Reflect.field(gameMc, "GameManager");
    } else if (Reflect.hasField(gameMc, Obfu.raw("GameManager"))) {
      mgrCtr = Reflect.field(gameMc, Obfu.raw("GameManager"));
    } else if (compat && Reflect.hasField(gameMc, Obfu.raw("Manager"))) {
      mgrCtr = Reflect.field(gameMc, Obfu.raw("Manager"));
    }
    if (mgrCtr == null) {
      throw new Error("GameManagerNotFound");
    }
    var mgrProto:Dynamic = Reflect.field(mgrCtr, "prototype");
    var mainField: Null<String> = null;
    if (Reflect.hasField(mgrProto, "main")) {
      mainField = "main";
    } else if (Reflect.hasField(mgrProto, Obfu.raw("main"))) {
      mainField = Obfu.raw("main");
    } else if (compat && Reflect.hasField(mgrProto, Obfu.raw("onNewFrame"))) {
      mainField = Obfu.raw("onNewFrame");
    }
    if (mainField == null) {
      throw new Error("BadGameManager");
    }
    if (mainField != "main") {
      Reflect.setField(mgrProto, "main", function () {
        Reflect.callMethod(
          untyped __this__,
          Reflect.field(untyped __this__, mainField),
          untyped __arguments__
        );
      });
    }
    return mgrCtr;
  }

  public static function getDeepField(obj:Dynamic, fields:Array<String>):Dynamic {
    var cur:Dynamic = obj;
    for (field in fields) {
      cur = Reflect.field(cur, field);
    }
    return cur;
  }

  public static function setDeepField(obj:Dynamic, fields:Array<String>, value:Dynamic):Void {
    var cur:Dynamic = obj;
    for (i in 0...(fields.length - 1)) {
      cur = Reflect.field(cur, fields[i]);
    }
    Reflect.setField(cur, fields[fields.length - 1], value);
  }

  public static function ensureGameEventsOnce(handler: GameEventsHandler):GameEventsHandler {
    var done:Bool = false;
    return {
      onGameEnd: function(result:ISetRunResultOptions):Void {
        if (!done) {
          done = true;
          handler.onGameEnd(result);
        }
      },
      onGameCrash: function(error: etwin.Error): Void {
        if (!done) {
          done = true;
          handler.onGameCrash(error);
        }
      }
    };
  }
}
