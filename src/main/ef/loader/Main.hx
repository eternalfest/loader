package ef.loader;

#if !(flash8 || flash)
#error
#end

import devtools.server.Devtools;
import ef.api.run.ISetRunResultOptions;
import ef.api.ApiRoutes;
import ef.api.run.Run;
import ef.api.run.IRun;
import ef.api.run.RunOptions;
import ef.patcher.IConsoleModule;
import ef.loader.Version;
import ef.patcher.LocalConsoleModule;
import ef.patcher.NoopConsoleModule;
import ef.patcher.SwfSocketConsoleModule;
import ef.loader.LoaderParameters;
import FlashStd;
import haxe.Json;
import swf_socket.SwfSocket;
import swf_socket.SwfSocketClient;
import mt.hfest.IManager;
import ef.loader.i18n.Dictionary;
import ef.loader.i18n.Locale;
import net.demurgos.uri.Uri;
import tink.core.Error;
import Reflect;
import etwin.Obfu;
#if flash8
import etwin.flash.MovieClip;
import ef.api.HttpClient;
import ef.loader.views.Uberjudgement;
import ef.api.Api;
import tink.core.Future;
import tink.core.Outcome;
#else
import openfl.system.Security;
import ef.loader.views.uberjudgement.LoaderView;
import format.SVG;
import openfl.Assets;
import openfl.display.Sprite;
#end

class Main #if !flash8 extends Sprite #end {
  #if flash8
  public static var CONSOLE: IConsoleModule;
  public static var IS_DEBUG: Bool;

  public static function main(): Void {
    {
      // flasm game.swf
      // ```
      // push "Hello, World!", 1, 'hxTrace'
      // callFunction
      // pop
      // ```
      // flasm -a game.flm
      Reflect.setField(flash.Lib._global, etwin.Obfu.raw("hxTrace"), function(x: Dynamic): Void {
        Main.CONSOLE.log(x);
        // haxe.Log.trace(x);
      });
    }

    haxe.Log.setColor(0xffffff);

    var parameters: LoaderParameters;
    try {
      parameters = Main.getParameters();
    } catch (err: Error) {
      trace(err);
      return;
    }

    var swfSocketClient: Null<SwfSocketClient> = null;
    if (parameters.swfSocketServer != null && parameters.objectId != null) {
      switch(SwfSocket.connect(parameters.swfSocketServer, parameters.objectId)) {
        case Outcome.Failure(err):
          trace(err);
          return;
        case Outcome.Success(client):
          swfSocketClient = client;
      }
    }

    var devtools: Null<Devtools> = null;
    if (swfSocketClient != null) {
      devtools = new Devtools(swfSocketClient);
      devtools.runtime.consoleAPI.log("Starting to load the game");
      devtools.runtime.consoleAPI.log("Loader version: " + Version.get());
    }

    var loaderUriStr: Null<String> = Reflect.field(flash.Lib.current, "_url");
    if (loaderUriStr != null) {
      var loaderUri: Uri = new Uri(loaderUriStr);
      Main.IS_DEBUG = loaderUri.getHostname() == Obfu.raw("localhost");
    }

    if (!Main.IS_DEBUG) {
      Main.CONSOLE = new NoopConsoleModule();
    } else if (devtools != null) {
      Main.CONSOLE = new SwfSocketConsoleModule(devtools.runtime.consoleAPI);
    } else {
      Main.CONSOLE = new LocalConsoleModule();
    }

    Main.load(parameters.apiBase, parameters.gameUri, parameters.run, parameters.locale, swfSocketClient, devtools);
  }

  public static function load(apiBase: Uri, gameUri: Uri, run: IRun, locale: String, swfSocketClient: Null<SwfSocketClient>, devtools: Null<Devtools>): Void {
    var locale: Locale = switch(locale) {
      case "es-SP":
        Locale.EsSp;
      case "fr-FR":
        Locale.FrFr;
      default:
        Locale.EnUs;
    }
    var dictionary = Dictionary.getDictionary(locale);

    var httpClient: HttpClient = new HttpClient();
    var apiRoutes: ApiRoutes = new ApiRoutes(apiBase);
    var api: Api = new Api(httpClient, apiRoutes);
    var loaderService: LoaderService = new LoaderService(api, Main.CONSOLE, Main.IS_DEBUG);

    var currentMc: MovieClip = cast flash.Lib.current;
    var virtualRoot: MovieClip = FlashStd.addEmptyMovie(currentMc, 100);
    var loaderRoot: MovieClip = FlashStd.addEmptyMovie(currentMc, 101);

    var loaderView = new Uberjudgement(loaderRoot);

    var loaderTask: LoaderTask = loaderService.load({
      swfSocketClient: swfSocketClient,
      devtools: devtools,
      virtualRoot: virtualRoot,
      gameUri: gameUri,
      locale: locale,
      gameEventsHandler: {
        onGameEnd: function(result: ISetRunResultOptions): Void {
          Main.onEnd(loaderRoot, api, run.id, result);
        },
        onGameCrash: function(error: etwin.Error): Void {
          Main.onCrash(loaderRoot, error);
        },
      },
      run: run,
      awaitStartGameSignal: function(): Future<Dynamic> {
        return loaderView.awaitStartGameSignal();
      }
    });

    loaderView.setLoaderTask(loaderTask);

    loaderTask.handle(function(outcome: Outcome<Dynamic, Error>): Void {
      switch(outcome) {
        case Outcome.Success(data): {
          loaderView.destroy();
          var gameManager: IManager = data;
          var onEnterFrame: Void -> Void = null;
          onEnterFrame = function() {
            try {
              gameManager.main();
            } catch (e: Dynamic) {
              FlashStd.removeEnterFrameListener(onEnterFrame);
              // TODO: `Reflect.is(e, etwin.Error)` and wrap into an error if needed
              Main.onCrash(loaderRoot, e);
            }
          }
          FlashStd.addEnterFrameListener(onEnterFrame);
        };
        case Outcome.Failure(failure): {
          trace("Load error");
          Main.CONSOLE.warn(failure);
        }
      };
      return;
    });
  }

  public static function getParameters(): LoaderParameters {
    var loaderUriStr: Null<String> = Reflect.field(flash.Lib.current, "_url");
    if (loaderUriStr == null) {
      throw new Error("Unable to retrieve loader URI");
    }
    var loaderUri: Uri = new Uri(loaderUriStr);
    var loaderOrigin: String = loaderUri.getOrigin();

    var gameId: Null<String> = Reflect.field(flash.Lib.current, Obfu.raw("game"));

    if (gameId == null) {
      throw new Error("Missing parameter: game");
    }

    var gameUriStr = loaderOrigin + "/api/v1/games/" + gameId;
    var apiBaseStr = loaderOrigin + "/api/v1";

    var runOptions: Null<String> = Reflect.field(flash.Lib.current, Obfu.raw("options"));
    if (runOptions == null) {
      throw new Error("Missing parameter: options");
    }

    var run: Null<String> = Reflect.field(flash.Lib.current, Obfu.raw("run"));
    if (run == null) {
      throw new Error("Missing parameter: run");
    }

    var swfSocketServer: Null<String> = Reflect.hasField(flash.Lib.current, Obfu.raw("server")) ? Reflect.field(flash.Lib.current, Obfu.raw("server")) : null;
    var swfSocketObjectId: Null<String> = Reflect.hasField(flash.Lib.current, Obfu.raw("object_id")) ? Reflect.field(flash.Lib.current, Obfu.raw("object_id")) : null;

    return {
      apiBase: new Uri(apiBaseStr),
      gameUri: new Uri(gameUriStr),
      run: Run.readJson(Json.parse(run)),
      locale: RunOptions.readJson(Json.parse(runOptions)).locale,
      swfSocketServer: swfSocketServer,
      objectId: swfSocketObjectId
    };
  }

  public static function onEnd(loaderRoot: Dynamic, api: Api, runId: String, result: ISetRunResultOptions) {
    var loaderView = new Uberjudgement(loaderRoot);
    loaderView.mc._alpha = 0;

    var onEnterFrame: Void -> Void = null;
    onEnterFrame = function() {
      loaderView.mc._alpha = Math.min(100, loaderView.mc._alpha + 5);
      if (loaderView.mc._alpha == 100) {
        FlashStd.removeEnterFrameListener(onEnterFrame);
        api.setRunResult(runId, result).handle(function() {
          loaderView.setGameEnd();
          if (!Main.IS_DEBUG) {
            flash.Lib.getURL("/runs/" + runId, "_self");
          } else {
            Main.CONSOLE.log(result);
          }
        });
      }
    }
    FlashStd.addEnterFrameListener(onEnterFrame);
  }

  public static function onCrash(loaderRoot: Dynamic, error: Null<etwin.Error>) {
    if (error != null) {
      Main.CONSOLE.error("FATAL ERROR: " + error);
      Main.CONSOLE.error(error);
    }

    var loaderView = new Uberjudgement(loaderRoot);
    loaderView.mc._alpha = 0;

    var onEnterFrame: Void -> Void = null;
    onEnterFrame = function() {
      loaderView.mc._alpha = Math.min(100, loaderView.mc._alpha + 5);
      if (loaderView.mc._alpha == 100) {
        FlashStd.removeEnterFrameListener(onEnterFrame);
        trace(etwin.Obfu.raw("UnexpectedGameEnd"));
      }
    }
    FlashStd.addEnterFrameListener(onEnterFrame);
  }

  #else

  public function new() {
    super();
    Security.allowDomain("*");
    Security.allowInsecureDomain("*");

    trace("Flash 11 loader");
    var raysSvg = new SVG(Assets.getText("ef/loader/back.svg"));
    trace(raysSvg);

    // var loader: Loader = new Loader(this);
    var view:LoaderView = new LoaderView();
    this.addChild(view);
    // loader.load().handle(function(outcome){});
  }
  #end
}
