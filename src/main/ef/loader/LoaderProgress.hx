package ef.loader;

import etwin.ds.Nil;
import ef.loader.Progress;

class LoaderProgress {
  public var engine: Nil<Progress>;
  public var content: Nil<Progress>;
  public var contentI18n: Nil<Progress>;
  public var patcher: Nil<Progress>;
  public var musics: Array<Progress>;

  public function new() {
    this.engine = Nil.none();
    this.content = Nil.none();
    this.contentI18n = Nil.none();
    this.patcher = Nil.none();
    this.musics = [];
  }
}
