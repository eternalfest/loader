package ef.loader.views.uberjudgement;

import format.SVG;
import openfl.Assets;
import openfl.display.Shape;
import openfl.display.Sprite;

class LoaderView extends Sprite implements ILoaderView {
  private var background: Shape;

  public function new() {
    super();
    this.attachBackground();
  }

  private function attachBackground(): Void {
    this.background = new Shape();
    var raysSvg = new SVG(Assets.getText("ef/loader/back.svg"));
    var eyeSvg = new SVG(Assets.getText("ef/loader/logo.svg"));
    raysSvg.render(this.background.graphics);
    eyeSvg.render(this.background.graphics);
    this.addChild(this.background);
  }
}
