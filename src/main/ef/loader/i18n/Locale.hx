package ef.loader.i18n;

enum Locale {
  EnUs;
  EsSp;
  FrFr;
}

class LocaleTools {
  public static function stringify(value: Locale): String {
    switch (value) {
      case Locale.EnUs: return "en-US";
      case Locale.EsSp: return "es-SP";
      case Locale.FrFr: return "fr-FR";
    }
  }
}
