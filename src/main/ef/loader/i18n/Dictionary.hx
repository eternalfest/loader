package ef.loader.i18n;

import ef.loader.i18n.Locale;
import tink.core.Error;
import etwin.Obfu;

class Dictionary {
  public var locale(default, null):Locale;
  private var xml:Xml;
  private var statics:Map<Int, String>;
  private var families:Map<Int, String>;
  private var items:Map<Int, String>;

  public function new(locale:Locale, messagesNode:Xml) {
    this.locale = locale;
    this.xml = messagesNode;

    this.statics = new Map<Int, String>();
    this.families = new Map<Int, String>();
    this.items = new Map<Int, String>();

    if (messagesNode.nodeType != Xml.Element || messagesNode.nodeName != Obfu.raw("messages")) {
      throw new Error("Assertion: Expected `messagesNode` to be <messages> XML Element");
    }
    for (child in messagesNode.elements()) {
      if (child.nodeType != Xml.Element) {
        continue;
      }
      if (child.nodeName == Obfu.raw("statics")) {
        this.populate(child, this.statics, Obfu.raw("t"), Obfu.raw("id"), Obfu.raw("v"));
      } else if (child.nodeName == Obfu.raw("families")) {
        this.populate(child, this.families, Obfu.raw("family"), Obfu.raw("id"), Obfu.raw("name"));
      } else if (child.nodeName == Obfu.raw("items")) {
        this.populate(child, this.items, Obfu.raw("item"), Obfu.raw("id"), Obfu.raw("name"));
      } else {
        continue;
      }
    }
  }

  private function populate(rootNode:Xml, map:Map<Int, String>, elemName:String, idAttr:String, textAttr:Null<String>):Void {
    for (node in rootNode.elements()) {
      if (node.nodeType != Xml.Element || node.nodeName != elemName) {
        continue;
      }
      var idString:Null<String> = node.get(idAttr);
      var text:Null<String> = node.get(textAttr);
      if (idString == null || text == null) {
        continue;
      }
      var id:Null<Int> = Std.parseInt(idString);
      if (id == null) {
        continue;
      }
      map.set(id, text);
    }
  }

  public function getStatic(id:Int):String {
    var text:Null<String> = this.statics.get(id);
    if (text == null) {
      if (this.locale != Dictionary.DEFAULT_LOCALE) {
        text = Dictionary.getDictionary(Dictionary.DEFAULT_LOCALE).getStatic(id);
      } else {
        text = "?";
      }
    }
    return text;
  }

  public function getXmlString():String {
    return this.xml.toString();
  }

  private static var DEFAULT_LOCALE:Locale = Locale.EnUs;
  private static var _dictionaries:Map<Locale, Dictionary> = new Map<Locale, Dictionary>();

  public static function fromXmlDocument(locale:Locale, xmlDocument:Xml, compat:Bool = false):Dictionary {
    if (xmlDocument.nodeType != Xml.Document) {
      throw new Error("Assertion: Expected `xmlDocument.nodeType` to be `Xml.Document`");
    }
    var messagesNode:Null<Xml> = xmlDocument.firstElement();
    if (messagesNode != null && messagesNode.nodeType == Xml.Element && messagesNode.nodeName == Obfu.raw("lang")) {
      if (compat) {
        messagesNode.nodeName = Obfu.raw("messages");
      }
    }
    if (messagesNode == null || messagesNode.nodeType != Xml.Element || messagesNode.nodeName != Obfu.raw("messages")) {
      throw new Error("Assertion: Expected <messages> XML Element as first child of `xmlDocument`");
    }
    return new Dictionary(locale, messagesNode);
  }

  public static function getDictionary(locale:Locale):Dictionary {
    var dictionary:Null<Dictionary> = Dictionary._dictionaries.get(locale);
    if (dictionary == null) {
      dictionary = Dictionary.fromXmlDocument(locale, Dictionary.loadXml(locale));
      Dictionary._dictionaries.set(locale, dictionary);
    }
    return dictionary;
  }

  private static function loadXml(locale:Locale):Xml {
    return Xml.parse(Dictionary.loadXmlString(locale));
  }

  private static function loadXmlString(locale:Locale):String {
    #if flash8
    return switch (locale) {
      case Locale.EnUs: haxe.Resource.getString("mt/hfest/messages.en-US.xml");
      case Locale.EsSp: haxe.Resource.getString("mt/hfest/messages.es-SP.xml");
      case Locale.FrFr: haxe.Resource.getString("mt/hfest/messages.fr-FR.xml");
      default: throw new Error("UnknownLocale");
    }
    #else
    return switch (locale) {
      case Locale.EnUs: openfl.Assets.getText("mt/hfest/messages.en-US.xml");
      case Locale.EsSp: openfl.Assets.getText("mt/hfest/messages.es-SP.xml");
      case Locale.FrFr: openfl.Assets.getText("mt/hfest/messages.fr-FR.xml");
      default: throw new Error("UnknownLocale");
    }
    #end
  }
}
