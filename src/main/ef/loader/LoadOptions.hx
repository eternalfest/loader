package ef.loader;

import devtools.server.Devtools;
import ef.api.run.IRun;
import ef.loader.i18n.Locale;
import net.demurgos.uri.Uri;
import swf_socket.SwfSocketClient;
import tink.core.Future;

#if flash8
import etwin.flash.MovieClip;
#else
import flash.display.MovieClip;
#end

typedef LoadOptions = {
  swfSocketClient: Null<SwfSocketClient>,
  devtools: Null<Devtools>,
  virtualRoot: MovieClip,
  gameUri: Uri,
  locale: Locale,
  gameEventsHandler: GameEventsHandler,
  run: IRun,
  awaitStartGameSignal: Void -> Future<Dynamic>
}
