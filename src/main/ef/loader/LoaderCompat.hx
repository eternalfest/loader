package ef.loader;

import flash.MovieClip;
import flash.Sound;
import haxe.Json;
import Reflect;
import etwin.Obfu;
import tink.core.Error;

class LoaderCompat {
  public static var NO_OP = function() {
  };

  public var prepareGame: Void -> Void;
  public var eternalDev: Bool;
  public var verbose: Bool;
  public var gamescene: MovieClip;
  public var hideLoader: Void -> Void;
  public var musics: Array<Sound>;
  public var loaderScene: {onRelease: Dynamic};
  public var goptions: String;
  public var eternal: {ext: MovieClip, families: String, useGameXML: Bool, gameXML: String, gameXMLdatas: Dynamic};

  public static function getXmlAttribute(xmlNode: Dynamic, attrName: String) {
    return Reflect.field(xmlNode.attributes, attrName);
  }

  public function new(
    gameEngine: MovieClip,
    enginePatcher: MovieClip,
    musics: Null<Array<Sound>>,
    gameOptions: Array<String>,
    families: String,
    gameContent: Null<GameContent>,
    isProduction: Bool
  ) {
    this.prepareGame = NO_OP;
    Reflect.setField(this, Obfu.raw("prepareGame"), this.prepareGame);
    this.eternalDev = !isProduction;
    Reflect.setField(this, Obfu.raw("eternalDev"), this.eternalDev);
    this.verbose = !isProduction;
    Reflect.setField(this, Obfu.raw("verbose"), this.verbose);
    this.gamescene = gameEngine;
    Reflect.setField(this, Obfu.raw("gamescene"), this.gamescene);
    this.hideLoader = NO_OP;
    Reflect.setField(this, Obfu.raw("hideLoader"), this.hideLoader);
    this.musics = musics;
    Reflect.setField(this, Obfu.raw("musics"), this.musics);
    this.loaderScene = {
      onRelease: NO_OP
    };
    Reflect.setField(this, Obfu.raw("loaderScene"), this.loaderScene);
    Reflect.setField(this.loaderScene, Obfu.raw("onRelease"), this.loaderScene.onRelease);
    this.goptions = gameOptions.join(",");
    Reflect.setField(this, Obfu.raw("goptions"), this.goptions);
    this.eternal = {
      ext: enginePatcher,
      families: families,
      useGameXML: null,
      gameXML: null,
      gameXMLdatas: null
    }

    if (gameContent == null) {
      this.eternal.useGameXML = false;
      this.eternal.gameXML = null;
      this.eternal.gameXMLdatas = null;
    } else {
      var xmlString = gameContent.sourceString;

      var flashXml: Dynamic = untyped __new__(Reflect.field(flash.Lib._global, "XML"));
      flashXml.ignoreWhite = true;
      flashXml.parseXML(xmlString);

      var contentRoot: Dynamic = flashXml.childNodes[0];
      var childNodes: Array<Dynamic> = contentRoot.childNodes;
      var data: Dynamic = {};
      for (child in childNodes) {
        if (child.nodeName == Obfu.raw("datas")) {
          var dataNodes: Array<Dynamic> = child.childNodes;
          for (dataNode in dataNodes) {
            var dataKey: String = LoaderCompat.getXmlAttribute(dataNode, Obfu.raw("name"));
            if (dataKey != null) {
              try {
                var jsonString = Std.string(dataNode.firstChild.nodeValue);
                Reflect.setField(data, dataKey, Json.parse(jsonString));
              } catch (err: Dynamic) {
                throw new Error("Error with data: " + dataKey);
              }
            }
          }
        } else if (child.nodeName == Obfu.raw("messages")) {
          child.removeNode();
          var replacement: Dynamic = flashXml.createElement(Obfu.raw("lang"));
          var langNodes: Array<Dynamic> = child.childNodes;
          for (langNode in langNodes) {
            replacement.appendChild(langNode);
          }
          contentRoot.appendChild(replacement);
        }
      }

      this.eternal.useGameXML = true;
      this.eternal.gameXML = contentRoot;
      this.eternal.gameXMLdatas = data;
    }

    Reflect.setField(this, Obfu.raw("eternal"), this.eternal);
    Reflect.setField(this.eternal, Obfu.raw("ext"), this.eternal.ext);
    Reflect.setField(this.eternal, Obfu.raw("families"), this.eternal.families);
    Reflect.setField(this.eternal, Obfu.raw("useGameXML"), this.eternal.useGameXML);
    Reflect.setField(this.eternal, Obfu.raw("gameXML"), this.eternal.gameXML);
    Reflect.setField(this.eternal, Obfu.raw("gameXMLdatas"), this.eternal.gameXMLdatas);
  }
}
