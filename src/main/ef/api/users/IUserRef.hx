package ef.api.users;

interface IUserRef {
  var id:String;
  var displayName:String;
}
