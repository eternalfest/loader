package ef.api.games;

import ef.api.types.IIdRef;
import net.demurgos.uri.Uri;

interface IGame {
  var id: String;
  var uri: Uri;
  var displayName: String;
  var description: String;
  var createdAt: Date;
  var updatedAt: Date;
  var author: IGameAuthor;
  // ["big", "small", "puzzle", "challenge", "fun", "lab", "other"]
  var category: String;
  var iconFile: Null<IIdRef>;
  var isPrivate: Bool;
  var resources: Array<IGameResource>;
  var engines: IGameEngines;
  var modes: Array<IGameMode>;
}
