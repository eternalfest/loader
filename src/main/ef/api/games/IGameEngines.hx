package ef.api.games;

interface IGameEngines {
  // ["1.0.0", "2.1.0", "2.2.0", "3.0.0"]
  public var loader: String;
  public var game: Null<String>;
  public var external: Null<String>;
  public var eternaldev: Null<String>;
}
