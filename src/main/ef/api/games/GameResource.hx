package ef.api.games;

import ef.api.files.File;
import ef.api.files.IFile;
import ef.api.games.IGameResource;
import ef.api.types.Ucs2String;
import tink.core.Error;
import etwin.Obfu;

class GameResource implements IGameResource {
  public var file: IFile;
  public var tag: Null<String>;
  public var priority: String;

  public function new(file: IFile, tag: Null<String>, priority: String) {
    this.file = file;
    this.tag = tag;
    this.priority = priority;
  }

  public function toString(): String {
    return "[object GameResource]";
  }

  /**
   * Build a GameResource instance from a parsed JSON document.
   */
  public static function readJson(raw: Dynamic): GameResource {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var file: IFile = File.readJson(Reflect.field(raw, Obfu.raw("file")));
    var rawTag: Dynamic = Reflect.field(raw, Obfu.raw("tag"));
    var tag: Null<String> = rawTag != null ? Ucs2String.readJson(rawTag) : null;
    var priority: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("priority")));
    return new GameResource(file, tag, priority);
  }
}
