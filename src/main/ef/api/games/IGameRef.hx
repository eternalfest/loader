package ef.api.games;

interface IGameRef {
  var id:String;
  var displayName:String;
}
