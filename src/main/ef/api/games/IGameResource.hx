package ef.api.games;

import ef.api.files.IFile;

interface IGameResource {
  var file: IFile;
  var tag: Null<String>;
  var priority: String;
  // var file:ISimpleFile;
  // ["icon", "games-swf", "games-xml", "es-swf", "music", "lang-xml", "gfx", "other"]
  // var tag:String;
  // "high"
  // var priority:String;
}
