package ef.api.games;

import ef.api.games.GameModeState;

interface IGameMode {
  var id: String;
  var state: GameModeState;
  var options: Array<IGameOption>;
}
