package ef.api.games;

interface IGameAuthor {
  public var id: String;
  public var displayName: String;
}
