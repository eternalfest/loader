package ef.api.games;

import ef.api.games.GameOptionState;

interface IGameOption {
  var id: String;
  var state: GameOptionState;
}
