package ef.api.games;

import ef.api.games.IGame;
import ef.api.games.IGameAuthor;
import ef.api.games.IGameEngines;
import ef.api.types.BooleanType;
import ef.api.types.DateType;
import ef.api.types.GameModeArray;
import ef.api.types.GameResourceArray;
import ef.api.types.IdRef;
import ef.api.types.IIdRef;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import net.demurgos.uri.Uri;
import Reflect;
import tink.core.Error;
import etwin.Obfu;

class Game implements IGame {
  public var id: String;
  public var uri: Uri;
  public var displayName: String;
  public var description: String;
  public var createdAt: Date;
  public var updatedAt: Date;
  public var publicAccessFrom: Null<Date>;
  public var author: IGameAuthor;
  // ["big", "small", "puzzle", "challenge", "fun", "lab", "other"]
  public var category: String;
  public var iconFile: Null<IIdRef>;
  public var isPrivate: Bool;
  public var resources: Array<IGameResource>;
  public var engines: IGameEngines;
  public var modes: Array<IGameMode>;

  public function new(
    id: String,
    displayName: String,
    description: String,
    createdAt: Date,
    updatedAt: Date,
    publicAccessFrom: Null<Date>,
    author: IGameAuthor,
    category: String,
    iconFile: Null<IIdRef>,
    isPrivate: Bool,
    resources: Array<IGameResource>,
    engines: IGameEngines,
    modes: Array<IGameMode>
  ) {
    this.id = id;
    this.displayName = displayName;
    this.description = description;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.publicAccessFrom = publicAccessFrom;
    this.author = author;
    this.category = category;
    this.iconFile = iconFile;
    this.isPrivate = isPrivate;
    this.resources = resources;
    this.engines = engines;
    this.modes = modes;
  }

  public function toString(): String {
    return "[Game@" + this.id + "]";
  }

  public function getResources(tag: String): Array<IGameResource> {
    var result: Array<IGameResource> = [];
    for (resource in this.resources) {
      if (resource.tag == tag) {
        result.push(resource);
      }
    }
    return result;
  }

  public function getUniqueResource(tag: String): Null<IGameResource> {
    for (resource in this.resources) {
      if (resource.tag == tag) {
        return resource;
      }
    }
    return null;
  }

  /**
   * Build a Game instance from a parsed JSON document **without checking the validity of the
   * document**.
   */
  public static function readJson(raw: Dynamic): Game {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var id: String = UuidHex.readJson(Reflect.field(raw, Obfu.raw("id")));
    var displayName: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("display_name")));
    var description: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("description")));
    var createdAt: Date = DateType.readJson(Reflect.field(raw, Obfu.raw("created_at")));
    var updatedAt: Date = DateType.readJson(Reflect.field(raw, Obfu.raw("updated_at")));
    var rawPublicAcessFrom: Dynamic = Reflect.field(raw, Obfu.raw("public_access_from"));
    var publicAccessFrom: Null<Date> = rawPublicAcessFrom != null ? DateType.readJson(rawPublicAcessFrom) : null;
    var author: IGameAuthor = GameAuthor.readJson(Reflect.field(raw, Obfu.raw("author")));
    var category: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("category")));
    var rawIconFile: Dynamic = Reflect.field(raw, Obfu.raw("icon_file"));
    var iconFile: Null<IIdRef> = rawIconFile != null ? IdRef.readJson(rawIconFile) : null;
    var isPrivate: Bool = BooleanType.readJson(Reflect.field(raw, Obfu.raw("is_private")));
    var resources: Array<IGameResource> = GameResourceArray.readJson(Reflect.field(raw, Obfu.raw("resources")));
    var engines: IGameEngines = GameEngines.readJson(Reflect.field(raw, Obfu.raw("engines")));
    var modes: Array<IGameMode> = GameModeArray.readJson(Reflect.field(raw, Obfu.raw("game_modes")));
    return new Game(id, displayName, description, createdAt, updatedAt, publicAccessFrom, author, category, iconFile, isPrivate, resources, engines, modes);
  }
}
