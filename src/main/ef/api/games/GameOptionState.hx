package ef.api.games;

enum GameOptionState {
  /**
   * The option is displayed, unchecked and cannot be checked (constantly unchecked).
   * The option should never be active.
   */
  DISABLED;

  /**
   * The option is displayed, unchecked but can be checked.
   */
  ENABLED;

  /**
   * The option is displayed, checked and cannot be unchecked (constantly checked).
   */
  LOCKED;
}
