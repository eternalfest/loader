package ef.api.user;

import ef.api.types.DateType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import etwin.ds.Nil;
import etwin.Obfu;
import Reflect;
import tink.core.Error;

class ShortUser {
  public var id(default, null): String;
  public var displayName(default, null): String;

  public function new(
    id: String,
    displayName: String
  ) {
    this.id = id;
    this.displayName = displayName;
  }

  public function toString(): String {
    return "[ShortUser@" + this.id + "]";
  }
  public static function readJson(raw: Dynamic): ShortUser {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var id: String = UuidHex.readJson(Obfu.field(raw, "id"));
    var displayName: String = Ucs2String.readJson(Obfu.field(raw, "display_name"));
    return new ShortUser(id, displayName);
  }
}
