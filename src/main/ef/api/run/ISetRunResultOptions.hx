package ef.api.run;

interface ISetRunResultOptions {
  var isVictory: Bool;
  var maxLevel: Int;
  var scores: Array<Int>;
  var items: Map<String, Int>;
  var stats: Dynamic;
}
