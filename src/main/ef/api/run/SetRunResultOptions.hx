package ef.api.run;

import ef.api.run.ISetRunResultOptions;
import etwin.Obfu;

class SetRunResultOptions implements ISetRunResultOptions {
  public var isVictory: Bool;
  public var maxLevel: Int;
  public var scores: Array<Int>;
  public var items: Map<String, Int>;
  public var stats: Dynamic;

  public function new(isVictory: Bool, maxLevel: Int, scores: Array<Int>, items: Map<String, Int>, stats: Dynamic) {
    this.isVictory = isVictory;
    this.maxLevel = maxLevel;
    this.scores = scores;
    this.items = items;
    this.stats = stats;
  }

  public static function writeJson(doc: ISetRunResultOptions): Dynamic {
    var items = {};
    for (key in doc.items.keys()) {
      Reflect.setField(items, key, doc.items.get(key));
    }
    var raw: Dynamic = {};
    Reflect.setField(raw, Obfu.raw("is_victory"), doc.isVictory);
    Reflect.setField(raw, Obfu.raw("max_level"), doc.maxLevel);
    Reflect.setField(raw, Obfu.raw("scores"), doc.scores);
    Reflect.setField(raw, Obfu.raw("items"), items);
    Reflect.setField(raw, Obfu.raw("stats"), doc.stats);
    return raw;
  }
}
