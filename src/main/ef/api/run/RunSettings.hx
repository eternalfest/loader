package ef.api.run;

import ef.api.run.IRunSettings;
import ef.api.types.BooleanType;
import ef.api.types.Float64Type;
import tink.core.Error;
import etwin.Obfu;

class RunSettings implements IRunSettings {
  public var detail: Bool;
  public var music: Bool;
  public var shake: Bool;
  public var sound: Bool;
  public var volume: Float;

  public function new(detail: Bool, music: Bool, shake: Bool, sound: Bool, volume: Float) {
    this.detail = detail;
    this.music = music;
    this.shake = shake;
    this.sound = sound;
    this.volume = volume;
  }

  public static function readJson(raw: Dynamic): RunSettings {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var rawDetail: Dynamic = Reflect.field(raw, Obfu.raw("detail"));
    var rawDeprecatedDetails: Dynamic = Reflect.field(raw, Obfu.raw("details"));
    var detail: Bool = BooleanType.readJson(rawDetail != null ? rawDetail : rawDeprecatedDetails);
    var music: Bool = BooleanType.readJson(Reflect.field(raw, Obfu.raw("music")));
    var shake: Bool = BooleanType.readJson(Reflect.field(raw, Obfu.raw("shake")));
    var sound: Bool = BooleanType.readJson(Reflect.field(raw, Obfu.raw("sound")));
    var volume: Float = Float64Type.readJson(Reflect.field(raw, Obfu.raw("volume")));
    volume = Math.max(0, Math.min(1, volume));
    return new RunSettings(detail, music, shake, sound, volume);
  }
}
