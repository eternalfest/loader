package ef.api.run;

import ef.api.types.IIdRef;

interface IRunStart {
  var run: IIdRef;
  var startedAt: Date;
  var families: String;
  var items: Dynamic;
  var key: String;
}
