package ef.api.run;

import ef.api.games.GameRef;
import ef.api.games.IGameRef;
import ef.api.run.RunSettings;
import ef.api.types.DateType;
import ef.api.types.Ucs2String;
import ef.api.types.Ucs2StringArray;
import ef.api.types.UuidHex;
import ef.api.users.IUserRef;
import ef.api.users.UserRef;
import tink.core.Error;
import etwin.Obfu;

class Run implements IRun {
  public var id: String;
  public var createdAt: Date;
  public var startedAt: Null<Date>;
  public var game: IGameRef;
  public var user: IUserRef;
  public var gameMode: String;
  public var gameOptions: Array<String>;
  public var settings: IRunSettings;

  public function new(
    id: String,
    createdAt: Date,
    startedAt: Null<Date>,
    game: IGameRef,
    user: IUserRef,
    gameMode: String,
    gameOptions: Array<String>,
    settings: IRunSettings
  ) {
    this.id = id;
    this.createdAt = createdAt;
    this.startedAt = startedAt;
    this.game = game;
    this.user = user;
    this.gameMode = gameMode;
    this.gameOptions = gameOptions;
    this.settings = settings;
  }

  public static function readJson(raw: Dynamic): Run {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var id: String = UuidHex.readJson(Reflect.field(raw, Obfu.raw("id")));
    var createdAt: Date = DateType.readJson(Reflect.field(raw, Obfu.raw("created_at")));
    var rawStartedAt: Dynamic = Reflect.field(raw, Obfu.raw("started_at"));
    var startedAt: Null<Date> = rawStartedAt != null ? DateType.readJson(rawStartedAt) : null;
    var game: GameRef = GameRef.readJson(Reflect.field(raw, Obfu.raw("game")));
    var user: UserRef = UserRef.readJson(Reflect.field(raw, Obfu.raw("user")));
    var gameMode: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("game_mode")));
    var gameOptions: Array<String> = Ucs2StringArray.readJson(Reflect.field(raw, Obfu.raw("game_options")));
    var settings: RunSettings = RunSettings.readJson(Reflect.field(raw, Obfu.raw("settings")));

    return new Run(id, createdAt, startedAt, game, user, gameMode, gameOptions, settings);
  }
}
