package ef.api.types;

import tink.core.Error;
import etwin.Obfu;

class DateType {
  public static function equals(actual: Date, expected: Date): Bool {
    return actual.getTime() == expected.getTime();
  }

  public static function readJson(raw: Dynamic): Date {
    if (!Std.is(raw, String)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: String");
    }
    return DateType.fromIsoString(raw);
  }

  public static function testError(raw: Dynamic): Null<Error> {
    return null;
  }

  public static function fromIsoString(str: String): Date {
    // '2018-12-21T20:37:56.495Z'
    var year = Std.parseInt(str.substr(0, 4));
    var month = Std.parseInt(str.substr(5, 2)) - 1;
    var day = Std.parseInt(str.substr(8, 2));
    var hour = Std.parseInt(str.substr(11, 2));
    var min = Std.parseInt(str.substr(14, 2));
    var sec = Std.parseInt(str.substr(17, 2));
    var ms = Std.parseInt(str.substr(20, 3));
    var result: Dynamic = Date.now();
    result.setUTCFullYear(year);
    result.setUTCMonth(month);
    result.setUTCDate(day);
    result.setUTCHours(hour);
    result.setUTCMinutes(min);
    result.setUTCSeconds(sec);
    result.setUTCMilliseconds(ms);
    return result;
  }

  public static function toIsoString(date: Date): String {
    var _date: Dynamic = date;
    var year = leftPad(_date.getUTCFullYear(), 4);
    var month = leftPad(1 + _date.getUTCMonth(), 2);
    var day = leftPad(_date.getUTCDate(), 2);
    var hour = leftPad(_date.getUTCHours(), 2);
    var min = leftPad(_date.getUTCMinutes(), 2);
    var sec = leftPad(_date.getUTCSeconds(), 2);
    var ms = leftPad(_date.getUTCMilliseconds(), 3);
    // '2018-12-21T20:37:56.495Z'
    return year + "-" + month + "-" + day + Obfu.raw("T") + hour + ":" + min + ":" + sec + "." + ms + Obfu.raw("Z");
  }

  private static function leftPad(val: Int, len: Int): String {
    var result: String = Std.string(val);
    var resultLen: Int = result.length;
    for (_ in 0...(len - resultLen)) {
      result = "0" + result;
    }
    return result;
  }
}
