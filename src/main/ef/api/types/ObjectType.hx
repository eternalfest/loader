package ef.api.types;

enum ObjectType {
  Blob;
  Game;
}

class ObjectTypeTools {
  public static function readJson(raw: Dynamic): ObjectType {
    if (raw == Obfu.raw("Blob")) {
      return ObjectType.Blob;
    } else if (raw == Obfu.raw("Game")) {
      return ObjectType.Game;
    } else {
      throw new Error("unknown ObjectType: " + raw);
    }
  }
}
