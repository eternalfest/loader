package ef.api.types;

import ef.api.games.GameMode;
import ef.api.games.IGameMode;
import tink.core.Error;

class GameModeArray {
  public static function readJson(raw: Dynamic): Array<IGameMode> {
    if (!Std.is(raw, Array)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Array");
    }
    var result: Array<IGameMode> = [];
    for (i in 0...raw.length) {
      result.push(GameMode.readJson(raw[i]));
    }
    return result;
  }

  public static function testError(raw: Dynamic): Null<Error> {
    return null;
  }
}
