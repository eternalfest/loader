package ef.api.types;

import etwin.ds.Nil;
import etwin.Error;

class NilType {
  public static function readJson<T>(raw: Dynamic, reader: Dynamic -> T): Nil<T> {
    return raw != null ? Nil.some(reader(raw)) : Nil.none();
  }
}
