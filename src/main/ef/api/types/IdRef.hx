package ef.api.types;

import tink.core.Error;
import etwin.Obfu;

class IdRef implements IIdRef {
  public var id: String;

  public function new(id: String) {
    this.id = id;
  }

  public function toString(): String {
    return "[IdRef@" + this.id + "]";
  }

  public static function readJson(raw: Dynamic): IdRef {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var id: String = UuidHex.readJson(Reflect.field(raw, Obfu.raw("id")));
    return new IdRef(id);
  }
}
