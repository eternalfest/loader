package ef.api.types;

import ef.api.games.GameOption;
import ef.api.games.IGameOption;
import tink.core.Error;

class GameOptionArray {
  public static function readJson(raw: Dynamic): Array<IGameOption> {
    if (!Std.is(raw, Array)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Array");
    }
    var result: Array<IGameOption> = [];
    for (i in 0...raw.length) {
      result.push(GameOption.readJson(raw[i]));
    }
    return result;
  }

  public static function testError(raw: Dynamic): Null<Error> {
    return null;
  }
}
