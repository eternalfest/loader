package ef.api.types;

import ef.api.games.GameResource;
import ef.api.games.IGameResource;
import tink.core.Error;

class GameResourceArray {
  public static function readJson(raw: Dynamic): Array<IGameResource> {
    if (!Std.is(raw, Array)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Array");
    }
    var result: Array<IGameResource> = [];
    for (i in 0...raw.length) {
      result.push(GameResource.readJson(raw[i]));
    }
    return result;
  }

  public static function testError(raw: Dynamic): Null<Error> {
    return null;
  }
}
