package ef.api.types;

import etwin.Error;

class ArrayType {
  public static function readJson<T>(raw: Dynamic, reader: Dynamic -> T): Array<T> {
    if (!Std.is(raw, Array)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Array");
    }
    var result: Array<T> = [];
    for (i in 0...raw.length) {
      result.push(reader(raw[i]));
    }
    return result;
  }
}
