package ef.api.types;
import tink.core.Error;

class Float64Type {
  public static function readJson(raw:Dynamic):Float {
    if (!Std.is(raw, Float)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Float64");
    }
    return raw;
  }

  public static function testError(raw:Dynamic):Null<Error> {
    return null;
  }
}
