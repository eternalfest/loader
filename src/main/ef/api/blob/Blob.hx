package ef.api.blob;

import ef.api.types.IntType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import etwin.Error;
import etwin.Obfu;
import Reflect;

class Blob {
  public var id(default, null): String;
  public var mediaType(default, null): String;
  public var byteSize(default, null): Int;

  public function new(
    id: String,
    mediaType: String,
    byteSize: Int
  ) {
    this.id = id;
    this.mediaType = mediaType;
    this.byteSize = byteSize;
  }

  public function toString(): String {
    return "[Blob@" + this.id + "]";
  }

  public static function readJson(raw: Dynamic): Blob {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var id: String = UuidHex.readJson(Obfu.field(raw, "id"));
    var mediaType: String = Ucs2String.readJson(Obfu.field(raw, "media_type"));
    var byteSize: Int = IntType.readJson(Obfu.field(raw, "byte_size"));
    return new Blob(id, mediaType, byteSize);
  }
}
