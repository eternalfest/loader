package ef.api.files;

interface IFile {
  var id: String;
  var displayName: String;
  var byteSize: Int;
  var mediaType: String;
  var createdAt: Date;
  var updatedAt: Date;
}
