package ef.api.files;

import ef.api.types.DateType;
import ef.api.types.IntType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import tink.core.Error;
import etwin.Obfu;

class File implements IFile {
  public var id: String;
  public var displayName: String;
  public var byteSize: Int;
  public var mediaType: String;
  public var createdAt: Date;
  public var updatedAt: Date;

  public function new(id: String, displayName: String, byteSize: Int, mediaType: String, createdAt: Date, updatedAt: Date) {
    this.id = id;
    this.displayName = displayName;
    this.byteSize = byteSize;
    this.mediaType = mediaType;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public function toString(): String {
    return "[object File]";
  }

  /**
   * Build a File instance from a parsed JSON document.
   */
  public static function readJson(raw: Dynamic): File {
    if (!Reflect.isObject(raw)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: Object");
    }
    var id: String = UuidHex.readJson(Reflect.field(raw, Obfu.raw("id")));
    var displayName: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("display_name")));
    var byteSize: Int = IntType.readJson(Reflect.field(raw, Obfu.raw("byte_size")));
    var mediaType: String = Ucs2String.readJson(Reflect.field(raw, Obfu.raw("media_type")));
    var createdAt: Date = DateType.readJson(Reflect.field(raw, Obfu.raw("created_at")));
    var updatedAt: Date = DateType.readJson(Reflect.field(raw, Obfu.raw("updated_at")));

    return new File(id, displayName, byteSize, mediaType, createdAt, updatedAt);
  }
}
