package ef.api;

import haxe.Json;
import net.demurgos.uri.Uri;
import Reflect;
import tink.core.Error;
import tink.core.Future;
import tink.core.Outcome;
import tink.CoreApi.Surprise;
import etwin.Obfu;

#if flash8
import etwin.flash.LoadVars;
#end

class HttpClient {
  public function new() {}

#if flash8

  public function getText(uri: Uri, ?onProgress: Int -> Int -> Void): Surprise<String, Error> {
    return Future.async(function(handler: Outcome<String, Error> -> Void) {
      var uriString: String = uri.toString();
      var lv = new LoadVars();
      var resolved: Bool = false;

      var progressNotifier: Null<Void -> Void> = null;
      if (onProgress != null) {
        progressNotifier = function() {
          var loaded = lv.getBytesLoaded();
          var total = lv.getBytesTotal();
          if (loaded != null && total != null) {
            onProgress(loaded, total);
          }
        }
        FlashStd.addEnterFrameListener(progressNotifier);
      }

      lv.onData = function(data: String): Void {
        if (!resolved) {
          resolved = true;
          if (progressNotifier != null) {
            FlashStd.removeEnterFrameListener(progressNotifier);
            progressNotifier();
            progressNotifier = null;
          }
          handler(Outcome.Success(data));
        }
      }
      lv.onLoad = function(success: Bool): Void {
        if (!resolved) {
          resolved = true;
          if (progressNotifier != null) {
            FlashStd.removeEnterFrameListener(progressNotifier);
            progressNotifier();
            progressNotifier = null;
          }
          handler(Outcome.Failure(new Error("HttpError: GET " + uriString + ", reported success: " + success)));
        }
      }
      lv.load(uriString);
    });
  }
#else
  public function getText(uri:Uri, ?onProgress: Int -> Int -> Void):Surprise<String, Error> {
    return Future.sync(Outcome.Failure(new Error("NotImplemented: HttpClient.getText")));
  }
#end


#if flash8

  private function rawSend(method: String, uri: Uri, data: Null<Map<String, String>> = null): Surprise<String, Error> {
    return Future.async(function(handler: Outcome<String, Error> -> Void) {
      var uriString: String = uri.toString();
      var responseLoadVars = new LoadVars();

      var resolved: Bool = false;

      responseLoadVars.onData = function(data: Null<String>): Void {
        if (!resolved) {
          resolved = true;
          if (data == null) {
            // e.g. 404 error against the REST API causes `data` to be `undefined`.
            handler(Outcome.Failure(new Error("HttpError(EmptyResponse): " + method + " " + uriString)));
          } else {
            handler(Outcome.Success(data));
          }
        }
      }
      responseLoadVars.onLoad = function(success: Bool): Void {
        if (!resolved) {
          resolved = true;
          handler(Outcome.Failure(new Error("HttpError(LoadError): " + method + " " + uriString + ", reported success: " + success)));
        }
      }

      var requestLoadVars = new flash.LoadVars();
      if (data != null) {
        Reflect.setField(requestLoadVars, Obfu.raw("flash"), Obfu.raw("true"));
        for (key in data.keys()) {
          Reflect.setField(requestLoadVars, key, data.get(key));
        }
      }
      var isSent: Bool = requestLoadVars.sendAndLoad(uriString, responseLoadVars, method);

      if (!isSent) {
        resolved = true;
        handler(Outcome.Failure(new Error("HttpError(NoSend): " + method + " " + uriString)));
      }
    });
  }
#else
  private function rawSend(method:String, uri:Uri, data:Null<Map<String, String>>):Surprise<String, Error> {
    return Future.sync(Outcome.Failure(new Error("NotImplemented: HttpClient.rawSend")));
  }
#end

  public function getJson(uri: Uri): Surprise<Dynamic, Error> {
    return this.getText(uri)
    .map(function(outcome: Outcome<String, Error>): Outcome<String, Error> {
      return switch(outcome) {
        case Outcome.Success(success):
          var text: String = success;
          Outcome.Success(Json.parse(text));
        case Outcome.Failure(failure):
          Outcome.Failure(failure);
      }
    });
  }

  public function send(method: String, uri: Uri, data: Null<Map<String, String>> = null): Surprise<Dynamic, Error> {
    return this.rawSend(method, uri, data)
    .map(function(outcome: Outcome<String, Error>): Outcome<String, Error> {
      return switch(outcome) {
        case Outcome.Success(success):
          var text: String = success;
          Outcome.Success(Json.parse(text));
        case Outcome.Failure(failure):
          Outcome.Failure(failure);
      }
    });
  }
}
