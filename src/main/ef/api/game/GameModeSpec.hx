package ef.api.game;

import ef.api.types.BooleanType;
import ef.api.types.StringMapType;
import ef.api.types.Ucs2String;
import etwin.Error;
import etwin.Obfu;
import Reflect;

class GameModeSpec {
  public var displayName(default, null): String;
  public var isVisible(default, null): Bool;
  public var options(default, null): Map<String, GameOptionSpec>;

  public function new(
    displayName: String,
    isVisible: Bool,
    options: Map<String, GameOptionSpec>
  ) {
    this.displayName = displayName;
    this.isVisible = isVisible;
    this.options = options;
  }

  public function toString(): String {
    return "[object GameModeSpec]";
  }

  public static function readJson(raw: Dynamic): GameModeSpec {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var displayName: String = Ucs2String.readJson(Obfu.field(raw, "display_name"));
    var isVisible: Bool = BooleanType.readJson(Obfu.field(raw, "is_visible"));
    var options: Map<String, GameOptionSpec> = StringMapType.readJson(Obfu.field(raw, "options"), GameOptionSpec.readJson);
    return new GameModeSpec(displayName, isVisible, options);
  }
}
