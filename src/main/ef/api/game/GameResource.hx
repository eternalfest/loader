package ef.api.game;

import ef.api.blob.Blob;
import ef.api.types.NilType;
import ef.api.types.Ucs2String;
import etwin.ds.Nil;
import etwin.Error;
import etwin.Obfu;
import Reflect;

class GameResource {
  public var displayName(default, null): Nil<String>;
  public var blob(default, null): Blob;

  public function new(
    displayName: Nil<String>,
    blob: Blob
  ) {
    this.displayName = displayName;
    this.blob = blob;
  }

  public function toString(): String {
    return "[object GameResource]";
  }

  public static function readJson(raw: Dynamic): GameResource {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var displayName: Nil<String> = NilType.readJson(Obfu.field(raw, "display_name"), Ucs2String.readJson);
    var blob: Blob = Blob.readJson(Obfu.field(raw, "blob"));
    return new GameResource(displayName, blob);
  }
}
