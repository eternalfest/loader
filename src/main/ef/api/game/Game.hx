package ef.api.game;

import ef.api.user.ShortUser;
import ef.api.types.DateType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import etwin.ds.Nil;
import etwin.Obfu;
import Reflect;
import tink.core.Error;

class Game {
  public var id(default, null): String;
  public var createdAt(default, null): Date;
  public var key(default, null): Nil<String>;
  public var owner(default, null): ShortUser;
  public var channels(default, null): GameChannelListing;

  public function new(
    id: String,
    createdAt: Date,
    key: Nil<String>,
    owner: ShortUser,
    channels: GameChannelListing
  ) {
    this.id = id;
    this.createdAt = createdAt;
    this.key = key;
    this.owner = owner;
    this.channels = channels;
  }

  public function toString(): String {
    return "[Game@" + this.id + "]";
  }

  /**
   * Build a Game instance from a parsed JSON document **without checking the validity of the
   * document**.
   */
  public static function readJson(raw: Dynamic): Game {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var id: String = UuidHex.readJson(Obfu.field(raw, "id"));
    var createdAt: Date = DateType.readJson(Obfu.field(raw, "created_at"));
    var rawKey: Dynamic = Obfu.field(raw, "key");
    var key: Nil<String> = rawKey != null ? Ucs2String.readJson(rawKey) : Nil.none();
    var owner: ShortUser = ShortUser.readJson(Obfu.field(raw, "owner"));
    var channels: GameChannelListing = GameChannelListing.readJson(Obfu.field(raw, "channels"));
    return new Game(id, createdAt, key, owner, channels);
  }
}
