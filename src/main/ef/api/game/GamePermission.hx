package ef.api.game;

import etwin.Error;
import etwin.Obfu;

enum GamePermission {
  None;
  View;
  Play;
  Debug;
  Manage;
}

class GamePermissionTools {
  public static function readJson(raw: Dynamic): GamePermission {
    if (raw == Obfu.raw("None")) {
      return GamePermission.None;
    } else if (raw == Obfu.raw("View")) {
      return GamePermission.View;
    } else if (raw == Obfu.raw("Play")) {
      return GamePermission.Play;
    } else if (raw == Obfu.raw("Debug")) {
      return GamePermission.Debug;
    } else if (raw == Obfu.raw("Manage")) {
      return GamePermission.Manage;
    } else {
      throw new Error("unknown GamePermission: " + raw);
    }
  }
}
