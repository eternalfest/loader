package ef.api.game;

import ef.api.blob.Blob;
import ef.api.types.Ucs2String;
import etwin.Error;
import etwin.Obfu;

enum GameEngine {
  V96;
  Custom(blob: Blob);
}

class GameEngineTools {
  public static function readJson(raw: Dynamic): GameEngine {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var typ: String = Ucs2String.readJson(Obfu.field(raw, "type"));
    if (typ == Obfu.raw("V96")) {
      return GameEngine.V96;
    } else if (typ == Obfu.raw("Custom")) {
      var blob: Blob = Blob.readJson(Obfu.field(raw, "blob"));
      return GameEngine.Custom(blob);
    } else {
      throw new Error("unknown GameEngine.type: " + typ);
    }
  }
}
