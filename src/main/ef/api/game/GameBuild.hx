package ef.api.game;

import ef.api.blob.Blob;
import ef.api.game.GameEngine.GameEngineTools;
import ef.api.types.ArrayType;
import ef.api.types.DateType;
import ef.api.types.NilType;
import ef.api.types.StringMapType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import etwin.ds.Nil;
import etwin.Obfu;
import Reflect;
import tink.core.Error;

class GameBuild {
  public var version(default, null): String;
  public var createdAt(default, null): Date;
  public var gitCommitRef(default, null): Nil<String>;
  public var mainLocale(default, null): String;
  public var displayName(default, null): String;
  public var description(default, null): String;
  public var icon(default, null): Nil<Blob>;
  public var loader(default, null): String;
  public var engine(default, null): GameEngine;
  public var patcher(default, null): Nil<GamePatcher>;
  public var debug(default, null): Nil<Blob>;
  public var content(default, null): Nil<Blob>;
  public var contentI18n(default, null): Nil<Blob>;
  public var musics(default, null): Array<GameResource>;
  public var modes(default, null): Map<String, GameModeSpec>;
  public var families(default, null): String;
  public var category(default, null): String;
  public var i18n(default, null): Map<String, GameBuildI18n>;

  public function new(
    version: String,
    createdAt: Date,
    gitCommitRef: Nil<String>,
    mainLocale: String,
    displayName: String,
    description: String,
    icon: Nil<Blob>,
    loader: String,
    engine: GameEngine,
    patcher: Nil<GamePatcher>,
    debug: Nil<Blob>,
    content: Nil<Blob>,
    contentI18n: Nil<Blob>,
    musics: Array<GameResource>,
    modes: Map<String, GameModeSpec>,
    families: String,
    category: String,
    i18n: Map<String, GameBuildI18n>
  ) {
    this.version = version;
    this.createdAt = createdAt;
    this.gitCommitRef = gitCommitRef;
    this.mainLocale = mainLocale;
    this.displayName = displayName;
    this.description = description;
    this.icon = icon;
    this.loader = loader;
    this.engine = engine;
    this.patcher = patcher;
    this.debug = debug;
    this.content = content;
    this.contentI18n = contentI18n;
    this.musics = musics;
    this.modes = modes;
    this.families = families;
    this.category = category;
    this.i18n = i18n;
  }

  public function toString(): String {
    return "[GameBuild@" + this.version + "]";
  }

  public static function readJson(raw: Dynamic): GameBuild {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var version: String = Ucs2String.readJson(Obfu.field(raw, "version"));
    var createdAt: Date = DateType.readJson(Obfu.field(raw, "created_at"));
    var gitCommitRef: Nil<String> = NilType.readJson(Obfu.field(raw, "git_commit_ref"), Ucs2String.readJson);
    var mainLocale: String = Ucs2String.readJson(Obfu.field(raw, "main_locale"));
    var displayName: String = Ucs2String.readJson(Obfu.field(raw, "display_name"));
    var description: String = Ucs2String.readJson(Obfu.field(raw, "description"));
    var icon: Nil<Blob> = NilType.readJson(Obfu.field(raw, "icon"), Blob.readJson);
    var loader: String = Ucs2String.readJson(Obfu.field(raw, "loader"));
    var engine: GameEngine = GameEngineTools.readJson(Obfu.field(raw, "engine"));
    var patcher: Nil<GamePatcher> = NilType.readJson(Obfu.field(raw, "patcher"), GamePatcher.readJson);
    var debug: Nil<Blob> = NilType.readJson(Obfu.field(raw, "debug"), Blob.readJson);
    var content: Nil<Blob> = NilType.readJson(Obfu.field(raw, "content"), Blob.readJson);
    var contentI18n: Nil<Blob> = NilType.readJson(Obfu.field(raw, "content_i18n"), Blob.readJson);
    var musics: Array<GameResource> = ArrayType.readJson(Obfu.field(raw, "musics"), GameResource.readJson);
    var modes: Map<String, GameModeSpec> = StringMapType.readJson(Obfu.field(raw, "modes"), GameModeSpec.readJson);
    var families: String = Ucs2String.readJson(Obfu.field(raw, "families"));
    var category: String = Ucs2String.readJson(Obfu.field(raw, "category"));
    var i18n: Map<String, GameBuildI18n> = StringMapType.readJson(Obfu.field(raw, "i18n"), GameBuildI18n.readJson);
    return new GameBuild(version, createdAt, gitCommitRef, mainLocale, displayName, description, icon, loader, engine, patcher, debug, content, contentI18n, musics, modes, families, category, i18n);
  }
}
