package ef.api.game;

import ef.api.blob.Blob;
import ef.api.game.GameEngine.GameEngineTools;
import ef.api.types.BooleanType;
import ef.api.types.StringMapType;
import ef.api.types.Ucs2String;
import etwin.ds.Nil;
import etwin.Obfu;
import Reflect;
import tink.core.Error;

class GameOptionSpec {
  public var displayName(default, null): String;
  public var isVisible(default, null): Bool;
  public var isEnabled(default, null): Bool;
  public var defaultValue(default, null): Bool;

  public function new(
    displayName: String,
    isVisible: Bool,
    isEnabled: Bool,
    defaultValue: Bool
  ) {
    this.displayName = displayName;
    this.isVisible = isVisible;
    this.isEnabled = isEnabled;
    this.defaultValue = defaultValue;
  }

  public function toString(): String {
    return "[object GameOptionSpec]";
  }

  public static function readJson(raw: Dynamic): GameOptionSpec {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var displayName: String = Ucs2String.readJson(Obfu.field(raw, "display_name"));
    var isVisible: Bool = BooleanType.readJson(Obfu.field(raw, "is_visible"));
    var isEnabled: Bool = BooleanType.readJson(Obfu.field(raw, "is_enabled"));
    var defaultValue: Bool = BooleanType.readJson(Obfu.field(raw, "default_value"));
    return new GameOptionSpec(displayName, isVisible, isEnabled, defaultValue);
  }
}
