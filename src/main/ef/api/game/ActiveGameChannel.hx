package ef.api.game;

import ef.api.game.GamePermission.GamePermissionTools;
import ef.api.types.BooleanType;
import ef.api.types.DateType;
import ef.api.types.Ucs2String;
import ef.api.types.UuidHex;
import etwin.ds.Nil;
import etwin.Obfu;
import Reflect;
import tink.core.Error;

class ActiveGameChannel {
  public var key(default, null): String;
  public var isEnabled(default, null): Bool;
  public var isPinned(default, null): Bool;
  public var publicationDate(default, null): Nil<Date>;
  public var sortUpdateDate(default, null): Date;
  public var defaultPermission(default, null): GamePermission;
  public var build(default, null): GameBuild;

  public function new(
    key: String,
    isEnabled: Bool,
    isPinned: Bool,
    publicationDate: Nil<Date>,
    sortUpdateDate: Date,
    defaultPermission: GamePermission,
    build: GameBuild
  ) {
    this.key = key;
    this.isEnabled = isEnabled;
    this.isPinned = isPinned;
    this.publicationDate = publicationDate;
    this.sortUpdateDate = sortUpdateDate;
    this.defaultPermission = defaultPermission;
    this.build = build;
  }

  public function toString(): String {
    return "[ActiveGameChannel@" + this.key + "]";
  }

  public static function readJson(raw: Dynamic): ActiveGameChannel {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var key: String = Ucs2String.readJson(Obfu.field(raw, "key"));
    var isEnabled: Bool = BooleanType.readJson(Obfu.field(raw, "is_enabled"));
    var isPinned: Bool = BooleanType.readJson(Obfu.field(raw, "is_pinned"));
    var rawPublicationDate: Dynamic = Obfu.field(raw, "publication_date");
    var publicationDate: Nil<Date> = rawPublicationDate != null ? DateType.readJson(rawPublicationDate) : Nil.none();
    var sortUpdateDate: Date = DateType.readJson(Obfu.field(raw, "sort_update_date"));
    var defaultPermission: GamePermission = GamePermissionTools.readJson(Obfu.field(raw, "default_permission"));
    var build: GameBuild = GameBuild.readJson(Obfu.field(raw, "build"));
    return new ActiveGameChannel(key, isEnabled, isPinned, publicationDate, sortUpdateDate, defaultPermission, build);
  }
}
