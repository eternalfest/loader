package ef.api.game;

import ef.api.blob.Blob;
import ef.api.types.Ucs2String;
import etwin.Error;
import etwin.Obfu;
import Reflect;

class GamePatcher {
  public var blob(default, null): Blob;
  public var framework(default, null): PatcherFramework;

  public function new(
    blob: Blob,
    framework: PatcherFramework
  ) {
    this.blob = blob;
    this.framework = framework;
  }

  public function toString(): String {
    return "[object GamePatcher]";
  }

  public static function readJson(raw: Dynamic): GamePatcher {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var blob: Blob = Blob.readJson(Obfu.field(raw, "blob"));
    var framework: PatcherFramework = PatcherFramework.readJson(Obfu.field(raw, "framework"));
    return new GamePatcher(blob, framework);
  }
}
