package ef.api.game;

import ef.api.blob.Blob;
import ef.api.types.NilType;
import ef.api.types.Ucs2String;
import etwin.ds.Nil;
import etwin.Error;
import etwin.Obfu;
import Reflect;

class PatcherFramework {
  public var name(default, null): String;
  public var version(default, null): String;

  public function new(
    name: String,
    version: String
  ) {
    this.name = name;
    this.version = version;
  }

  public function toString(): String {
    return "[object PatcherFramework]";
  }

  public static function readJson(raw: Dynamic): PatcherFramework {
    if (!Reflect.isObject(raw)) {
      throw new Error("actual: " + Type.typeof(raw) + ", expected: Object");
    }
    var name: String = Ucs2String.readJson(Obfu.field(raw, "name"));
    var version: String = Ucs2String.readJson(Obfu.field(raw, "version"));
    return new PatcherFramework(name, version);
  }
}
