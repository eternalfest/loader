package ef.api;

import ef.api.game.Game;
import ef.api.HttpClient;
import ef.api.run.ISetRunResultOptions;
import ef.api.run.RunStart;
import ef.api.run.SetRunResultOptions;
import haxe.Json;
import net.demurgos.uri.Uri;
import etwin.Obfu;
import tink.core.Error;
import tink.core.Outcome;
import tink.CoreApi.Surprise;

class Api {
  var httpClient: HttpClient;
  var routes: ApiRoutes;

  public function new(httpClient: HttpClient, routes: ApiRoutes) {
    this.httpClient = httpClient;
    this.routes = routes;
  }

  public function getGame(uri: Uri): Surprise<Game, Error> {
    return this.httpClient
    .getJson(uri)
    .map(function(outcome: Outcome<Dynamic, Error>): Outcome<Game, Error> {
      return switch(outcome) {
        case Outcome.Success(success):
          var raw: Dynamic = success;
          try {
            return Outcome.Success(Game.readJson(raw));
          } catch (err: Error) {
            return Outcome.Failure(err);
          }
        case Outcome.Failure(failure):
          Outcome.Failure(failure);
      }
    });
  }

  public function startRun(runId: String): Surprise<RunStart, Error> {
    var body: Map<String, String> = new Map();
    body.set(Obfu.raw("key"), "0000");
    return this.httpClient
    .send(Obfu.raw("POST"), this.routes.runStartUri(runId), body)
    .map(function(outcome: Outcome<Dynamic, Error>): Outcome<RunStart, Error> {
      return switch(outcome) {
        case Outcome.Success(success):
          var raw: Dynamic = success;
          try {
            return Outcome.Success(RunStart.readJson(raw));
          } catch (err: Error) {
            return Outcome.Failure(err);
          }
        case Outcome.Failure(failure):
          Outcome.Failure(failure);
      }
    });
  }

  public function setRunResult(runId: String, runResult: ISetRunResultOptions): Surprise<Dynamic, Error> {
    var raw: Dynamic = SetRunResultOptions.writeJson(runResult);
    var body: Map<String, String> = new Map();
    for (fieldName in Reflect.fields(raw)) {
      body.set(fieldName, Json.stringify(Reflect.field(raw, fieldName)));
    }
    return this.httpClient
    .send(Obfu.raw("POST"), this.routes.runResultUri(runId), body)
    .map(function(outcome: Outcome<Dynamic, Error>): Outcome<Dynamic, Error> {
      return switch(outcome) {
        case Outcome.Success(success):
          var raw: Dynamic = success;
          try {
            return Outcome.Success(null);
          } catch (err: Error) {
            return Outcome.Failure(err);
          }
        case Outcome.Failure(failure):
          Outcome.Failure(failure);
      }
    });
  }

  public function getXml(uri: Uri, ?onProgress: Int -> Int -> Void): Surprise<Xml, Error> {
    return this.httpClient
    .getText(uri, onProgress)
    .map(function(outcome: Outcome<String, Error>): Outcome<Xml, Error> {
      return switch(outcome) {
        case Outcome.Success(success):
          Outcome.Success(Xml.parse(success));
        case Outcome.Failure(failure):
          Outcome.Failure(failure);
      }
    });
  }

  public function resolveRawFileUri(fileId: String): Uri {
    return this.routes.rawBlob(fileId);
  }

  public function resolveGameFileUri(): Uri {
    return this.routes.gameFile();
  }
}
