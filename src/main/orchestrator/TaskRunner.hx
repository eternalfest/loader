package orchestrator;

import tink.core.Error;
import tink.core.Future;
import tink.core.Outcome;
import tink.CoreApi.Surprise;
import etwin.ds.WeakMap;

class TaskRunner<T> {
  private var tasks:WeakMap<Token<Dynamic>, Task<Dynamic>>;
  private var dependencies:WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>;
  private var dependants:WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>;
  private var target:Token<T>;

  /**
   * Set of tasks that were marked for execution.
   * They may have already been completed
   */
  private var scheduled:WeakMap<Token<Dynamic>, Bool>;

  /**
   * Set of tasks that were started.
   * They may have already been completed
   */
  private var started:WeakMap<Token<Dynamic>, Bool>;

  /**
   * Map of results for completed tasks
   */
  private var results:WeakMap<Token<Dynamic>, Dynamic>;

  /**
   * Final callback
   */
  private var callback:Null<Outcome<T, Error> -> Void>;

  public function new(
    tasks:WeakMap<Token<Dynamic>, Task<Dynamic>>,
    dependencies:WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>,
    dependants:WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>,
    target:Token<T>
  ) {
    this.tasks = tasks;
    this.dependencies = dependencies;
    this.dependants = dependants;
    this.results = new WeakMap<Token<Dynamic>, Dynamic>();
    this.scheduled = new WeakMap<Token<Dynamic>, Bool>();
    this.started = new WeakMap<Token<Dynamic>, Bool>();
    var stack: Array<Token<Dynamic>> = [target.erase()];
    while (true) {
      var token: Null<Token<Dynamic>> = stack.pop();
      if (token == null) {
        break;
      }
      this.scheduled.set(token, false);
      this.started.set(token, false);
      var deps: Null<Array<Token<Dynamic>>> = this.dependencies.get(token);
      if (deps == null) {
        throw new Error("AssertionError: unknown token in dependencies");
      }
      for (dep in deps) {
        if (!this.scheduled.exists(token)) {
          stack.push(dep);
        }
      }
    }
    this.target = target;
    this.callback = null;
  }

  public function execute():Surprise<T, Error> {
    return Future.async(function(handler:Outcome<T, Error> -> Void) {
      this.callback = handler;
      if (this.results.exists(this.target)) {
        // TODO(demurgos): Clarify the type of the result, it should be Null<T>
        var result:T = this.results.get(this.target);
        this.resolve(Outcome.Success(result));
      } else {
        this.tryStart(this.target);
      }
    });
  }

  public function isPending(task:Token<Dynamic>):Bool {
    return this.scheduled.get(task) && !this.results.exists(task);
  }

  public function isRunning(task:Token<Dynamic>):Bool {
    return this.started.get(task) && !this.results.exists(task);
  }

  private function tryStart(task:Token<Dynamic>):Void {
    if (this.started.get(task)) {
      return;
    }
    this.scheduled.set(task, true);

    var dependencies = this.dependencies.get(task);
    var ready:Bool = true;

    for (dependency in dependencies) {
      if (!this.results.exists(dependency)) {
        ready = false;
      }
    }

    if (ready) {
      this.start(task, false);
    } else {
      for (dependency in dependencies) {
        this.tryStart(dependency);
      }
    }
  }

  private function start(token:Token<Dynamic>, force:Bool):Void {
    if (this.started.get(token) && !force) {
      return;
    }
    this.started.set(token, true);

    var task:Task<Dynamic> = this.tasks.get(token);
    var abstractTask:AbstractTask<Dynamic> = switch (task) {
      case Task.Async(task): task;
      case Task.Sync(task): task;
    }

    abstractTask.runAsync(untyped this.results).handle(function(outcome:Outcome<Dynamic, Error>) {
      this.onTaskOutcome(token, outcome);
    });
  }

  private function onTaskOutcome(task:Token<Dynamic>, outcome:Outcome<Dynamic, Error>):Void {
    switch (outcome) {
      case Outcome.Failure(err):
        this.resolve(Outcome.Failure(err));
      case Outcome.Success(result):
        this.results.set(task, result);
        if (task == this.target) {
          this.resolve(Outcome.Success(result));
        } else {
          for (dependant in this.dependants.get(task)) {
            if (this.scheduled.get(dependant)) {
              this.tryStart(dependant);
            }
          }
        }
    }
  }

  private function resolve(outcome:Outcome<T, Error>):Void {
    if (this.callback != null) {
      var callback:Outcome<T, Error> -> Void = this.callback;
      this.callback = null;
      callback(outcome);
    } else {
      // TODO(demurgos): Throw error
    }
  }
}
