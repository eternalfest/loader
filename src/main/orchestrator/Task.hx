package orchestrator;

import orchestrator.AsyncTask;
import orchestrator.SyncTask;

enum Task<T> {
  Sync(task: SyncTask<T>);
  Async(task: AsyncTask<T>);
}
