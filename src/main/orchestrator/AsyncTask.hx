package orchestrator;

import tink.core.Error;
import tink.CoreApi.Surprise;

class AsyncTask<T> extends AbstractTask<T> {
  public var fn:IDependencies -> Surprise<T, Error>;

  public function new(token:Token<Dynamic>, name:String, fn:IDependencies -> Surprise<T, Error>) {
    super(token, name);
    this.fn = fn;
  }

  override public function runAsync(dependencies:IDependencies):Surprise<T, Error> {
    return this.fn(dependencies);
  }
}
