package orchestrator;

import etwin.Symbol;

class Token<T> {
  private var symbol: Symbol;

  public function new(): Void {
    this.symbol = new Symbol();
  }

  public function toString(): String {
    return this.symbol.toString();
  }

  public function erase(): Token<Dynamic> {
    untyped {
      return this;
    }
  }
}
