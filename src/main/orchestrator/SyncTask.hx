package orchestrator;

import tink.core.Error;
import tink.core.Future;
import tink.core.Outcome;
import tink.CoreApi.Surprise;

class SyncTask<T> extends AbstractTask<T> {
  public var fn:IDependencies -> T;

  public function new(token:Token<Dynamic>, name:String, fn:IDependencies -> T) {
    super(token, name);
    this.fn = fn;
  }

  override public function runAsync(dependencies:IDependencies):Surprise<T, Error> {
    var outcome:Outcome<T, Error>;
    try {
      outcome = Outcome.Success(this.fn(dependencies));
    } catch (err:Error) {
      outcome = Outcome.Failure(err);
    }
    return Future.sync(outcome);
  }
}
