package orchestrator;

import orchestrator.Task;
import tink.core.Error;
import tink.CoreApi.Surprise;
import etwin.ds.WeakMap;

class Orchestrator {
  private var tasks:WeakMap<Token<Dynamic>, Task<Dynamic>>;
  private var dependencies:WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>;
  private var dependants:WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>;

  public function new() {
    this.tasks = new WeakMap<Token<Dynamic>, Task<Dynamic>>();
    this.dependencies = new WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>();
    this.dependants = new WeakMap<Token<Dynamic>, Array<Token<Dynamic>>>();
  }

  private static function createToken<T>():Token<T> {
    var token:Token<T> = new Token();
    return token;
  }

  public function asyncTask<T>(dependencies:Array<Token<Dynamic>>, fn:IDependencies -> Surprise<T, Error>):Token<T> {
    var token:Token<T> = Orchestrator.createToken();
    var taskName:String = token.toString();
    var task:Task<T> = Task.Async(new AsyncTask(token, taskName, fn));
    this.addTask(task, dependencies);
    return token;
  }

  public function syncTask<T>(dependencies:Array<Token<Dynamic>>, fn:IDependencies -> T):Token<T> {
    var token:Token<T> = Orchestrator.createToken();
    var taskName:String = token.toString();
    var task:Task<T> = Task.Sync(new SyncTask(token, taskName, fn));
    this.addTask(task, dependencies);
    return token;
  }

  public function startRunner<T>(token:Token<T>): TaskRunner<T> {
    if (!this.tasks.exists(token)) {
      throw new Error("Unknown task");
    }
    return new TaskRunner(this.tasks, this.dependencies, this.dependants, token);
  }

  public function start<T>(token:Token<T>):Surprise<T, Error> {
    return this.startRunner(token).execute();
  }

  private function addTask(task:Task<Dynamic>, dependencies:Array<Token<Dynamic>>):Void {
    var abstractTask:AbstractTask<Dynamic> = switch (task) {
      case Task.Async(task): task;
      case Task.Sync(task): task;
    }
    var token:Token<Dynamic> = abstractTask.token;
    if (this.tasks.exists(token)) {
      throw new Error("A task with the provided token already exists");
    }
    // TODO(demurgos): Assert token does not exists in the remaining maps
    this.tasks.set(token, task);
    for (dependency in dependencies) {
      if (!this.tasks.exists(dependency)) {
        throw new Error("The task requires an unknown dependency");
      }
      var dependencyDependants:Null<Array<Token<Dynamic>>> = this.dependants.get(dependency);
      // TODO(demurgos): Assert not null
      dependencyDependants.push(token);
    }
    this.dependencies.set(token, dependencies);
    this.dependants.set(token, []);
  }
}
