package ;

import etwin.Obfu;
#if flash8
import etwin.flash.MovieClip;
#else
import openfl.display.MovieClip;
#end

import StdInc;

class FlashStd {
  private static var _onEnterFrame: Dynamic = null;
  private static var uid: Int = 0;
  private static var mcSeed: String = Std.string(Math.floor(10000 * Math.random()));

  public static function __init__() {
    flash.Lib.current.onEnterFrame = FlashStd.onEnterFrame;
    #if flash8
    StdInc.init(flash.Lib._global, true);
    #end
  }

  #if flash8

  public static function addEmptyMovie(mc: MovieClip, depth: Int): MovieClip {
    var clipname = Obfu.raw("empty_") + mcSeed + Obfu.raw("_") + FlashStd.uid++;
    mc.createEmptyMovieClip(clipname, depth);
    return Reflect.field(mc, clipname);
  }

  public static function createTextField(mc: MovieClip, depth: Int): Dynamic {
    var clipname = Obfu.raw("text_") + mcSeed + Obfu.raw("_") + FlashStd.uid++;
    mc.createTextField(clipname, depth, 0, 0, 100, 20);
    return Reflect.field(mc, clipname);
  }

  #end

  public static function arrayFromIterator<I>(iterator: Iterator<I>): Array<I> {
    var result: Array<I> = [];
    for (item in iterator) {
      result.push(item);
    }
    return result;
  }

  public static function addEnterFrameListener(handler: Void -> Void): Void {
    if (FlashStd._onEnterFrame == null) {
      FlashStd._onEnterFrame = handler;
    } else if (Reflect.isFunction(FlashStd._onEnterFrame)) {
      FlashStd._onEnterFrame = [FlashStd._onEnterFrame, handler];
    } else {
      FlashStd._onEnterFrame.push(handler);
    }
  }

  public static function removeEnterFrameListener(handler: Void -> Void): Bool {
    if (FlashStd._onEnterFrame == null) {
      return false;
    } else if (Reflect.isFunction(FlashStd._onEnterFrame)) {
      if (FlashStd._onEnterFrame == handler) {
        FlashStd._onEnterFrame = null;
        return true;
      }
      return false;
    }
    var removed: Bool = FlashStd._onEnterFrame.remove(handler);
    if (removed && FlashStd._onEnterFrame.length == 1) {
      FlashStd._onEnterFrame = FlashStd._onEnterFrame[0];
    }
    return removed;
  }

  private static function onEnterFrame(): Void {
    if (FlashStd._onEnterFrame == null) {
      return;
    }
    if (Reflect.isFunction(FlashStd._onEnterFrame)) {
      FlashStd._onEnterFrame();
    } else {
      for (handler in (FlashStd._onEnterFrame: Array<Dynamic>)) {
        try {
          handler();
        } catch (err: Dynamic) {
          // Ignore errors (we could eventually log a warning)
        }
      }
    }
  }
}
