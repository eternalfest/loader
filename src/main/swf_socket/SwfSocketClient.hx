package swf_socket;

import etwin.Obfu;
import flash.external.ExternalInterface;
import haxe.Constraints.Function;
import tink.core.Error;
import tink.core.Noise;
import tink.core.Outcome;

class SwfSocketClient {
  public var objectId: String;
  public var clientMethod: String;
  private var remoteMethod: String;
  private var router: Map<String, RequestHandler<Dynamic, Dynamic>>;

  public function new(objectId: String, clientMethod: String, remoteMethod: String) {
    this.objectId = objectId;
    this.clientMethod = clientMethod;
    this.remoteMethod = remoteMethod;
    this.router = new Map<String, RequestHandler<Dynamic, Dynamic>>();
#if flash8
    ExternalInterface.addCallback(clientMethod, this, this.onMessage);
#else
    ExternalInterface.addCallback(clientMethod, this.onMessage);
#end
  }

  public function onMessage(message: SwfSocketMessage): Dynamic {
    try {
      var handler: Null<Function> = this.router.get(message.name);
      if (handler != null) {
        var result: Dynamic = Reflect.callMethod(null, handler, [message.data]);
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("success"), true);
        Reflect.setField(raw, Obfu.raw("result"), result);
        return raw;
      } else {
        var rawError: Dynamic = {};
        Reflect.setField(rawError, Obfu.raw("message"), Obfu.raw("ResourceNotFound"));
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("success"), false);
        Reflect.setField(raw, Obfu.raw("error"), rawError);
        return raw;
      }
    } catch (err: Error) {
      trace(err);
      var raw: Dynamic = {};
      Reflect.setField(raw, Obfu.raw("success"), false);
      return raw;
    }
  }

  public function use(route: String, handler: RequestHandler<Dynamic, Dynamic>): Void {
    if (this.router.exists(route)) {
      throw new Error("DuplicateRouteDefinition");
    }
    this.router.set(route, handler);
  }

  public function emitMessage(name: String, data: Dynamic): Outcome<Noise, Error> {
    var message = {name: name, data: data};
    var response: Dynamic = ExternalInterface.call(this.remoteMethod, message);
    return switch(response.success) {
      case true:
        Outcome.Success(Noise);
      case false:
        Outcome.Failure(new Error("Cannot send message to js://" + this.remoteMethod));
      default:
        Outcome.Failure(new Error("Unexpected value for `success` from js://" + this.remoteMethod));
    }
  }
}
