package swf_socket;

interface SwfSocketMessage {
  public var name: String;
  public var data: Dynamic;
}
