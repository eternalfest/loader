package swf_socket;

import etwin.Obfu;
import flash.external.ExternalInterface;
import tink.core.Error;
import tink.core.Outcome;

class SwfSocket {
  public static function connect(serverMethod: String, objectId: String): Outcome<SwfSocketClient, Error> {
    if (!ExternalInterface.available) {
      return Outcome.Failure(new Error("External interface is not available"));
    }

    var clientMethod: String = Obfu.raw("swfSocketClient") + Std.string(Std.random(1 << 16));
    var fullClientMethod: String = objectId + ":" + clientMethod;

    var partialClient: PartialSwfSocketClient = new PartialSwfSocketClient(objectId, clientMethod);

    var response: Dynamic = ExternalInterface.call(serverMethod, fullClientMethod);
    return switch(Reflect.field(response, Obfu.raw("success"))) {
      case true:
        Outcome.Success(partialClient.complete(Reflect.field(response, Obfu.raw("result"))));
      case false:
        Outcome.Failure(new Error("Cannot connect to: js://" + serverMethod));
      default:
        Outcome.Failure(new Error("Unexpected value for `success` from js://" + serverMethod));
    }
  }
}
