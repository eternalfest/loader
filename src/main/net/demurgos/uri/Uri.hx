package net.demurgos.uri;

import etwin.Obfu;
import hre.Match;
import hre.RegExp;
import tink.core.Error;

typedef ParseUserInfoResult = {
  username: Null<String>,
  password: Null<String>,
  remainder: String
}

typedef ParseHostResult = {
  hostname: String,
  port: Null<Int>,
  path: String
}

typedef ParseAutorityResult = {
  username: Null<String>,
  password: Null<String>,
  hostname: String,
  port: Null<Int>,
  path: String
}

typedef ParseResult = {
  scheme: Null<String>,
  username: Null<String>,
  password: Null<String>,
  hostname: Null<String>,
  port: Null<Int>,
  path: String,
  query: Null<String>,
  fragment: Null<String>
}

class Uri {
  private static var COMMERCIAL_AT(default, never): String = "@";

  var scheme: Null<String>;
  var username: Null<String>;
  var password: Null<String>;
  var hostname: Null<String>;
  var port: Null<Int>;
  var path: String;
  var query: Null<String>;
  var fragment: Null<String>;

  public function new(string: String) {
    var parseResult = Uri.parse(string);
    this.scheme = parseResult.scheme;
    this.username = parseResult.username;
    this.password = parseResult.password;
    this.hostname = parseResult.hostname;
    this.port = parseResult.port;
    this.path = parseResult.path;
    this.query = parseResult.query;
    this.fragment = parseResult.fragment;
  }

  public function getScheme(): Null<String> {
    return this.scheme;
  }

  public function getUsername(): Null<String> {
    return this.username;
  }

  public function getPassword(): Null<String> {
    return this.password;
  }

  public function getUserInfo(): Null<String> {
    if (this.username == null && this.password == null) {
      return null;
    }
    var userInfo: String = "";
    if (this.username != null) {
      userInfo += this.username;
    }
    if (this.password != null) {
      userInfo += ":" + this.password;
    }
    return userInfo;
  }

  public function getHostname(): Null<String> {
    return this.hostname;
  }

  public function getPort(): Null<Int> {
    return this.port;
  }

  public function getHost(): Null<String> {
    if (this.hostname == null) {
      return null;
    }
    var host: String;
    if (this.hostname.indexOf(":") < 0) {
      host = this.hostname;
    } else {
      host = "[" + this.hostname + "]";
    }
    if (this.port != null) {
      host += ":" + Std.string(this.port);
    }
    return host;
  }

  public function getAutority(): Null<String> {
    var host: Null<String> = this.getHost();
    if (host == null) {
      return null;
    }
    var userInfo: Null<String> = this.getUserInfo();

    var authority: String;
    if (userInfo == null) {
      authority = host;
    } else {
      authority = userInfo + COMMERCIAL_AT + host;
    }

    return authority;
  }

  public function getOrigin(): Null<String> {
    var autority: Null<String> = this.getAutority();
    if (autority == null) {
      return null;
    }
    var scheme: Null<String> = this.getScheme();
    var origin: String;
    if (scheme == null) {
      origin = autority;
    } else if (scheme == "") {
      origin = "//" + autority;
    } else {
      origin = scheme + "://" + autority;
    }

    return origin;
  }

  public function getPath(): String {
    return this.path;
  }

  public function getQuery(): Null<String> {
    return this.query;
  }

  public function getFragment(): Null<String> {
    return this.fragment;
  }

  public function setFragment(fragment: Null<String>): Void {
    if (fragment != null && fragment.indexOf("#") >= 0) {
      throw new Error("InvalidFragment: Contains #");
    }
    this.fragment = fragment;
  }

  public function getResource(): String {
    var resource = this.path;
    if (this.query != null) {
      resource += "?" + query;
    }
    if (this.fragment != null) {
      resource += "#" + fragment;
    }
    return resource;
  }

  public function getUri(): String {
    var origin: Null<String> = this.getOrigin();
    var resource: String = this.getResource();
    return origin == null ? resource : origin + resource;
  }

  public function toString(): String {
    return this.getUri();
  }

  private static var schemeRegExp: RegExp = new RegExp("^[a-z][-+.a-z0-9]*$", Obfu.raw("i"));

  @:from
  public static function fromString(string: String): Uri {
    return new Uri(string);
  }

  /**
   * Parses an URI of the following form:
   * [protocol"://"[username[":"password]"@"]hostname[":"port]"/"?][path]["?"querystring]["#"fragment]
   */
  public static function parse(string: String): ParseResult {
    var pos: Int;
    var scheme: Null<String>;
    var username: Null<String>;
    var password: Null<String>;
    var hostname: Null<String>;
    var port: Null<Int>;
    var path: String;
    var query: Null<String>;
    var fragment: Null<String>;
    var isUrn: Bool = false;

    pos = string.indexOf("#");
    if (pos < 0) {
      fragment = null;
    } else {
      fragment = string.substring(pos + 1);
      if (fragment.length == 0) {
        fragment = null;
      }
      string = string.substring(0, pos);
    }

    pos = string.indexOf("?");
    if (pos < 0) {
      query = null;
    } else {
      query = string.substring(pos + 1);
      if (query.length == 0) {
        query = null;
      }
      string = string.substring(0, pos);
    }

    if (string.substring(0, 2) == "//") {
      scheme = "";
      var authority: ParseAutorityResult = Uri.parseAuthority(string.substring(2));
      username = authority.username;
      password = authority.password;
      hostname = authority.hostname;
      port = authority.port;
      path = authority.path;
    } else {
      pos = string.indexOf(":");
      if (pos < 0) {
        scheme = null;
        username = null;
        password = null;
        hostname = null;
        port = null;
        path = string;
      } else {
        scheme = string.substring(0, pos);
        if (Uri.schemeRegExp.test(scheme)) {
          if (string.substring(pos + 1, pos + 3) == "//") {
            var authority: ParseAutorityResult = Uri.parseAuthority(string.substring(pos + 3));
            username = authority.username;
            password = authority.password;
            hostname = authority.hostname;
            port = authority.port;
            path = authority.path;
          } else {
            isUrn = true;
            string = string.substring(1);
            throw new Error("URN are not supported");
          }
        } else {
          scheme = null;
          username = null;
          password = null;
          hostname = null;
          port = null;
          path = string;
        }
      }
    }

    return {
      scheme: scheme,
      username: username,
      password: password,
      hostname: hostname,
      port: port,
      path: path,
      query: query,
      fragment: fragment
    }
  }

  /**
   * Parses the authority at the start of the string
   * [username[":"password]"@"]hostname[":"port]
   */
  private static function parseAuthority(string: String): ParseAutorityResult {
    var userInfo: ParseUserInfoResult = Uri.parseUserInfo(string);
    var host: ParseHostResult = Uri.parseHost(userInfo.remainder);
    return {
      username: userInfo.username,
      password: userInfo.password,
      hostname: host.hostname,
      port: host.port,
      path: host.path
    }
  }

  /**
   * Parses the userInfo at the start of the string
   * [username[":"password]"@"]
   */
  private static function parseUserInfo(string: String): ParseUserInfoResult {
    var username: Null<String>;
    var password: Null<String>;

    // Get the pos of the first "@" before any "/"
    var firstSlashPos: Int = string.indexOf("/");
    var pos: Int = string.lastIndexOf(COMMERCIAL_AT, firstSlashPos < 0 ? string.length - 1 : firstSlashPos);

    if (pos < 0) {
      username = null;
      password = null;
    } else {
      var userInfoString = string.substring(0, pos);
      string = string.substring(pos + 1);
      pos = userInfoString.indexOf(":");
      var usernameString: String;
      var passwordString: String;
      if (pos < 0) {
        username = userInfoString;
        password = null;
      } else {
        username = userInfoString.substring(0, pos);
        password = userInfoString.substring(pos + 1);
      }
    }

    return {
      username: username,
      password: password,
      remainder: string
    }
  }

  /**
   * Parses the host at the start of the string
   * hostname[":"port]
   */
  private static function parseHost(string: String): ParseHostResult {
    var pos: Int;
    var hostname: String;
    var port: Null<Int>;
    var path: String;

    if (string.indexOf("\\") >= 0) {
      throw new Error("Invalid URI: Contains backslashes before the query");
    }

    pos = string.indexOf("/");
    var hostString: String;
    if (pos < 0) {
      hostString = string;
      path = "/";
    } else {
      hostString = string.substring(0, pos);
      path = string.substring(pos);
    }

    if (hostString.charAt(0) == "[") {
      // IPv6 host, requires port
      var re: RegExp = new RegExp("^\\[([:0-9a-fA-F]*)\\]:(\\d+)$");
      var match: Match = re.exec(hostString);
      if (match == null) {
        throw new Error("Invalid URI: IPv6 host requires port");
      }
      hostname = match.groups[1];
      port = Std.parseInt(match.groups[2]);
      if (port == null) {
        throw new Error("Invalid URI: IPv6 host requires valid port");
      }
    } else {
      var colonPos = hostString.indexOf(":");
      var hostnameString: String;
      var portString: String;
      if (colonPos < 0) {
        hostname = hostString;
        port = null;
      } else {
        hostname = hostString.substring(0, colonPos);
        port = Std.parseInt(hostString.substring(colonPos + 1));
        if (port == null) {
          throw new Error("Invalid URI: invalid port");
        }
      }
    }

    return {
      hostname: hostname,
      port: port,
      path: path
    }
  }

  private static function decode(string: String): String {
    // TODO: URI decode
    return string;
  }

  public static function readJson(raw: Dynamic): Uri {
    if (!Std.is(raw, String)) {
      throw new Error("Actual: " + Type.typeof(raw) + ", Expected: String");
    }
    return new Uri(raw);
  }
}
