package ;

import Reflect;
import etwin.Obfu;
#if flash8
import etwin.flash.MovieClip;
#else
import openfl.display.MovieClip;
#end

class StdInc {
  static function __init__() {
    #if flash8
    StdInc.init(flash.Lib._global, true);
    #end
  }

  #if flash8

  /**
   * Whitelist of globals that must be defined on the real `_global` object.
   * (Attaching them to a virtual global fails because of direct accesses)
   */
  public static var RAW_GLOBALS: Array<String> = [Obfu.raw("tick"), Obfu.raw("7;4B"), Obfu.raw("tmod"), Obfu.raw("-V;B")];

  public static function init(mc: MovieClip, compat: Bool, ?global: Dynamic, ?root: Dynamic): Void untyped {
    if (Reflect.field(mc, "__initializedStdInc") == true) {
      return;
    }

    if (global == null) {
      global = _global;
    }
    if (root == null) {
      root = flash.Lib._root;
    }

    if (Reflect.field(_global, Obfu.raw("string")) == null) {
      Reflect.setField(_global, Obfu.raw("string"), function(x: Dynamic): Dynamic {
        return x;
      });
    }

    // Keep `write` flag, keep `delete` flag, enable `hidden` flag
    // http://www.ryanjuckett.com/programming/how-to-use-assetpropflags-in-actionscript-2-0/
    // https://web.archive.org/web/20170606204438/http://www.ryanjuckett.com/programming/how-to-use-assetpropflags-in-actionscript-2-0/
    // Unprotect `Array` prototype (Haxe protects it).
    _global.ASSetPropFlags(Array.prototype, null, 0, 7);

    // `remove` and `insert` are provided by Haxe

    Reflect.setField(Array.prototype, Obfu.raw("copy"), Reflect.field(Array.prototype, Obfu.raw("slice")));
    _global.ASSetPropFlags(Array.prototype, Obfu.raw("copy"), 1);

    Reflect.setField(__eval__("Color").prototype, Obfu.raw("reset"), function(): Void {
      __this__.setTransform({"ra": 100, "rb": 0, "ba": 100, "bb": 0, "ga": 100, "gb": 0, "aa": 100, "ab": 0});
    });
    _global.ASSetPropFlags(__eval__("Color").prototype, Obfu.raw("reset"), 1);

    Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("get"), function(x: String): String {
      return __this__.attributes[x];
    });
    _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("get"), 1);

    Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("set"), function(x: String, y: String): Void {
      __this__.attributes[x] = y;
    });
    _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("set"), 1);

    Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("exists"), function(x: String): Bool {
      return __this__.attributes[x] != null;
    });
    _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("exists"), 1);

    Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("iterAttributes"), function(f: String -> String -> Void): Void {
      for (attributeName in Reflect.fields(__this__.attributes)) {
        f(attributeName, __this__.attributes[attributeName]);
      }
    });
    _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("iterAttributes"), 1);

    // TODO(demurgos): Replace `Dynamic` by `flash.xml.XMLNode`
    Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("iterElements"), function(f: Dynamic -> Void): Void {
      var node: Null<Dynamic> = __this__.firstChild;
      while (node != null) {
        f(node);
        node = node.nextSibling;
      }
    });
    _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("iterElements"), 1);

    Reflect.setField(__eval__("XML").prototype, Obfu.raw("get"), function(x: String): String {
      return __this__.attributes[x];
    });
    _global.ASSetPropFlags(__eval__("XML").prototype, Obfu.raw("get"), 1);

    Reflect.setField(__eval__("XML").prototype, Obfu.raw("set"), function(x: String, y: String): Void {
      __this__.attributes[x] = y;
    });
    _global.ASSetPropFlags(__eval__("XML").prototype, Obfu.raw("set"), 1);

    Reflect.setField(__eval__("XML").prototype, Obfu.raw("exists"), function(x: String): Bool {
      return __this__.attributes[x] != null;
    });
    _global.ASSetPropFlags(__eval__("XML").prototype, Obfu.raw("exists"), 1);

    if (compat) {
      Reflect.setField(Array.prototype, Obfu.raw("remove"), Array.prototype.remove);
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("remove"), 1);
      Reflect.setField(Array.prototype, Obfu.raw("0ENPA"), Array.prototype.remove);
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("0ENPA"), 1);

      Reflect.setField(Array.prototype, Obfu.raw("4]GSJ("), Array.prototype.insert);
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("4]GSJ("), 1);
      Reflect.setField(Array.prototype, Obfu.raw("insert"), Array.prototype.insert);
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("insert"), 1);
      Reflect.setField(Array.prototype, Obfu.raw("insertAt"), Array.prototype.insert);
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("insertAt"), 1);

      Reflect.setField(Array.prototype, Obfu.raw("],lYS"), Reflect.field(Array.prototype, Obfu.raw("copy")));
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("],lYS"), 1);
      Reflect.setField(Array.prototype, Obfu.raw("slice2"), Reflect.field(Array.prototype, Obfu.raw("copy")));
      _global.ASSetPropFlags(Array.prototype, Obfu.raw("slice2"), 1);

      Reflect.setField(__eval__("Color").prototype, Obfu.raw("{L 35"), Reflect.field(__eval__("Color").prototype, Obfu.raw("reset")));
      _global.ASSetPropFlags(__eval__("Color").prototype, Obfu.raw("{L 35"), 1);
      Reflect.setField(__eval__("Color").prototype, Obfu.raw("identityTransform"), Reflect.field(__eval__("Color").prototype, Obfu.raw("reset")));
      _global.ASSetPropFlags(__eval__("Color").prototype, Obfu.raw("identityTransform"), 1);

      Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw(" Ff_G"), Reflect.field(__eval__("XMLNode").prototype, Obfu.raw("exists")));
      _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw(" Ff_G"), 1);
      Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("isset"), Reflect.field(__eval__("XMLNode").prototype, Obfu.raw("exists")));
      _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("isset"), 1);

      Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("}}{7B"), Reflect.field(__eval__("XMLNode").prototype, Obfu.raw("iterAttributes")));
      _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("}}{7B"), 1);
      Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("eachAttr"), Reflect.field(__eval__("XMLNode").prototype, Obfu.raw("iterAttributes")));
      _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("eachAttr"), 1);

      Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("2s=h5("), Reflect.field(__eval__("XMLNode").prototype, Obfu.raw("iterElements")));
      _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("2s=h5("), 1);
      Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("each"), Reflect.field(__eval__("XMLNode").prototype, Obfu.raw("iterElements")));
      _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("each"), 1);
      Reflect.setField(__eval__("XMLNode").prototype, Obfu.raw("eachNode"), Reflect.field(__eval__("XMLNode").prototype, Obfu.raw("iterElements")));
      _global.ASSetPropFlags(__eval__("XMLNode").prototype, Obfu.raw("eachNode"), 1);

      Reflect.setField(__eval__("XML").prototype, Obfu.raw(" Ff_G"), Reflect.field(__eval__("XML").prototype, Obfu.raw("exists")));
      _global.ASSetPropFlags(__eval__("XML").prototype, Obfu.raw(" Ff_G"), 1);
      Reflect.setField(__eval__("XML").prototype, Obfu.raw("isset"), Reflect.field(__eval__("XML").prototype, Obfu.raw("exists")));
      _global.ASSetPropFlags(__eval__("XML").prototype, Obfu.raw("isset"), 1);
    }

    // Restore `Array` prototype protection.
    _global.ASSetPropFlags(Array.prototype, null, 7);

    if (compat) {
      // 1cEQH = StyleSheet
      Reflect.setField(global, Obfu.raw("1cEQH"), Reflect.field(Reflect.field(_global, "TextField"), Obfu.raw("1cEQH")));
    }

    if (!Reflect.hasField(mc, Obfu.raw("Std"))) {
      Reflect.setField(mc, Obfu.raw("Std"), {});
    }

    var mcStd: Dynamic = Reflect.field(mc, Obfu.raw("Std"));

    Reflect.setField(mc, Obfu.raw("HfestStd"), mcStd);

    /**
     * "{XfS6" (`compat` does not define this property because it is a primitive value)
     */
    if (!Reflect.hasField(mcStd, Obfu.raw("uid"))) {
      Reflect.setField(mcStd, Obfu.raw("uid"), 0);
    }

    /**
     * "7bSH*"
     */
    Reflect.setField(mcStd, Obfu.raw("addMovie"), function(mc: MovieClip, link: String, depth: Int): MovieClip {
      var clipname = link + "@" + mcSeed + "@" + __this__[Obfu.raw("uid")]++;
      mc.attachMovie(link, clipname, depth);
      return Reflect.field(mc, clipname);
    });

    /**
     * "-4*l(("
     */
    Reflect.setField(mcStd, Obfu.raw("addEmptyMovie"), function(mc: MovieClip, depth: Int): MovieClip {
      var clipname = 'empty@' + mcSeed + "@" + __this__[Obfu.raw("uid")]++;
      mc.createEmptyMovieClip(clipname, depth);
      return Reflect.field(mc, clipname);
    });

    /**
     * "5jHJK("
     */
    Reflect.setField(mcStd, Obfu.raw("addDuplicatedMovie"), function(mc: MovieClip, depth: Int): MovieClip {
      var clipname = "dup@" + mcSeed + "@" + __this__[Obfu.raw("uid")]++;
      mc.duplicateMovieClip(clipname, depth);
      return Reflect.field(mc._parent, clipname);
    });

    /**
     * "6{X3L", "get
     */
    Reflect.setField(mcStd, Obfu.raw("getVariable"), function(mc: MovieClip, v: String): Dynamic {
      return Reflect.field(mc, v);
    });

    /**
     * "4BHo7", "set"
     */
    Reflect.setField(mcStd, Obfu.raw("setVariable"), function(mc: MovieClip, v: String, value: Dynamic): Void {
      Reflect.setField(mc, v, value);
    });

    /**
     * "9HH5a"
     */
    Reflect.setField(mcStd, Obfu.raw("getRoot"), function(): MovieClip {
      return root;
    });

    /**
     * "16(VU"
     */
    Reflect.setField(mcStd, Obfu.raw("getGlobal"), function(v: String): Dynamic {
      return Reflect.field(global, v);
    });

    /**
     * "3Wr3K("
     */
    Reflect.setField(mcStd, Obfu.raw("setGlobal"), function(v: String, vv: Dynamic): Void {
      Reflect.setField(global, v, vv);
      if (StdInc.RAW_GLOBALS.indexOf(v) >= 0) {
        Reflect.setField(flash.Lib._global, v, vv);
      }
    });

    // TODO(demurgos): Replace `Dynamic` by `flash.TextField`
    Reflect.setField(mcStd, Obfu.raw("createTextField"), function(mc: MovieClip, depth: Int): Dynamic {
      var clipname = "text@" + mcSeed + "@" + __this__[Obfu.raw("uid")]++;
      mc.createTextField(clipname, depth, 0, 0, 100, 20);
      return Reflect.field(mc, clipname);
    });

    /**
     * "2g(*"
     */
    Reflect.setField(mcStd, Obfu.raw("returnarg"), function(x: Dynamic): Dynamic {
      return x;
    });

    Reflect.setField(mcStd, Obfu.raw("hitTest"), function(mc1: MovieClip, mc2: MovieClip): Bool {
      return mc1.hitTest(mc2);
    });

    Reflect.setField(mcStd, Obfu.raw("random"), function(n: Int): Int {
      return __random__(n);
    });

    /**
     * ",))hC"
     */
    Reflect.setField(mcStd, Obfu.raw("getMouseX"), function(): Float {
      return flash.Lib._root._xmouse;
    });

    /**
     * "8p1P5"
     */
    Reflect.setField(mcStd, Obfu.raw("getMouseY"), function(): Float {
      return flash.Lib._root._ymouse;
    });

    Reflect.setField(mcStd, Obfu.raw("escape"), StringTools.urlEncode);

    Reflect.setField(mcStd, Obfu.raw("unescape"), StringTools.urlDecode);

    Reflect.setField(mcStd, Obfu.raw("parseInt"), _global.parseInt);

    Reflect.setField(mcStd, Obfu.raw("toString"), Reflect.makeVarArgs(function(args: Array<Dynamic>): String {
      if (args.length == 0) {
        return "<StdInc.init~Std>";
      }
      var x: Dynamic = args[0];
      return __eval__("String")(x);
    }));

    /**
     * "}0=HF("
     */
    Reflect.setField(mcStd, Obfu.raw("nbrToBase"), function(x: Int, n: Int): String {
      return __eval__("Number")(x).toString(n);
    });

    Reflect.setField(mcStd, Obfu.raw("isNaN"), _global.isNaN);

    Reflect.setField(mcStd, Obfu.raw("POSITIVE_INFINITY"), __eval__("Infinity"));

    Reflect.setField(mcStd, Obfu.raw("registerClass"), __eval__("Object").registerClass);

    /**
     * "+QD*"
     */
    Reflect.setField(mcStd, Obfu.raw("extend"), function(a: Dynamic, b: Dynamic): Dynamic {
      for (key in Reflect.fields(b)) {
        Reflect.setField(a, key, Reflect.field(b, key));
      }
      return a;
    });

    /**
     * "6{VYR"
     */
    Reflect.setField(mcStd, Obfu.raw("bindFunction"), Reflect.makeVarArgs(function(args: Array<Dynamic>): Dynamic {
      var o: Dynamic = args[0];
      var f: String = args[1];
      var boundArgs = args.slice(2);
      return Reflect.makeVarArgs(function(freeArgs: Array<Dynamic>) {
        return Reflect.callMethod(o, Reflect.field(o, f), boundArgs.concat(freeArgs));
      });
    }));

    Reflect.setField(mcStd, Obfu.raw("getTimer"), function(): Int {
      return __gettimer__();
    });

    /**
     * ")goHJ("
     */
    Reflect.setField(mcStd, Obfu.raw("getTypeOf"), function(o: Dynamic): String {
      return __typeof__(o);
    });

    /**
     * "-jONz"
     */
    Reflect.setField(mcStd, Obfu.raw("isInstanceOf"), function(o: Dynamic, k: Dynamic): Bool {
      return __instanceof__(o, k);
    });

    /**
     * "9ss9+"
     */
    Reflect.setField(mcStd, Obfu.raw("each"), function(o: Dynamic, f: String -> Void): Void {
      for (key in Reflect.fields(o)) {
        f(key);
      }
    });

    /**
     * "]1dDm"
     */
    Reflect.setField(mcStd, Obfu.raw("instanciate"), function(o: Dynamic, a1: Dynamic, a2: Dynamic, a3: Dynamic, a4: Dynamic): Dynamic {
      return __new__(o, a1, a2, a3, a4);
    });

    /**
     * "7t4B ("
     */
    Reflect.setField(mcStd, Obfu.raw("removeKey"), function(o: Dynamic, k: String): Void {
      Reflect.deleteField(o, k);
    });

    Reflect.setField(mcStd, Obfu.raw("fscommand"), function(c: String, a: Dynamic): Void {
      flash.Lib.fscommand("" + c, a);
    });

    /**
     * "=JpT6"
     */
    Reflect.setField(mcStd, Obfu.raw("throwError"), function(e: Dynamic): Void {
      throw e;
    });

    if (compat) {
      Reflect.setField(mcStd, Obfu.raw("7bSH*"), Reflect.field(mcStd, Obfu.raw("addMovie")));
      Reflect.setField(mcStd, Obfu.raw("-4*l(("), Reflect.field(mcStd, Obfu.raw("addEmptyMovie")));
      Reflect.setField(mcStd, Obfu.raw("5jHJK("), Reflect.field(mcStd, Obfu.raw("addDuplicatedMovie")));
      Reflect.setField(mcStd, Obfu.raw("6{X3L"), Reflect.field(mcStd, Obfu.raw("getVariable")));
      Reflect.setField(mcStd, Obfu.raw("get"), Reflect.field(mcStd, Obfu.raw("getVariable")));
      // External 2
      Reflect.setField(mcStd, Obfu.raw("getValue"), Reflect.field(mcStd, Obfu.raw("getVariable")));
      Reflect.setField(mcStd, Obfu.raw("4BHo7"), Reflect.field(mcStd, Obfu.raw("setVariable")));
      Reflect.setField(mcStd, Obfu.raw("set"), Reflect.field(mcStd, Obfu.raw("setVariable")));
      // External 2
      Reflect.setField(mcStd, Obfu.raw("setValue"), Reflect.field(mcStd, Obfu.raw("setVariable")));
      Reflect.setField(mcStd, Obfu.raw("9HH5a"), Reflect.field(mcStd, Obfu.raw("getRoot")));
      Reflect.setField(mcStd, Obfu.raw("16(VU"), Reflect.field(mcStd, Obfu.raw("getGlobal")));
      // External 2
      Reflect.setField(mcStd, Obfu.raw("getEnv"), Reflect.field(mcStd, Obfu.raw("getGlobal")));
      Reflect.setField(mcStd, Obfu.raw("3Wr3K("), Reflect.field(mcStd, Obfu.raw("setGlobal")));
      // External 2
      Reflect.setField(mcStd, Obfu.raw("setEnv"), Reflect.field(mcStd, Obfu.raw("setGlobal")));
      Reflect.setField(mcStd, Obfu.raw("2g(*"), Reflect.field(mcStd, Obfu.raw("returnarg")));
      Reflect.setField(mcStd, Obfu.raw(",))hC"), Reflect.field(mcStd, Obfu.raw("getMouseX")));
      Reflect.setField(mcStd, Obfu.raw("8p1P5"), Reflect.field(mcStd, Obfu.raw("getMouseY")));
      Reflect.setField(mcStd, Obfu.raw("}0=HF("), Reflect.field(mcStd, Obfu.raw("nbrToBase")));
      Reflect.setField(mcStd, Obfu.raw("=Pn1n"), Reflect.field(mcStd, Obfu.raw("POSITIVE_INFINITY")));
      Reflect.setField(mcStd, Obfu.raw("Infinity"), Reflect.field(mcStd, Obfu.raw("POSITIVE_INFINITY")));
      Reflect.setField(mcStd, Obfu.raw("+QD*"), Reflect.field(mcStd, Obfu.raw("extend")));
      Reflect.setField(mcStd, Obfu.raw("6{VYR"), Reflect.field(mcStd, Obfu.raw("bindFunction")));
      Reflect.setField(mcStd, Obfu.raw(")goHJ("), Reflect.field(mcStd, Obfu.raw("getTypeOf")));
      Reflect.setField(mcStd, Obfu.raw("-jONz"), Reflect.field(mcStd, Obfu.raw("isInstanceOf")));
      Reflect.setField(mcStd, Obfu.raw("9ss9+"), Reflect.field(mcStd, Obfu.raw("each")));
      Reflect.setField(mcStd, Obfu.raw("]1dDm"), Reflect.field(mcStd, Obfu.raw("instanciate")));
      Reflect.setField(mcStd, Obfu.raw("7t4B ("), Reflect.field(mcStd, Obfu.raw("removeKey")));
      Reflect.setField(mcStd, Obfu.raw("=JpT6"), Reflect.field(mcStd, Obfu.raw("throwError")));
      Reflect.setField(mc, Obfu.raw("3Wt"), mcStd);
      Reflect.setField(mc, Obfu.raw("lib"), mcStd);
    }

    Reflect.setField(mc, Obfu.raw("Log"), function() {});
    var mcLog: Dynamic = Reflect.field(mc, Obfu.raw("Log"));
    Reflect.setField(mcLog, Obfu.raw("std"), mcStd);
    Reflect.setField(mcLog, Obfu.raw("ready"), false);

    Reflect.setField(mcLog, Obfu.raw("clean"), function() {
      Reflect.field(__this__, Obfu.raw("text2")).text = "";
      Reflect.field(__this__, Obfu.raw("text1")).text = "";
    });

    Reflect.setField(mcLog, Obfu.raw("init"), function(mc, w, h) {
      if (mc == null) {
        mc = root;
      }
      if (w == null) {
        w = Stage.width;
      }
      if (h == null) {
        h = Stage.height;
      }

      Reflect.setField(__this__, Obfu.raw("innerHeight"), h - 30);
      Reflect.setField(__this__, Obfu.raw("ready"), true);
      var text1 = mcStd[Obfu.raw("createTextField")](mc, 99999);
      Reflect.setField(__this__, Obfu.raw("text1"), text1);
      var text2 = mcStd[Obfu.raw("createTextField")](mc, 99998);
      Reflect.setField(__this__, Obfu.raw("text2"), text2);
      text1._x = 5;
      text1._y = 20;
      text1._width = w - 10;
      text1._height = h;
      text2._x = w / 2;
      text2._y = 20;
      text2._width = w / 2;
      text2._height = h - 30;
      text1.wordWrap = true;
      text2.wordWrap = true;
      text1.selectable = false;
      text2.selectable = false;
      var me = __this__;
      // Timer ID
      Reflect.setField(__this__, Obfu.raw("tid"), setInterval(function() {
        if (Reflect.field(me, Obfu.raw("text2")).text != '') {
          Reflect.field(me, Obfu.raw("text2")).text = '';
        }
      }, 10));

      if (compat) {
        Reflect.setField(__this__, Obfu.raw("[VAKp"), Reflect.field(__this__, Obfu.raw("innerHeight")));
        Reflect.setField(__this__, Obfu.raw(";lvaa"), Reflect.field(__this__, Obfu.raw("ready")));
        Reflect.setField(__this__, Obfu.raw("]AO l"), Reflect.field(__this__, Obfu.raw("text1")));
        Reflect.setField(__this__, Obfu.raw("{RuFi"), Reflect.field(__this__, Obfu.raw("text2")));
        Reflect.setField(__this__, Obfu.raw("](L"), Reflect.field(__this__, Obfu.raw("tid")));
      }
    });

    Reflect.setField(mcLog, Obfu.raw("addText"), function(s: String) {
      if (!Reflect.field(__this__, Obfu.raw("ready"))) {
        Reflect.callMethod(__this__, Reflect.field(__this__, Obfu.raw("init")), []);
      }
      Reflect.field(__this__, Obfu.raw("text1")).text += (((s.split('\n')).join('\\n')).split('\r')).join('\\r') + '\n';
      while (Reflect.field(__this__, Obfu.raw("text1")).textHeight > Reflect.field(__this__, Obfu.raw("innerHeight"))) {
        var idx = Reflect.field(__this__, Obfu.raw("text1")).text.indexOf('\r', 0);
        var ntext = Reflect.field(__this__, Obfu.raw("text1")).text.substr(idx + 1);
        Reflect.field(__this__, Obfu.raw("text1")).text = ntext;
      }
    });

    Reflect.setField(mcLog, Obfu.raw("setColor"), function(c) {
      if (!Reflect.field(__this__, Obfu.raw("ready"))) {
        Reflect.callMethod(__this__, Reflect.field(__this__, Obfu.raw("init")), []);
      }
      Reflect.field(__this__, Obfu.raw("text1")).textColor = c;
      Reflect.field(__this__, Obfu.raw("text2")).textColor = c;
    });

    Reflect.setField(mcLog, Obfu.raw("print"), function(x) {
      if (!Reflect.field(__this__, Obfu.raw("ready"))) {
        Reflect.callMethod(__this__, Reflect.field(__this__, Obfu.raw("init")), []);
      }
      Reflect.field(__this__, Obfu.raw("text2")).text += '~ ' + __this__.toString(x, '') + '\n';
    });

    Reflect.setField(mcLog, Obfu.raw("remove"), function() {
      clearInterval(Reflect.field(__this__, Obfu.raw("tid")));
      Reflect.setField(__this__, Obfu.raw("ready"), false);
      Reflect.field(__this__, Obfu.raw("text1")).removeMovieClip();
      Reflect.field(__this__, Obfu.raw("text2")).removeMovieClip();
    });

    // object, string
    Reflect.setField(mcLog, Obfu.raw("toString"), Reflect.makeVarArgs(function(args: Array<Dynamic>): String {
      if (args.length == 0) {
        return "<StdInc.init~Log>";
      }
      var o: Dynamic = args[0];
      var s: String = args[1];
      if (s == null) {
        s = '';
      } else {
        if (s.length >= 20) {
          return '<...>';
        }
      }
      var argType: String = __typeof__(o);
      if (argType == Obfu.raw("object") || argType == Obfu.raw("movieclip")) {
        var txt;
        if (__instanceof__(o, Array)) {
          var c = o.length;
          var i = 0;
          txt = '[';
          s += '    ';
          while (i < c) {
            txt += (i > 0 ? ',' : '') + __this__.toString(obj[i], s);
            i++;
          }
          s = s.substring(4);
          txt += ']';
        } else {
          var str = obj.toString();
          if (__typeof__(str) == 'string' && str != '[object Object]') {
            return str;
          }
          var key;
          txt = '{\n';
          if (__typeof__(o) == 'movieclip') {
            txt = 'MC(' + o._name + ') ' + txt;
          }
          s += '    ';
          for (key in Reflect.fields(o)) {
            txt += s + key + ' : ' + __this__.toString(o[key], s) + '\n';
          }
          s = s.substring(4);
          txt += s + '}';
        }
        return txt;
      } else if (argType == Obfu.raw("function")) {
        return '<fun>';
      } else if (argType == Obfu.raw("string")) {
        return o;
      } else {
        return String(o);
      }
    }));

    Reflect.setField(mcLog, Obfu.raw("trace"), function(o: Dynamic): Void {
      var str = (__this__.toString(o, "")).split("\n");
      var i = 0 ;
      var c = str.length;
      while (i < c) {
        __this__[Obfu.raw("addText")](str[i]);
        ++i;
      }
    });

    if (compat) {
      Reflect.setField(mcLog, Obfu.raw("+rJ"), Reflect.field(mcLog, Obfu.raw("std")));
      Reflect.setField(mcLog, Obfu.raw("lib"), Reflect.field(mcLog, Obfu.raw("std")));
      Reflect.setField(mcLog, Obfu.raw(";lvaa"), Reflect.field(mcLog, Obfu.raw("ready")));
      Reflect.setField(mcLog, Obfu.raw("}-B2"), Reflect.field(mcLog, Obfu.raw("init")));
      Reflect.setField(mcLog, Obfu.raw("createTextArea"), Reflect.field(mcLog, Obfu.raw("init")));
      Reflect.setField(mcLog, Obfu.raw("+9L}b"), Reflect.field(mcLog, Obfu.raw("addText")));
      Reflect.setField(mcLog, Obfu.raw("(H5 S"), Reflect.field(mcLog, Obfu.raw("setColor")));
      Reflect.setField(mcLog, Obfu.raw("color"), Reflect.field(mcLog, Obfu.raw("setColor")));
      Reflect.setField(mcLog, Obfu.raw("clear"), Reflect.field(mcLog, Obfu.raw("clean")));
      // TODO(demurgos): Rename, remove is `0ENPA`: detach?
      Reflect.setField(mcLog, Obfu.raw(",}2X ("), Reflect.field(mcLog, Obfu.raw("remove")));

      Reflect.setField(mc, Obfu.raw("]{i"), mcLog);
      Reflect.setField(mc, Obfu.raw("textManager"), mcLog);
      Reflect.setField(mcStd, Obfu.raw("Log"), mcLog);
    }

    Reflect.setField(mc, Obfu.raw("Hash"), function() {});
    var mcHash: Dynamic = Reflect.field(mc, Obfu.raw("Hash"));

    Reflect.setField(mcHash.prototype, Obfu.raw("get"), function(k) {
      return __this__[k];
    });

    Reflect.setField(mcHash.prototype, Obfu.raw("set"), function(k, i) {
      __this__[k] = i;
    });

    Reflect.setField(mcHash.prototype, Obfu.raw("remove"), function(k) {
      var result = __this__[k] != null;
      __delete__(__this__, k);
      return result;
    });

    Reflect.setField(mcHash.prototype, Obfu.raw("isset"), function(k) {
      return __this__[k] != null;
    });

    Reflect.setField(mcHash.prototype, Obfu.raw("each"), function(f) {
      var key;
      for (key in __this__) {
        f(key, __this__[key]);
      }
    });

    if (compat) {
      Reflect.setField(mcHash.prototype, Obfu.raw("0ENPA"), Reflect.field(mcHash.prototype, Obfu.raw("remove")));
      Reflect.setField(mcHash.prototype, Obfu.raw(" Ff_G"), Reflect.field(mcHash.prototype, Obfu.raw("isset")));
      Reflect.setField(mcHash.prototype, Obfu.raw("5uJ2"), Reflect.field(mcHash.prototype, Obfu.raw("each")));

      Reflect.setField(mc, Obfu.raw("19{O"), mcHash);
      Reflect.setField(mc, Obfu.raw("containerObject"), mcHash);
    }

    _global.ASSetPropFlags(mcHash.prototype, null, 1);

    if (Reflect.setField(mc, "__initializedStdInc", true)) {
      return;
    }
  }
  #end
}
