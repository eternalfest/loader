package mt.serializer;

import Reflect;
import haxe.ds.Option;

class SimpleSerializer {
  /**
   * Serialization key (weak encryption)
   */
  private var key:String;

  /**
   * Current mask (on 32 bits)
   */
  private var mask:Int;

  private var byteShift:Int;

  /**
   * Circular Redundancy Check
   */
  private var crc:Int;

  /**
   * Currently encoded string.
   */
  private var encoded:String;

  public function new(key:String) {
    this.key = key;
    this.mask = 0;
    for (i in 0...key.length) {
      this.mask = ((this.mask * 51) + key.charCodeAt(i)) & -1;
    }
  }

  /**
   * Return the Circular Redundancy Check (CRC)
   */
  private function getCrc():String {
    var result:String = "";
    for (i in 0...5) {
      result += SimpleSerializer.charset.charAt((this.crc >> (6 * i)) & 63);
    }
    return result;
  }

  public function serialize(obj:Dynamic):String {
    this.byteShift = 0;
    this.crc = 0;
    this.encoded = "";
    this.write(obj);
    this.encoded += this.getCrc();
    return this.encoded;
  }

  public function deserialize(encoded:String):Option<Dynamic> {
    this.byteShift = 0;
    this.crc = 0;
    this.encoded = encoded;
    var result:Dynamic = this.read();
    if (this.encoded != this.getCrc()) {
      return Option.None;
    }
    return Option.Some(result);
  }

  private function writeString(string:String) {
    for (i in 0...string.length) {
      this.writeCharacter(string.charAt(i));
    }
  }

  /**
   * Write a character to the encoded string.
   */
  private function writeCharacter(character:String) {
    var plainValue = SimpleSerializer.charset.indexOf(character, 0);
    if (plainValue < 0) {
      this.encoded += character;
      return;
    }

    var cipheredValue = plainValue ^ this.mask >> this.byteShift & 63;
    this.crc = (this.crc * 51 + cipheredValue) ^ -1;
    this.encoded += SimpleSerializer.charset.charAt(cipheredValue);
    this.byteShift = (this.byteShift + 6) % 28;
  }

  /**
   * Read the next character of the encoded string.
   * This shortens the encoded string (the character is consumed).
   */
  private function readCharacter():String {
    var character:String = this.encoded.charAt(0);
    this.encoded = this.encoded.substring(1, this.encoded.length);
    var cipheredValue = SimpleSerializer.charset.indexOf(character, 0);
    if (cipheredValue < 0) {
      return character;
    }
    this.crc = (this.crc * 51 + cipheredValue) ^ -1;
    var plainValue = cipheredValue ^ this.mask >> this.byteShift & 63;
    this.byteShift = (this.byteShift + 6) % 28;
    return SimpleSerializer.charset.charAt(plainValue);
  }

  private function readString():String {
    var result:String = "";
    while (true) {
      var nextCharacter = this.readCharacter();
      if (nextCharacter == null) {
        return null;
      } else if (nextCharacter == ":") {
        break;
      } else {
        result += nextCharacter;
      }
    }
    return result;
  }

  private function writeArray(array:Array<Dynamic>) {
    // TODO: Specify radix
    this.writeString(Std.string(array.length));
    this.writeString(":");
    var nullCount:Int = 0;
    for (i in 0...array.length) {
      if (array[i] == null) {
        nullCount++;
        continue;
      }
      if (nullCount > 1) {
        // TODO: Specify radix
        this.writeString(SimpleSerializer.controlChars.charAt(11) + Std.string(nullCount) + ":");
        nullCount = 0;
      } else if (nullCount == 1) {
        this.write(null);
        nullCount = 0;
      }
      this.write(array[i]);
    }
    if (nullCount > 0) {
      // TODO: Specify radix
      this.writeString(SimpleSerializer.controlChars.charAt(11) + Std.string(nullCount) + ":");
    }
  }

  private function writeObject(object:Dynamic) {
    for (key in Reflect.fields(object)) {
      this.writeString(key);
      this.writeString(":");
      this.write(Reflect.field(object, key));
    }
    this.writeString(":");
  }

  private function write(value:Dynamic) {
    if (value == null) {
      this.writeString(SimpleSerializer.controlChars.charAt(1));
    } else if (Std.is(value, Array)) {
      this.writeString(SimpleSerializer.controlChars.charAt(2));
      this.writeArray(value);
    } else if (Std.is(value, String)) {
      this.writeString(SimpleSerializer.controlChars.charAt(3));
      this.writeString(value);
      this.writeString(":");
    } else if (Std.is(value, Float) || Std.is(value, Int)) {
      var floatValue:Float = value;
      if (Math.isNaN(floatValue)) {
        this.writeString(SimpleSerializer.controlChars.charAt(4));
      } else if (floatValue == Math.POSITIVE_INFINITY) {
        this.writeString(SimpleSerializer.controlChars.charAt(5));
      } else if (floatValue == Math.NEGATIVE_INFINITY) {
        this.writeString(SimpleSerializer.controlChars.charAt(6));
      } else {
        var intValue:Int = Std.int(floatValue);
        this.writeString(SimpleSerializer.controlChars.charAt(7));
        if (intValue < 0) {
          this.writeString(SimpleSerializer.controlChars.charAt(1));
        }
        // TODO: Specify radix
        this.writeString(Std.string(intValue < 0 ? -intValue : intValue));
        this.writeString(":");
      }
    } else if (Std.is(value, Bool)) {
      if (value) {
        this.writeString(SimpleSerializer.controlChars.charAt(8));
      } else {
        this.writeString(SimpleSerializer.controlChars.charAt(9));
      }
    } else {
      this.writeString(SimpleSerializer.controlChars.charAt(10));
      this.writeObject(value);
    }
  }

  private function readArray():Array<Dynamic> {
    // TODO: specify radix
    var arrayLength = Std.parseInt(this.readString());
    var result:Array<Dynamic> = [];
    for (i in 0...arrayLength) {
      result.push(this.read());
    }
    return result;
  }

  private function readObject():Dynamic {
    var result:Dynamic = {};
    while (true) {
      var key:String = this.readString();
      if (key == null) {
        return null;
      }
      if (key == "") {
        break;
      }
      var value = this.read();
      Reflect.setField(result, key, value);
    }
    return result;
  }

  private function read():Dynamic {
    var controlChar = this.readCharacter();
    if (controlChar == SimpleSerializer.controlChars.charAt(1)) {
      return null;
    } else if (controlChar == SimpleSerializer.controlChars.charAt(2)) {
      return this.readArray();
    } else if (controlChar == SimpleSerializer.controlChars.charAt(3)) {
      return this.readString();
    } else if (controlChar == SimpleSerializer.controlChars.charAt(4)) {
      return Math.NaN;
    } else if (controlChar == SimpleSerializer.controlChars.charAt(5)) {
      return Math.POSITIVE_INFINITY;
    } else if (controlChar == SimpleSerializer.controlChars.charAt(6)) {
      return Math.NEGATIVE_INFINITY;
    } else if (controlChar == SimpleSerializer.controlChars.charAt(7)) {
      var signChar:String = this.readCharacter();
      var number:String = this.readString();
      if (signChar == SimpleSerializer.controlChars.charAt(1)) {
        // TODO: specify radix
        return -Std.parseInt(number);
      } else {
        // The signChar was in fact the first digit
        // TODO: specify radix
        return Std.parseInt(signChar + number);
      }
    } else if (controlChar == SimpleSerializer.controlChars.charAt(8)) {
      return true;
    } else if (controlChar == SimpleSerializer.controlChars.charAt(9)) {
      return false;
    } else if (controlChar == SimpleSerializer.controlChars.charAt(10)) {
      return this.readObject();
    } else {
      return null;
    }
  }

  public static var controlChars:String = "$uasxIintfoj";
  public static var charset:String = ":_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
}
