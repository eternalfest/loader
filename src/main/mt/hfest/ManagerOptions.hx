package mt.hfest;

import etwin.Obfu;
import mt.hfest.IManagerOptions;
#if flash8
import etwin.flash.Sound;
#else
import openfl.media.Sound;
#end

typedef ManagerOptionsStruct = {
  messagesXmlString: String,
  isDevServer: Bool,
  familiesString: String,
  gameOptionsString: String,
  musics: Array<Sound>
}

class ManagerOptions implements IManagerOptions {
  public var messagesXmlString: String;
  public var isDevServer: Bool;
  public var familiesString: String;
  public var gameOptionsString: String;
  public var musics: Array<Sound>;

  public function new() {}

  public static function fromStruct(struct: ManagerOptionsStruct): ManagerOptions {
    var result: ManagerOptions = new ManagerOptions();
    result.messagesXmlString = struct.messagesXmlString;
    result.isDevServer = struct.isDevServer;
    result.familiesString = struct.familiesString;
    result.gameOptionsString = struct.gameOptionsString;
    result.musics = struct.musics;
    return result;
  }

  public static function compat(options: IManagerOptions): Void {
    Reflect.setField(options, Obfu.raw("5J]Ek"), options.messagesXmlString);
    Reflect.setField(options, Obfu.raw("3 qNS"), options.isDevServer);
    Reflect.setField(options, Obfu.raw("0O q{"), options.familiesString);
    Reflect.setField(options, Obfu.raw("+LT;C("), options.gameOptionsString);
    Reflect.setField(options, Obfu.raw("}mR6O"), options.musics);

    Reflect.setField(options, Obfu.raw("rawLang"), options.messagesXmlString);

    Reflect.setField(options, Obfu.raw("langXml"), options.messagesXmlString);
    Reflect.setField(options, Obfu.raw("messagesXmlString"), options.messagesXmlString);
    Reflect.setField(options, Obfu.raw("isGameLocal"), options.isDevServer);
    Reflect.setField(options, Obfu.raw("isDevServer"), options.isDevServer);
    Reflect.setField(options, Obfu.raw("familiesList"), options.familiesString);
    Reflect.setField(options, Obfu.raw("familiesString"), options.familiesString);
    Reflect.setField(options, Obfu.raw("optionsString"), options.gameOptionsString);
    Reflect.setField(options, Obfu.raw("options"), options.gameOptionsString);
    Reflect.setField(options, Obfu.raw("gameOptionsString"), options.gameOptionsString);
    Reflect.setField(options, Obfu.raw("musics"), options.musics);
  }
}
