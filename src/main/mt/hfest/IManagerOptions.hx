package mt.hfest;

#if flash8
import etwin.flash.Sound;
#else
import openfl.media.Sound;
#end

interface IManagerOptions {
  var messagesXmlString: String;
  var isDevServer: Bool;
  var familiesString: String;
  var gameOptionsString: String;
  var musics: Array<Sound>;
}
