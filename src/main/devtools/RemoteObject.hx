package devtools;

typedef RemoteObject = {
  type: String,
  ?subtype: String,
  ?className: String,
  ?value: Dynamic,
  ?unserializableValue: Dynamic,
  ?description: String,
  ?objectId: RemoteObjectId,
};
