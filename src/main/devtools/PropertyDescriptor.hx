package devtools;

typedef PropertyDescriptor = {
name:String,
?value:RemoteObject,
?writable:Bool,
?get:RemoteObject,
?set:RemoteObject,
configurable:Bool,
enumerable:Bool,
?wasThrown:Bool,
?isOwn:Bool,
}
