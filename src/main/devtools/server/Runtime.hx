package devtools.server;

import etwin.Obfu;
import devtools.server.runtime.ConsoleAPI;
import devtools.server.runtime.ConsoleAPICalledEvent;
import devtools.server.runtime.GetPropertiesOptions;
import devtools.server.runtime.GetPropertiesResult;
import Reflect;
import swf_socket.SwfSocketClient;
import tink.core.Error;

#if flash8
import flash.MovieClip;
#else
import openfl.display.MovieClip;
#end

class Runtime {
  public var consoleAPI: ConsoleAPI;

  var socket: SwfSocketClient;
  var objects: Dynamic;
  var nextId: Int;

  public function new(socket: SwfSocketClient) {
    this.socket = socket;
    this.objects = {};
    this.nextId = 0;
    this.consoleAPI = new ConsoleAPI(this);
    this.socket.use("runtime/getProperties", this.handleGetProperties);
  }

  public function handleGetProperties(options: GetPropertiesOptions): GetPropertiesResult {
    var object: Dynamic = this.getObjectById(Reflect.field(options, Obfu.raw("objectId")));
    var properties: Array<PropertyDescriptor> = [];
    for (key in Reflect.fields(object)) {
      properties.push(this.getPropertyDescriptor(object, key));
    }
    if (Reflect.field(object, "prototype") != null) {
      properties.push(this.getPropertyDescriptor(object, "prototype"));
    }
    var raw: Dynamic = {};
    Reflect.setField(raw, Obfu.raw("result"), properties);
    return raw;
  }

  public function emitConsoleAPICalled(event: ConsoleAPICalledEvent): Void {
    var rawEvent: Dynamic = {};
    Reflect.setField(rawEvent, Obfu.raw("type"), event.type);
    Reflect.setField(rawEvent, Obfu.raw("args"), event.args);
    Reflect.setField(rawEvent, Obfu.raw("executionContextId"), event.executionContextId);
    Reflect.setField(rawEvent, Obfu.raw("timestamp"), event.timestamp);
    this.socket.emitMessage("runtime/consoleAPICalled", rawEvent);
  }

  public function getOrCreateRemoteObject(source: Dynamic): RemoteObject {
    return switch (untyped __typeof__(source)) {
      case "function": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("function"));
        Reflect.setField(raw, Obfu.raw("objectId"), this.getOrCreateObjectId(source));
        raw;
      }
      case "movieclip": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("object"));
        Reflect.setField(raw, Obfu.raw("className"), Obfu.raw("flash.MovieClip"));
        Reflect.setField(raw, Obfu.raw("objectId"), this.getOrCreateObjectId(source));
        raw;
      }
      case "object": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("object"));
        if (Std.is(source, Array)) {
          Reflect.setField(raw, Obfu.raw("subtype"), Obfu.raw("array"));
        } else if (Std.is(source, MovieClip)) {
          Reflect.setField(raw, Obfu.raw("className"), Obfu.raw("flash.MovieClip"));
        }
        Reflect.setField(raw, Obfu.raw("objectId"), this.getOrCreateObjectId(source));
        raw;
      }
      case "boolean": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("boolean"));
        Reflect.setField(raw, Obfu.raw("value"), source);
        raw;
      }
      case "string": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("string"));
        Reflect.setField(raw, Obfu.raw("value"), source);
        raw;
      }
      case "number": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("number"));
        if (Math.isNaN(source)) {
          Reflect.setField(raw, Obfu.raw("unserializableValue"), Obfu.raw("NaN"));
        } else if (source == Math.POSITIVE_INFINITY) {
          Reflect.setField(raw, Obfu.raw("unserializableValue"), Obfu.raw("Infinity"));
        } else if (source == Math.NEGATIVE_INFINITY) {
          Reflect.setField(raw, Obfu.raw("unserializableValue"), Obfu.raw("-Infinity"));
        } else if (source == 0 && 1 / source == Math.NEGATIVE_INFINITY) {
          Reflect.setField(raw, Obfu.raw("unserializableValue"), Obfu.raw("-0"));
        } else {
          Reflect.setField(raw, Obfu.raw("value"), source);
        }
        raw;
      }
      case "undefined": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("undefined"));
        raw;
      }
      case "null": {
        var raw: Dynamic = {};
        Reflect.setField(raw, Obfu.raw("type"), Obfu.raw("object"));
        Reflect.setField(raw, Obfu.raw("subtype"), Obfu.raw("null"));
        raw;
      }
      default:
        throw new Error("UnreachableCode");
    }
  }

  private function getObjectById(id: RemoteObjectId): Dynamic {
    var result: Dynamic = Reflect.field(this.objects, id);
    if (result == null) {
      throw new Error("Object not found for id: " + id);
    }
    return result;
  }

  private function getPropertyDescriptor(object: Dynamic, key: String): PropertyDescriptor {
    var raw: Dynamic = {};
    Reflect.setField(raw, Obfu.raw("name"), key);
    Reflect.setField(raw, Obfu.raw("value"), this.getOrCreateRemoteObject(Reflect.field(object, key)));
    Reflect.setField(raw, Obfu.raw("configurable"), true);
    Reflect.setField(raw, Obfu.raw("enumerable"), true);
    return raw;
  }

  private function getOrCreateObjectId(obj: Dynamic): String {
    for (objectId in Reflect.fields(this.objects)) {
      if (Reflect.field(this.objects, objectId) == obj) {
        return objectId;
      }
    }
    var id: String = this.getNextId();
    Reflect.setField(this.objects, id, obj);
    return id;
  }

  private function getNextId(): String {
    return Obfu.raw("o") + this.nextId++;
  }
}
