package devtools.server.runtime;

import devtools.RemoteObject;
import etwin.Obfu;

class ConsoleAPI {
  private var runtime: Runtime;

  public function new(runtime: Runtime) {
    this.runtime = runtime;
  }

  public function log(data: Dynamic): Void {
    var ro: RemoteObject = this.runtime.getOrCreateRemoteObject(data);
    var event: ConsoleAPICalledEvent = {
      type: Obfu.raw("log"),
      args: [ro],
      executionContextId: 0,
      timestamp: 0
    }
    this.runtime.emitConsoleAPICalled(event);
  }

  public function warn(data: Dynamic): Void {
    var ro: RemoteObject = this.runtime.getOrCreateRemoteObject(data);
    var event: ConsoleAPICalledEvent = {
      type: Obfu.raw("warning"),
      args: [ro],
      executionContextId: 0,
      timestamp: 0
    }
    this.runtime.emitConsoleAPICalled(event);
  }

  public function error(data: Dynamic): Void {
    var ro: RemoteObject = this.runtime.getOrCreateRemoteObject(data);
    var event: ConsoleAPICalledEvent = {
      type: Obfu.raw("error"),
      args: [ro],
      executionContextId: 0,
      timestamp: 0
    }
    this.runtime.emitConsoleAPICalled(event);
  }

  public function setPrint(message: String): Void {
    var ro: RemoteObject = this.runtime.getOrCreateRemoteObject(Std.string(message));
    var event: ConsoleAPICalledEvent = {
      type: "set-print",
      args: [ro],
      executionContextId: 0,
      timestamp: 0
    }
    this.runtime.emitConsoleAPICalled(event);
  }

  public function clear(): Void {
    var event: ConsoleAPICalledEvent = {
      type: "clear-print",
      args: [],
      executionContextId: 0,
      timestamp: 0
    }
    this.runtime.emitConsoleAPICalled(event);
  }
}
