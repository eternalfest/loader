package devtools.server.runtime;

typedef GetPropertiesOptions = {
objectId:RemoteObjectId,
ownProperties:Bool,
accessorPropertiesOnly:Bool,
};
