package devtools.server.runtime;

typedef ConsoleAPICalledEvent = {
type:String,
args:Array<RemoteObject>,
executionContextId:ExecutionContextId,
timestamp:Timestamp,
?stackTrace:Dynamic,
?context:String,
};
