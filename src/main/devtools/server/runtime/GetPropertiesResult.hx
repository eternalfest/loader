package devtools.server.runtime;

typedef GetPropertiesResult = {
result:Array<PropertyDescriptor>,
?internalProperties:Array<InternalPropertyDescriptor>,
?exceptionDetails:ExceptionDetails,
};
