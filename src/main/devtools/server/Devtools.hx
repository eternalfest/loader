package devtools.server;

import swf_socket.SwfSocketClient;

class Devtools {
  var socket: SwfSocketClient;
  public var runtime: Runtime;

  public function new(socket: SwfSocketClient) {
    this.socket = socket;
    this.runtime = new Runtime(socket);
  }
}
