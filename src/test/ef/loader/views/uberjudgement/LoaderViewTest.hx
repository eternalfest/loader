package ef.loader.views.uberjudgement;

import openfl.Assets;
import massive.munit.Assert;

class LoaderViewTest {
  @Test
  public function loadAssets () {
    var backText: String = Assets.getText("ef/loader/back.svg");
    Assert.isNotNull(backText);
    Assert.isTrue(backText.length > 0);
    var logoText: String = Assets.getText("ef/loader/logo.svg");
    Assert.isNotNull(logoText);
    Assert.isTrue(logoText.length > 0);
  }
}
