package ef.loader.i18n;

import massive.munit.Assert;

class DictionaryTest {
  @Test
  public function getStatic() {
    var en:Dictionary = Dictionary.getDictionary(Locale.En);
    var es:Dictionary = Dictionary.getDictionary(Locale.Es);
    var fr:Dictionary = Dictionary.getDictionary(Locale.Fr);
    Assert.areEqual("Travelling to Hammerfest...", en.getStatic(50));
    Assert.areEqual("Viajando a Hammerfest...", es.getStatic(50));
    Assert.areEqual("Voyage vers Hammerfest...", fr.getStatic(50));
  }
}
