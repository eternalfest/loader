package ef.api.types;

import massive.munit.Assert;

class DataTypeTest {
  @Test
  public function parseSamples() {
    var input: String = "2018-12-21T20:37:56.000Z";
    var expected: Date = Date.fromTime(1545424676000);
    var actual: Date = DateType.fromIsoString(input);
    Assert.isTrue(DateType.equals(actual, expected));
  }
}
