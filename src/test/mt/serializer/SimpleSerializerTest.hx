package mt.serializer;

import haxe.ds.Option;
import massive.munit.Assert;

class SimpleSerializerTest {
  @Test
  public function deserialize() {
    var key:String = "$RJrjk05eeJrzp5Pazre7z9an788baz61kBKJ1EZ4";
    var serializer:SimpleSerializer = new SimpleSerializer(key);
    var actual:Option<Dynamic> = serializer.deserialize("xNOpZUjm:Dt2:DnR2aUuk5mDt9rsyG6_");
    switch actual {
      case Option.Some(some):
        Assert.isTrue(Std.is(some, String));
        Assert.areEqual("httpXX__eternalfestYYnet_", some);
      default:
        Assert.fail("Expected result to be Option.Some");
    }
  }
}
