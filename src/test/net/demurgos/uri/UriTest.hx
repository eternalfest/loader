package net.demurgos.uri;

import massive.munit.Assert;

class UriTest {
  @Test
  public function parse() {
    var string: String = "https://eternalfest.net";
    var uri: Uri = new Uri(string);
    Assert.areEqual("https", uri.getScheme());
    Assert.areEqual(null, uri.getUsername());
    Assert.areEqual(null, uri.getPassword());
    Assert.areEqual("eternalfest.net", uri.getHostname());
    Assert.areEqual(null, uri.getPort());
    Assert.areEqual("/", uri.getPath());
    Assert.areEqual(null, uri.getQuery());
    Assert.areEqual(null, uri.getFragment());
    Assert.areEqual("eternalfest.net", uri.getHost());
    Assert.areEqual("/", uri.getResource());
  }
}
