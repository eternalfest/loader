# Contributing

## Requirements

The requirements with a normal font-weight are required to build the loader.
The requirements with a normal font-weight are additional requirements to develop and test the
loader.

### General

- **[_git_][git-home] latest, verified with 2.11.0 (2016-11-29)**
- **[_GNU make_][make-home] latest, verified with 4.1 (2014-10-05)**
- [_Node.js_][notes-node] latest, verified with 7.4.0
- [_npm_][notes-npm] latest, verified with 4.0.5
- [_Apache Flex SDK_][notes-flex-sdk]
- [Xvfb][xvfb-home] latest

#### Xvfb Installation

- Arch Linux

  ```shell
  # Run as root
  pacman -S xorg-server-xvfb
  ```

- Debian

  ```shell
  # Run as root
  apt-get install xvfb
  ```

### Haxe for Flash 8 environment

See the [_Haxe for Flash 8_ guide][notes-flash8].

- **[Haxe][haxe-home] 3.1.3 + bugfix**
- **[haxelib-home], verified with 3.1.0-rc.4**

### Haxelib

The following libraries are required **with their exact version**. You can install them from
the project root of the project with `make prepare.haxelib`.

- **[_lime_][github-lime] 2.3.3 (2015-04-21)**
- **[_openfl_][openfl-home] 3.0.1 (2015-04-09)**
- **[_svg_][github-svg] 1.1.1 (2016-10-10)**
- [_munit_][github-munit] 2.1.2 (2015-09-24)

## Development

### Trusted locations

Add the project's directory to the trusted locations, [in the Flash
settings][flash-settings-trusted-locations].

### _make_ targets

#### default

Install the dependencies and build the loader.

#### build

Build the loader.

#### dev-server

Start the development server on port 50317. It is used to test the HTTP requests.

#### prepare

Install both the _Haxelib_ and _Node.js_ dependencies.

#### prepare.dev-server

Install the _Node.js_ dependencies. They are required for the development server.

#### prepare.haxelib

Install the _Haxelib_ dependencies.

#### run

Build the loader and open the Flash 8 version with `xdg-open`.

#### test

Run the tests of the loader

## Editor

### IntelliJ IDEA

1. **Project Structure | Project | Project SDK**
2. **New** and choose `/usr/lib/haxe`


1. **Project Structure | Modules | Haxe**
2. Choose **OpenFL** and select **project.lime**
3. Add Haxe run configuration for **loader**.
4. Set the classpath to `/usr/lib/haxe/std`


[flash-settings-trusted-locations]: https://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager04.html
[git-home]: https://git-scm.com/
[github-lime]: https://github.com/openfl/lime
[github-munit]: https://github.com/massiveinteractive/MassiveUnit
[github-svg]: https://github.com/openfl/svg
[haxe-home]: http://haxe.org/
[haxelib-home]: http://lib.haxe.org/
[make-home]: https://www.gnu.org/software/make/
[openfl-home]: http://www.openfl.org/
[notes-flash8]: https://github.com/demurgos/notes/blob/master/tools/languages/haxe/flash8.md
[notes-flex-sdk]: https://github.com/demurgos/notes/blob/master/tools/languages/haxe/apache-flex-sdk/index.md
[notes-node]: https://github.com/demurgos/notes/blob/master/tools/languages/javascript/node.md
[notes-npm]: https://github.com/demurgos/notes/blob/master/tools/languages/javascript/npm.md
[xvfb-home]: https://www.x.org/releases/X11R7.6/doc/man/man1/Xvfb.1.xhtml
