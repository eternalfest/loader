import { getObfuMap as getGameMap } from "@eternalfest-types/game";
import { asMap, getAs2Map, mergeMaps, obfuscateBytes } from "@eternalfest/obf/obf";
import { outputFileAsync, readFile, readTextFile } from "./fs-utils";
import meta from "./meta";
import { fromSysPath, Furi, join as furiJoin } from "furi";

function getExtraAs2MapUri(): Furi {
  return furiJoin(fromSysPath(meta.dirname), "extra-as2.map.json");
}

async function getExtraAs2Map(): Promise<Map<string, string>> {
  const furi: Furi = getExtraAs2MapUri();
  const jsonStr: string = await readTextFile(furi);
  const raw: string[] = JSON.parse(jsonStr);
  return asMap(raw);
}

export async function obfuscate(
  inputFile: URL,
  outputFile: URL,
  mapFile: URL,
  key: string | null,
): Promise<void> {
  const as2Map: Map<string, string> = await getAs2Map();
  const extraAs2Map: Map<string, string> = await getExtraAs2Map();
  const gameMap: Map<string, string> = await getGameMap();
  const parentMap: Map<string, string> = mergeMaps(mergeMaps(as2Map, extraAs2Map), gameMap);

  const inBytes: Uint8Array = await readFile(inputFile);
  const result: any = obfuscateBytes(inBytes, key, parentMap);
  const mapRecord: Record<string, string> = Object.create(null);
  for (const [newStr, oldStr] of result.map) {
    Reflect.set(mapRecord, newStr, oldStr);
  }
  await Promise.all([
    outputFileAsync(outputFile, result.bytes),
    outputFileAsync(mapFile, Buffer.from(`${JSON.stringify(mapRecord, null, 2)}\n`)),
  ]);
}
