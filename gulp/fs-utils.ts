import * as fs from "fs";
import * as sysPath from "path";
import * as url from "url";

export async function outputFileAsync(filePath: URL, data: Uint8Array): Promise<void> {
  await mkdirpAsync(url.pathToFileURL(sysPath.dirname(url.fileURLToPath(filePath.toString()))));
  await writeFileAsync(filePath, data);
}

export async function mkdirpAsync(dirPath: fs.PathLike): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.mkdir(dirPath, {recursive: true}, (err: NodeJS.ErrnoException | null): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function writeFileAsync(filePath: fs.PathLike, data: Uint8Array): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function readFile(filePath: fs.PathLike): Promise<Uint8Array> {
  return new Promise<Uint8Array>((resolve, reject): void => {
    fs.readFile(filePath, {encoding: null}, (
      err: NodeJS.ErrnoException | null,
      data: Uint8Array
    ): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

export async function readTextFile(filePath: fs.PathLike): Promise<string> {
  return new Promise<string>((resolve, reject): void => {
    fs.readFile(filePath, {encoding: "utf-8"}, (
      err: NodeJS.ErrnoException | null,
      text: string
    ): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(text);
      }
    });
  });
}

export async function fileExists(filePath: fs.PathLike): Promise<boolean> {
  return new Promise<boolean>((resolve, reject): void => {
    fs.stat(filePath, (err: NodeJS.ErrnoException | null, stats: fs.Stats): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(stats.isFile());
      }
    });
  });
}
