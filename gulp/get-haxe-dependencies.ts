import * as sysPath from "path";

const projectRoot = sysPath.resolve(__dirname, "..");
const haxelibJsonPath = sysPath.resolve(projectRoot, "haxelib.json");

export function getHaxeDependencies() {
  const haxelibJson = require(haxelibJsonPath);
  return haxelibJson.dependencies;
}
