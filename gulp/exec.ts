import * as cp from "child_process";

export async function exec(name: string, args: ReadonlyArray<string>, opts: cp.SpawnOptions): Promise<void> {
  return new Promise((resolve, reject) => {
    const process = cp.spawn(
      name,
      args,
      Object.assign({stdio: "inherit"}, opts)
    );
    process.once("exit", (code, _signal) => {
      if (code !== 0) {
        reject(new Error("Process did not exit with code 0"));
      } else {
        resolve();
      }
    })
  });
}
